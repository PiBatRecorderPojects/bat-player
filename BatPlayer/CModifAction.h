/* 
 * File:   CModifAction.h
   BatPlayer Copyright (c) 2020 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Const.h"
#include "CModeGen.h"

#ifndef CMODIFACTION_H
#define CMODIFACTION_H

//-------------------------------------------------------------------------
//! \class CModificatorDir
//! \brief Management class of directory selected type modifier
class CModificatorDir : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param pDir Pointer on the dir to modify
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CModificatorDir(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    char *pDir,              
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Recursive directory search
  //! \param *pStartDir Start dir (/TEST/SUB for exemple)
  void SearchDir(
    char *pStartDir
    );

  //-------------------------------------------------------------------------
  //! \brief Set curent directory
  //! \param *pDir  Full path (/TEST/SUB for example)
  void SetDir(
    char *pDir
    );

protected:
  //! Pointer on the dir to modify
  char *pParam;

  // SD card directory list
  std::vector<String> lstDir;

  // Index of selected directory
  int idxDir;
};

//-------------------------------------------------------------------------
//! \class CModificatorFile
//! \brief Management class of file selected type modifier
class CModificatorFile : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param pDir Pointer on the dir of the file
  //! \param pFile Pointer on the file name to modify
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CModificatorFile(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    char *pDir,    
    char *pFile,              
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Recherche des fichiers dans un répertoire
  //! \param *pDir     Chemin complet (/TEST/SUB par exemple)
  //! \param *pFilter  Filtre de recherche des fichiers (*.wav par exemple, *.* pour tous)
  void SearchFiles(
    char *pDir,
    char *pFilter
    );

  //-------------------------------------------------------------------------
  //! \brief Set curent directory and file
  //! \param *pDir  Full path (/TEST/SUB for example)
  //! \param *pFile File name
  void SetFile(
    char *pDir,
    char *pFile
    );

protected:
  //! Pointer on the file name to modify
  char *pParam;

  // List of files in the directory
  std::vector<String> lstFiles;

  // Index of the selected file
  int idxFile;
};

//-------------------------------------------------------------------------
//! \class CModifAction
//! \brief Class allowing the modification of an action of a scenario
class CModifAction: public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! Constructor (initialization of parameters to default values)
  CModifAction();

  //-------------------------------------------------------------------------
  //! Destructor
  virtual ~CModifAction();
  
  //-------------------------------------------------------------------------
  //! Mode start
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! End of mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
    
  //-------------------------------------------------------------------------
  //! \brief Consistency between the different parameters
  //! \param iAction Index of affected action
  void Coherenceparams();

  //-------------------------------------------------------------------------
  //! \brief Save the current action in the list
  //! \param idxAct Index of affected action
  //! \return true if action is OK
  bool SaveAction(
    int idxAct
    );
    
  //-------------------------------------------------------------------------
  //! \brief Initializes the current action from the list
  //! \param idxAct Index of affected action
  void SetAction(
    int idxAct
    );
    
protected:
  //! Action being modified
  ActionScenario action;

  //! Line number of the action
  int iLine;

  //! Total number of lines in the action file
  int iNbLine;

  //! Boolean for del action
  bool bDel;

  //! Action time in hh:mm:ss for WAKEUP action
  char sTime[10];
};

#endif // CMODIFACTION_H
