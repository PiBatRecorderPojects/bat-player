/* 
 * File:   CModeGen.cpp
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CModeGeneric
  * -   CModeCopyright
  */
#include <U8g2lib.h>
#include "TimeLib.h"
#include <algorithm>
#include <EEPROM.h>
#include <Wire.h>
#include "SdFat.h"
#include "Const.h"
#include "CModeGen.h"
#include "CModeParFiles.h"
#include "CModePlay.h"
#include "RamMonitor.h"
#include <Snooze.h>
#include "CModifAction.h"
#include "CModeAuto.h"

// Gestionnaire écran
extern U8G2 *pDisplay;

// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef SD_FAT_TYPE
  extern SdFat sd;
#endif
#ifdef MMC_SDFAT
  extern SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  extern SdFatSdioEX sd;
#endif

// Ram monitoring (https://github.com/veonik/Teensy-RAM-Monitor)
RamMonitor ramMonitor;

// Gestionnaire du fichier LogPR.txt
extern LogFile logPR;

// Chaines de l'énuméré du menu des modes Play -> Files -> Params -> Play
extern const char **txtMODES[MAXLANGUAGE][3];
const char *txtIDXMODEF[]       = {"[%s]",
                                   "[%s]",
                                   "[%s]",
                                   "[%s]"};
// Chaines pour le log de changement de mode
extern const char *txtLogPlay[];
extern const char *txtLogFiles[];
extern const char *txtLogParams[];
extern const char *txtLogError[];
extern const char *txtLogIndet[];
extern const char *txtLogSFiles[];
extern const char *txtLogCopyright[];
extern const char *txtSWError[];
extern const char *txtLogModifAct[];
extern const char *txtLogSelBPS[];
extern const char *txtLogAutoMode[];
extern const char *txtLogStandby[];
extern const char *txtSDERROR[];
extern const char *txtErrorFileBPS[];
extern const char *txtLogErrOpenBPS[];
const char *txtAction[] = {"PLAYFILE", "PLAYDIR", "WAIT", "WAKEUP", "ERROR"};
const char *txtValid[]  = {"Valid", "Invalid"};

//----------------------------------------------------
// Function to create or activate a new mode
CGenericMode* CreateNewMode(
  CGenericMode* pOldMode,   // Pointer on the previous mode of operation
  int idxMode               // New mode of operation index
  )
{
  uint32_t TotalRam, FreeRam, HeapUsed, FreeRamP;
  CGenericMode* pNewMode = NULL;
  // Dynamic creation of operating modes
  // First, destruction of the previous mode to free memory
  if (pOldMode != NULL)
    delete pOldMode;
  // Second, creating the new mode of operation in memory
  ParamModes *pParams = CModeGeneric::GetParamsC();
  switch (idxMode)
  {
  case MPLAY   : pNewMode = new CModePlay();              LogFile::AddLog( LLOG, txtLogPlay      [pParams->iLanguage]);  break;
  case MFILES  : pNewMode = new CModeFiles();             LogFile::AddLog( LLOG, txtLogFiles     [pParams->iLanguage]);  break;
  case MBPSSEL : pNewMode = new CModeFiles(true);         LogFile::AddLog( LLOG, txtLogSelBPS    [pParams->iLanguage]);  break;
  case MPARAMS : pNewMode = new CModeParams();            LogFile::AddLog( LLOG, txtLogParams    [pParams->iLanguage]);  break;
  case MMODIFA : pNewMode = new CModifAction();           LogFile::AddLog( LLOG, txtLogModifAct  [pParams->iLanguage]);  break;
  case MAUTOP  : 
        // Vérification fichier scénario
        if (CModeGeneric::ReadScenarioFile(CModeGeneric::GetParamsC()->sScenarioName))
        {
          // Lecteur auto
          pNewMode = new CModeAuto();
          LogFile::AddLog( LLOG, txtLogAutoMode  [pParams->iLanguage]);
        }
        else
        {
          // Lecteur manuel
          Serial.printf("CreateNewMode MAUTOP but incorrect %s file !\n", CModeGeneric::GetParamsC()->sScenarioName);
          pNewMode = new CModePlay();
          LogFile::AddLog( LLOG, txtLogPlay      [pParams->iLanguage]);
        }
        break;
  case MCRIGHT : pNewMode = new CModeCopyright();         LogFile::AddLog( LLOG, txtLogCopyright [pParams->iLanguage]);  break;
  case MERROR  : pNewMode = new CModeError();             LogFile::AddLog( LLOG, txtLogError     [pParams->iLanguage]);  break;
  default      :                                          LogFile::AddLog( LLOG, txtLogIndet     [pParams->iLanguage]);
    // Erreur logiciel !
    CModeError::SetTxtError((char *)txtSWError[pParams->iLanguage]);
    pNewMode = new CModeError();
  }
  // Print ram usage after new (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
  TotalRam = 262144;
  FreeRam  = ramMonitor.free();
  HeapUsed = ramMonitor.heap_used();
  FreeRamP = FreeRam * 100 / TotalRam;
  LogFile::AddLog( LLOG, "Total Ram %d, free Ram %d (%d%%), heap used %d, stack used %d, unallocated %d", TotalRam, FreeRam, FreeRamP, HeapUsed, ramMonitor.stack_used(), ramMonitor.unallocated());
  return pNewMode;
}

/*-----------------------------------------------------------------------------
 Classe générique de gestion d'un mode de fonctionnement
 Définie l'interface minimale de gestion d'un mode de fonctionnement
 Gère le menu de changement de mode, l'état de la batterie et la luminosité écran
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
//! \param bparams true si avec les paramètres communs aux différents modes
CModeGeneric::CModeGeneric(
   bool bparams
  )
{
  //Serial.println("CModeGeneric constructeur");
  // Initialisation des paramètres
  iChargBat = 100;
  paramsC.bLumiere = false;
  if (strlen(paramsC.sCurrentDir) == 0)
  {
    strcpy( paramsC.sCurrentDir, "/");
    paramsC.iSelect = 0;
  }
  bModifParam = false;
  bParams = bparams;
  if (bParams)
  {
    // Création des modificateurs communs aux différents modes dans l'ordre de l'énuméré IDXPARAMS
    // Modificateur de l'état de la batterie
    LstModifParams.push_back(new CBatteryModifier( &(iChargBat), 25, 8, 76, 1));
    // Modificateur de luminosité
    LstModifParams.push_back(new CModificatorLight( &(paramsC.bLumiere), 108, 0));
    // Modificateur du mode de fonctionnement
    LstModifParams.push_back(new CEnumModifier( "[&$$$$$$$$$]", false, true, txtIDXMODEF, &(paramsC.iMode), IDXMODEMAX, (const char **)txtMODES, 0, 0));
    // On rend le paramètre de visualisation de la batterie non modifiable
    LstModifParams[IDXBAT]->SetEditable( false);
  }
  bADCBat = false;
  //Serial.println("CModeGeneric constructeur OK");
}

//-------------------------------------------------------------------------
// Destructeur
CModeGeneric::~CModeGeneric()
{
}

//-------------------------------------------------------------------------
// Début du mode
void CModeGeneric::BeginMode()
{
  //Serial.println("CModeGeneric::BeginMode");
  iYFirstLine = 10;
  // Par défaut, pas de lecture écriture des paramètres. Lecture et écritures à faire explicitement
  bWriteParams = false;
  bReadParams  = false;
  // Appel de la méthode parente
  CGenericMode::BeginMode();
  // Initialisation d'un éventuel MCP3221
  bADCBat = ADCBat.begin();
  if (bParams)
  {
    // On sélectionne par défaut le paramètre de changement de mode
    LstModifParams[idxSelParam]->SetbSelect( false);
    idxSelParam = IDXMODE;
    LstModifParams[IDXMODE]->SetbSelect( true);
    //Serial.println("CModeGeneric::BeginMode OK");
  }
  // Prochain temps de test des batteries
  TimeBat = millis() + 100;
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeGeneric::EndMode()
{
  // Appel de la méthode parente
  CGenericMode::EndMode();
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModeGeneric::OnEndChange(
  int idxModifier
  )
{
  iNewMode = NOMODE;
  if (bParams)
    // Selon le paramètre modifié
    switch (idxModifier)
    {
    case IDXMODE:
      // Changement de mode de fonctionnement Play -> Files -> Params -> Play (ou Auto)
      switch (paramsC.iMode)
      {
      case IDXMODEPLAY:
        // Vérification du mode
        if (paramsC.bModeAuto and strstr(paramsC.sScenarioName, ".bps") != NULL and strlen(paramsC.sScenarioName) > 4)
        {
          // Lecteur auto
          iNewMode = MAUTOP;
          Serial.println("CModeGeneric::OnEndChange MAUTOP");
        }
        else
        {
          if (paramsC.bModeAuto)
            Serial.printf("CModeGeneric::OnEndChange MAUTOP but incorrect %s file !\n", paramsC.sScenarioName);
          // Lecteur manuel
          iNewMode = MPLAY;
          Serial.println("CModeGeneric::OnEndChange MPLAY");
        }
        break;
      case IDXMODEFILES:
        iNewMode = MFILES;
        break;
      case IDXMODEPARAMS:
      default:
        iNewMode = MPARAMS;
      }
      break;
    default:
      // Appel de la fonction de base
      iNewMode = CGenericMode::OnEndChange( idxModifier);
    }
  else
    // Appel de la fonction de base
    iNewMode = CGenericMode::OnEndChange( idxModifier);
  return iNewMode;
}

//-------------------------------------------------------------------------
// Initialisation des paramètres par défaut
void CModeGeneric::SetDefault()
{
  Serial.println("CModeGeneric::SetDefault");
  paramsC.iMode = MFILES;
  paramsC.bLumiere = false;
  paramsC.iModePlay = PLAYONE;
  paramsC.iLanguage = LFRENCH;
  paramsC.iScreenType = ST_SH1106;
  paramsC.iSelect = 0;
  paramsC.sCurrentDir[0] = 0;
  paramsC.sWaveName[0] = 0;
  paramsC.sScenarioName[0] = 0;
  paramsC.bModeAuto = false;
  paramsC.bFiltre = false;
  paramsC.iDefGain = Z0DB;
}
  
//-------------------------------------------------------------------------
// Lecture des paramètres sauvegardés en EEPROM
// Retourne true si lecture OK
bool CModeGeneric::ReadParams()
{
  return StaticReadParams();
}

//-------------------------------------------------------------------------
// Reading saved settings in static function
bool CModeGeneric::StaticReadParams()
{
  bool bOK = false;
  // Vérification si les paramètres ont été écris une fois en EEPROM
  int address = 256;
  uint16_t uiControle = 0;
  char *p = (char *)&uiControle;
  for (int i = 0; i<(int)sizeof(uint16_t); i++)
    *p++ = EEPROM.read(address++);
  if (uiControle == 0x55AA)
  {
    // Lecture des paramètres aux adresses suivantes
    p = (char *)&paramsC;
    for (int i = 0; i<(int)sizeof(ParamModes); i++)
      *p++ = EEPROM.read(address++);
    // Vérification des paramètres
    if (paramsC.iMode < MPLAY or paramsC.iMode > MFILES)
      paramsC.iMode = MFILES;
    char *pBool = (char *)&paramsC.bLumiere;
    if (*pBool < 0 or *pBool > 1)
      paramsC.bLumiere = false;
    if (paramsC.iModePlay < PLAYONE or paramsC.iModePlay > PLAYFLOOP)
      paramsC.iModePlay = PLAYONE;
    if (paramsC.iLanguage < 0 or paramsC.iLanguage >= MAXLANGUAGE)
      paramsC.iLanguage = LFRENCH;
    if (paramsC.iScreenType < 0 or paramsC.iScreenType >= ST_MAX)
      paramsC.iScreenType = ST_SH1106;
    if (paramsC.bModeAuto < false or paramsC.bModeAuto > true)
      paramsC.bModeAuto = false;
    if (strlen(paramsC.sScenarioName) > 0 and !sd.exists(paramsC.sScenarioName))
    {
      Serial.printf("CModeGeneric::StaticReadParams error paramsC.sScenarioName [%s]\n", paramsC.sScenarioName);
      paramsC.sScenarioName[0] = 0;
    }
    // Always without filtre
    paramsC.bFiltre = false;
    if (paramsC.iDefGain < 0 or paramsC.iDefGain >= MAXDB)
      paramsC.iDefGain = Z0DB;
    // Init de la langue des modificateurs
    CGenericModifier::SetLanguage( paramsC.iLanguage);
    bOK = true;
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Test the existence of a directory and a file
//! \param *pDir  Dir name
//! \param *pFile File name (possibly NULL)
bool CModeGeneric::TestFiles(
  char *pDir,
  char *pFile
  )
{
  //Serial.printf("CModeGeneric::TestFiles( %d, %s)\n", pDir, pFile);
  bool bOK = true;
  // We position ourselves at the root of the map
  sd.chdir( "/");
  if (!sd.chdir( pDir))
  {
    // Invalid directory, the directory name is deleted
    Serial.printf("TestFiles, Invalid directory [%s]\n", pDir);
    pDir[0] = 0;
    bOK = false;
  }
  if (pFile != NULL and !sd.exists(pFile))
  {
    // Invalid file, the file name is deleted
    Serial.printf("TestFiles, Invalid file name [%s]\n", pFile);
    pFile[0] = 0;
    bOK = false;
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Initialisation du répertoire de départ
// Retourne true si OK
bool CModeGeneric::InitDirFile()
{
  bool bOK = true;
  // Controle du répertoire courant
  if (!sd.chdir( "/"))
    Serial.printf("CModeGeneric::InitDirFile erreur sur sd.chdir(/)\n");
  //Serial.printf("CModeGeneric::InitDirFile sCurrentDir [%s], sWaveName [%s]\n", paramsC.sCurrentDir, paramsC.sWaveName);
  if (!sd.chdir( paramsC.sCurrentDir))
  {
    // Répertoire courant incorrect, on part de la racine, pas de fichier sélectionné et forcément en mode Files
    Serial.printf("CModeGeneric::InitDirFile erreur sur sd.chdir(%s)\n", paramsC.sCurrentDir);
    strcpy( paramsC.sCurrentDir, "/");
    paramsC.sWaveName[0] = 0;
    paramsC.iSelect = 0;
    paramsC.iMode = MFILES;
    bOK = false;
  }
  else
  {
    // Vérification que le fichier wav existe
#ifdef SDFAT_FILE_TYPE
    FsFile wavfile;
#else
    File wavfile;
#endif
    if (wavfile.open( paramsC.sWaveName, O_READ))
      wavfile.close();
    else
    {
      // Fichier courant incorrect, RAZ du nom, pas de fichier sélectionné et forcément en mode Files
      paramsC.sWaveName[0] = 0;
      paramsC.iSelect = 0;
      paramsC.iMode = MFILES;
      bOK = false;
    }
  }
  return bOK;
}
  
//-------------------------------------------------------------------------
// Ecriture des paramètres sauvegardés en EEPROM
void CModeGeneric::WriteParams()
{
  StaticWriteParams();
}

//-------------------------------------------------------------------------
// Writing parameters to save them in static function
void CModeGeneric::StaticWriteParams()  
{
  //Serial.printf("WriteParams sCurrentDir [%s], sWaveName [%s]\n", paramsC.sCurrentDir, paramsC.sWaveName);
  // Sauvegarde systématique en utilisant la fonction update qui ne fait une écriture que si la valeur présente en EEPROM est différente de celle à mémoriser
  int address = 256; // Pour ne pas détruire les paramètres PassiveRecorder (address=0) sur la maquette utilisée par les 2 programmes
  uint16_t uiControle = 0x55AA;
  char *p = (char *)&uiControle;
  for (int i = 0; i<(int)sizeof(uint16_t); i++)
    EEPROM.update(address++, *p++);
  p = (char *)&paramsC;
  for (int i = 0; i<(int)sizeof(ParamModes); i++)
    EEPROM.update(address++, *p++);
}
  
//-------------------------------------------------------------------------
// Update screen (standard behavior = pDisplay->sendBuffer())
void CModeGeneric::UpdateScreen()
{
  // Test si la lecture des batteries doit être faite
  if (millis() > TimeBat)
  {
    // Lecture de l'état des batteries
    CheckBatteries();
    // Prochaine lecture dans 10s
    TimeBat = millis() + 10000;
  }
#if defined(__IMXRT1062__) // Teensy 4.1
  // For Teensy 4.1
  // Classic UpdateScreen = 37ms max at 280MHz
  // This Update Screen, 860µs for one updateDisplayArea
  //unsigned long uiTimePrint = micros();
  __disable_irq();
  pDisplay->updateDisplayArea( 0, 0, 8, 4);
  __enable_irq();
  //unsigned long uiTimePrintB = micros();
  delay(1); // To process a possible DMA interruption
  __disable_irq();
  pDisplay->updateDisplayArea( 8, 0, 8, 4);
  __enable_irq();
  delay(1); // To process a possible DMA interruption
  __disable_irq();
  pDisplay->updateDisplayArea( 0, 4, 8, 4);
  __enable_irq();
  delay(1); // To process a possible DMA interruption
  __disable_irq();
  pDisplay->updateDisplayArea( 8, 4, 8, 4);
  __enable_irq();
  //Serial.printf("updateDisplayArea T4.1 8x4 Elem %dµs, total %dµs\n", uiTimePrintB-uiTimePrint, micros()-uiTimePrint);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // A full display requires 33ms, the time between two sample processing interrupts (32ms to 384kHz).
  // Interruptions disrupts screen update
  // If interrupts are stopped, you may lose a sample buffer
  // By cutting the update in 4 zones for about 10ms each and blocking the interrupts on the display of each zone, we do not lose buffer
  // Stop interrupts, Display 1/4, Enables interrupts, delays 1ms ...
  // Warning, updateDisplayArea uses tile coordinates (8x8 pixels)
  // Classic UpdateScreen = 33ms max at 144MHz
  // This UpdateScreen = 7ms at 144MHz !!!
  //unsigned long uiTimePrint = millis();
  unsigned char sreg_backup = SREG;
  cli();
  pDisplay->updateDisplayArea( 0, 0, 8, 4);
  SREG = sreg_backup;
  delay(1); // To process a possible DMA interruption
  sreg_backup = SREG;
  cli();
  pDisplay->updateDisplayArea( 8, 0, 8, 4);
  SREG = sreg_backup;
  delay(1); // To process a possible DMA interruption
  sreg_backup = SREG;
  cli();
  pDisplay->updateDisplayArea( 0, 4, 8, 4);
  SREG = sreg_backup;
  delay(1); // To process a possible DMA interruption
  sreg_backup = SREG;
  cli();
  pDisplay->updateDisplayArea( 8, 4, 8, 4);
  SREG = sreg_backup;
  //Serial.printf("sendBuffer %dms\n", millis()-uiTimePrint); 
#endif
}

//-------------------------------------------------------------------------
// Returns the pointer to the common parameters
ParamModes *CModeGeneric::GetParamsC()
{
  return &paramsC;
}
  
//-------------------------------------------------------------------------
// List the wav files and directories of the current or root directory
void CModeGeneric::ListFiles(
  const char * dirname // Répertoire de départ des fichiers wav ou, si null, fichiers scénario bps de la racine
  )
{
  bool bWave = true;
  //Serial.printf("CModeGeneric::ListFiles [%s]\n", dirname);
  lstWaveFiles.clear();
  char name[80];
  char temp[80];
  SdFile file;
  // On sélectionne le répertoire
  if (dirname == NULL)
  {
    bWave = false;
    sd.chdir( "/");
  }
  else if (!sd.chdir( dirname))
    Serial.printf("CModeGeneric::ListFiles Erreur sd.chdir(%s)\n", dirname);
  // Si le fichier est différent de la racine, on ajoute /.. pour pouvoit remonter
  if (dirname != NULL and dirname[1] != 0)
    lstWaveFiles.push_back( "/..");
  // On recherche les fichiers du répertoire
#ifdef SD_FAT_TYPE
  FsFile root;
  if (dirname == NULL)
    root.open("/");
  else
    root.open(dirname);
#else
  sd.vwd()->rewind();
#endif
#ifdef SD_FAT_TYPE
  while (file.openNext(&root, O_RDONLY))
#else
  while (file.openNext(sd.vwd(), O_READ))
#endif
  {
    // Récupération du nom du fichier
    file.getName( name, 80);
    //Serial.println(name);
    // On ne prend pas en compte le répertoire "System Volume Information"
    if (strcmp( name, "System Volume Information") != 0)
    {
      if (file.isDir() and bWave)
      {
        // C'est un répertoire, ajout dans la liste
        sprintf( temp, "/%s", name);
        lstWaveFiles.push_back( temp);
      }
      else
      {
        // Test si fichier wav ou  bps
        strcpy( temp, name);
        strlwr( temp);
        if (bWave and strstr(temp, ".wav") != NULL)
          lstWaveFiles.push_back( name);
        else if (!bWave and strstr(temp, ".bps") != NULL)
          lstWaveFiles.push_back( name);
      }
    }
    file.close();
  }
  // Trie par ordre alphabétique croissant
  std::sort (lstWaveFiles.begin(), lstWaveFiles.end());
  /*Serial.println("CModeGeneric::ListFiles");
  for (int i=0; i<(int)lstWaveFiles.size(); i++)
  {
    Serial.print("[");
    Serial.print(lstWaveFiles[i]);
    Serial.println("]");
  }*/
}
    
//-------------------------------------------------------------------------
// Retourne la chaine à afficher en ne prenant que les n caractères les plus significatifs
// 123456789012345678901
//  FichierTestGrandRhinolophe.wav
//  rTestGrandRhinolophe
String CModeGeneric::PrepareStrFile(
  String str, // Chaine à vérifier
  int iMax    // Taille max du nom de fichier
  )
{
  String newStr = str;
  // Copie des 20 derniers caractères (sauf .wav)
  int imax  = iMax - 1;
  if ((int)newStr.length() > imax and (int)newStr.length() <= imax+4)
    // On enlève .wav
    newStr = newStr.substring( 0, imax);
  else if ((int)newStr.length() > imax)
  {
    // On enlève .wav
    newStr = newStr.substring( 0, newStr.length()-4);
    // On enlève les 1er caractères
    newStr = newStr.substring( newStr.length()-imax);
  }
  else if ((int)newStr.length() < imax)
  {
    // On complète a 15 caractéres éventuellement
    while ((int)newStr.length() < imax)
      newStr += " ";
  }
  //Serial.printf("PrepareStrFile(%s)=%s|\n", str.c_str(), newStr.c_str());
  return newStr;
}

//-------------------------------------------------------------------------
//! \brief Print Ram usage
//! \param pTxt Sting to print
void CModeGeneric::PrintRamUsage(
  char *pTxt
  )
{
  // Print ram usage after new (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
  //uint32_t TotalRam  = 262144;
  uint32_t TotalRam    = ramMonitor.total();
  uint32_t FreeRam     = ramMonitor.free();
  uint32_t TotalHeap   = ramMonitor.heap_total();
  uint32_t HeapUsed    = ramMonitor.heap_used();
  uint32_t HeapFree    = ramMonitor.heap_free();
  uint32_t StackUsed   = ramMonitor.stack_used();
  uint32_t FreeRamP    = FreeRam * 100 / TotalRam;
  int32_t  unallocated = ramMonitor.unallocated();
  bool bMemo = logPR.GetbConsole();
  logPR.SetbConsole( true);
  LogFile::AddLog( LLOG, "%s TOTAL RAM %d, FREE RAM %d (%d%%), HEAP total %d, used %d, free %d, STACK used %d, unallocated %d",
    pTxt, TotalRam, FreeRam, FreeRamP, TotalHeap, HeapUsed, HeapFree, StackUsed, unallocated);
  logPR.SetbConsole( bMemo);
}

// Liste des fichiers wav présents dans le répertoire courant
std::vector<String> CModeGeneric::lstWaveFiles;

//-------------------------------------------------------------------------
// Initialization function and calibration of the ADC0 to read the heterodyne frequency
void CModeGeneric::ADC0Init(
  bool bRefVCC  // true for VCC Ref, false for internal 1V2 ref
  )
{
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC1 = ADC0 T3.6 ***
  // Init de l'ADC1
  // 66.8.6 Configuration register (ADCx_CFG)
  ADC1_CFG = 0;                                                     // OVWREN disable overwriting, ADTRG Software trigger, REFSEL Selects VREFH/VREFL as reference voltage
                                                                    // ADHSC Normal conversion selected, ADLPC ADC hard block not in low power mode
  ADC1_CFG = ADC_CFG_MODE(2) | ADC_CFG_ADSTS(3) | ADC_CFG_ADLSMP    // 12 bits, 25 sample period, long sample time 
           | ADC_CFG_ADICLK(1) | ADC_CFG_ADIV(1) | ADC_CFG_AVGS(1); // IPG clock divided by 2, Input clock / 2, 8 samples averaged
  ADC1_CFG |= ADC_CFG_OVWREN;                                       // OVWREN enable overwriting
  ADC1_CFG |= ADC_CFG_ADLPC;                                        // ADC hard block in low power mode
  // 66.8.7 General control register (ADCx_GC)
  ADC1_GC &= ~ADC_GC_ACFE;                                          // Compare function disable
  ADC1_GC &= ~ADC_GC_ACFGT;                                         // Compare Function Greater Than disable
  ADC1_GC &= ~ADC_GC_ACREN;                                         // Range function disable
  ADC1_GC |= ADC_GC_AVGE;                                           // Averaging
  ADC1_GC &= ~ADC_GC_ADCO;                                          // Continuous Conversion disable
  ADC2_GC &= ~ADC_GC_DMAEN;                                         // DMA disable
  ADC2_GC &= ~ADC_GC_ADACKEN;                                       // ADACKEN Asynchronous clock output disable

  // Calibration ADC1
  ADC1_GC &= ~ADC_GC_CAL;                                           // Reset Cal bit
  ADC1_GS = ADC_GS_CALF;                                            // Reset calibration status
  ADC1_GC = ADC_GC_CAL;                                             // Start calibration
  while (ADC1_GC & ADC_GC_CAL);                                     // Wait for calibration
  if ((ADC1_GS & ADC_GS_CALF) > 0) Serial.println("CModeGeneric::ADC0Init Teensy 4.1 Error calibration");
  Serial.println("CModeGeneric::ADC0Init");
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Init de l'ADC0
  SIM_SCGC6 |= SIM_SCGC6_ADC0;
  ADC0_CFG1  = ADC_CFG1_ADIV(2) | ADC_CFG1_ADICLK(1) |                // Single-ended 12 bits, long sample time
               ADC_CFG1_MODE(1) | ADC_CFG1_ADLSMP;
  /*if (iPin == ADCPINBATEXT)
    ADC0_CFG2  = ADC_CFG2_MUXSEL | ADC_CFG2_ADLSTS(0);                // Select channels ADxxxb and Default longest sample time; 20 extra ADCK cycles; 24 ADCK cycles total
  else*/
  ADC0_CFG2  = ADC_CFG2_ADLSTS(0);                                    // Select channels ADxxxa and Default longest sample time; 20 extra ADCK cycles; 24 ADCK cycles total
  if (bRefVCC)
    ADC0_SC2   = ADC_SC2_REFSEL(0);                                   // Voltage reference = Vcc
  else
    ADC0_SC2   = ADC_SC2_REFSEL(1);                                   // Voltage ref internal (1V2), software trigger
  ADC0_SC3   = ADC_SC3_AVGE | ADC_SC3_AVGS(3);                        // Enable averaging, 32 samples
  // Calibration
  uint16_t sum;

  // Begin calibration
  ADC0_SC3 = ADC_SC3_CAL | ADC_SC3_AVGE | ADC_SC3_AVGS(3);
  // Wait for calibration
  while (ADC0_SC3 & ADC_SC3_CAL);

  // Plus side gain
  sum = ADC0_CLPS + ADC0_CLP4 + ADC0_CLP3 + ADC0_CLP2 + ADC0_CLP1 + ADC0_CLP0;
  sum = (sum / 2) | 0x8000;
  ADC0_PG = sum;

  // Minus side gain (not used in single-ended mode)
  sum = ADC0_CLMS + ADC0_CLM4 + ADC0_CLM3 + ADC0_CLM2 + ADC0_CLM1 + ADC0_CLM0;
  sum = (sum / 2) | 0x8000;
  ADC0_MG = sum;
#endif
}

//-------------------------------------------------------------------------
// Single-shot playback function of an analog input with ADC 0 to read the heterodyne frequency
// Returns the value read (0-4095)
uint16_t CModeGeneric::ADC0Read(
  int iPin  // Pin to read 
  )
{
  uint16_t value = 0;
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC1 = ADC0 T3.6 ***
	PROGMEM static const uint8_t adcT4_pin_to_channel[] = {
		7,      // 0/A0   AD_B1_02
		8,      // 1/A1   AD_B1_03
		12,     // 2/A2   AD_B1_07
		11,     // 3/A3   AD_B1_06
		6,      // 4/A4   AD_B1_01
		5,      // 5/A5   AD_B1_00
		15,     // 6/A6   AD_B1_10
		0,      // 7/A7   AD_B1_11
		13,     // 8/A8   AD_B1_08
		14,     // 9/A9   AD_B1_09
		255,	  // 10/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
		255,	  // 11/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
		3,      // 12/A12 AD_B1_14
		4,      // 13/A13 AD_B1_15
		7,      // 14/A0  AD_B1_02
		8,      // 15/A1  AD_B1_03
		12,     // 16/A2  AD_B1_07
		11,     // 17/A3  AD_B1_06
		6,      // 18/A4  AD_B1_01
		5,      // 19/A5  AD_B1_00
		15,     // 20/A6  AD_B1_10
		0,      // 21/A7  AD_B1_11
		13,     // 22/A8  AD_B1_08
		14,     // 23/A9  AD_B1_09
		255,    // 24/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
		255,    // 25/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
		255,    // 26/A12 AD_B1_14 - only on ADC2, do not use analogRead()
		255,    // 27/A13 AD_B1_15 - only on ADC2, do not use analogRead()
		255,    // 28
		255,    // 29
		255,    // 30
		255,    // 31
		255,    // 32
		255,    // 33
		255,    // 34
		255,    // 35
		255,    // 36
		255,    // 37
		255,    // 38/A14 AD_B1_12 - only on ADC2, do not use analogRead()
		255,    // 39/A15 AD_B1_13 - only on ADC2, do not use analogRead()
		9,      // 40/A16 AD_B1_04
		10,     // 41/A17 AD_B1_05
	};
  // Lecture ADC 1
  ADC1_HC0 = adcT4_pin_to_channel[iPin];                            // Select pin and start conversion
  while (!(ADC1_HS & ADC_HS_COCO0));                                // Wait end conversion
  value = ADC1_R0;
  Serial.printf("ADC0Read(%d)=%d\n", iPin, value);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy to ARM Pin Conversion Chart
  //                                A0  A1  A2  A3  A4  A5  A6  A7  A8  A9
  const uint8_t TsyADCtoARMpin[] = { 5, 14,  8,  9, 13, 12,  6,  7, 15,  4};
  // Selecting the Teensy pin and launching the conversion
  ADC0_SC1A = TsyADCtoARMpin[iPin];
  // Waiting end of conversion
  while ((ADC0_SC1A & ADC_SC1_COCO) == 0) ;
  // Reading the conversion
  value = ADC0_RA;
#endif
  return value;
}

//-------------------------------------------------------------------------
// Mesure de la batterie
bool CModeGeneric::CheckBatteries()
{
  bool bReturn = false;
  bool bADCInit = false;
  // Lecture de la tension batterie interne
  uint16_t uiVal;
  if (bADCBat)
  {
    // Présence d'un MCP3221, lecture de la tension
    fNivBatLiPo = ADCBat.getVoltage();
    //fNivBatLiPo += commonParams.dCorrectBat;
    Serial.printf("CModeGeneric::CheckBatteries MCP3221 %fV\n", fNivBatLiPo);
  }
  else
  {
    // Sinon, lecture via ADC interne processeur
    ADC0Init( false);
    bADCInit = true;
    uiVal = ADC0Read( PINA2_LIPO);
    // Calcul de la tension réelle (1.2 / 4096 * Diviseur pont de résistances (100k + 27k) / 27k)
    // Utilisation de la référence interne sinon la mesure n'est pas bonne avec la référence VCC (on n'utilise pas comme référence ce qu'on veut mesurer !)
    fNivBatLiPo = (double)uiVal * 0.0013780;
    //Serial.printf("CModeGeneric::CheckBatteries DAC interne %fV\n", fNivBatLiPo);
  }
  // Tableaux de charge par rapport à la tension d'une batterie LiPo
  // Source: Courbe de décharge d'une batterie Panasonic NRC18650B 3500mAh à 0.2C
  //                    100%  90%   80%   70%   60%   50%   40%   30%   20%   10%   5%    <1%
  //                    4,2   4.0   3,85  3,8   3,7   3.6   3.52  3.45  3.40  3.20  3.00  <2.90
  short TbPourcent[] = {100,  90,   80,   70,   60,   50,   40,   30,   20,   10,   5,    0};
  float TbVLiPo   [] = {4.18, 4.0,  3.85, 3.80, 3.70, 3.60, 3.52, 3.45, 3.40, 3.20, 3.00, 2.9};
  iNivBatLiPo = 0;
  for (int i=0; i<12; i++)
  {
    if (fNivBatLiPo >= TbVLiPo[i])
    {
      iNivBatLiPo = TbPourcent[i];
      break;
    }
  }
  
  if (!bADCInit)
    ADC0Init( false);
  // Lecture de la tension batterie externe
  uiVal = ADC0Read( PINA1_EXT);
  Serial.printf("ADC0Read = %d\n", uiVal);
  //uiVal = analogRead(PINA1_EXT);
  //uiVal = analogRead(A0);
  Serial.printf("analogRead = %d\n", uiVal);
#if defined(__IMXRT1062__) // Teensy 4.1 
  // Calcul de la tension réelle (3.3 / 4096 * Diviseur pont de résistance (100k + 5.6k) / 5.6k)
  // Sur Teensy 4.1, pas de référence interne, c'est forcément le 3.3V qui sert de référence
  fNivBatExt = (double)uiVal * 0.0151925;
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Calcul de la tension réelle (1.2 / 4096 * Diviseur pont de résistance (100k + 5.6k) / 5.6k)
  // Utilisation de la référence interne sinon la mesure n'est pas bonne avec la référence VCC (on n'utilise pas comme référence ce qu'on veut mesurer !)
  fNivBatExt = ((double)uiVal * 0.0055246);
#endif
  // Tableaux de charge par rapport à la tension d'une batterie plomb externe
  // 100%  90%   80%   70%   60%   50%  40%   30%   20%   10%   1%   <1%
  // 14    13,66 13,32 12,98 12,64 12,3 11,94 11,58 11,22 10,86 10,5 <10.5
  double TbVPlomb [] = {14, 13.66, 13.32, 12.98, 12.64, 12.3, 11.94, 11.58, 11.22, 10.86, 10.5, 0};
  iNivBatExt = 0;
  for (int i=0; i<12; i++)
  {
    if (fNivBatExt >= TbVPlomb[i])
    {
      iNivBatExt = TbPourcent[i];
      break;
    }
  }
  Serial.printf("fNivBatLiPo %f, fNivBatExt %f\n", fNivBatLiPo, fNivBatExt);
  // Retourne true uniquement si une batterie est à plus de 10% de charge
  if (iNivBatLiPo > 10 or iNivBatExt > 10)
    bReturn = true;
  return bReturn;
}

//-------------------------------------------------------------------------
//! \brief Reading a scenario file
//! \param *pFileName File name
bool CModeGeneric::ReadScenarioFile(
  char *pFileName
  )
{
  //Serial.printf("ReadScenarioFile(%s)\n", pFileName);
  bool bOK = true;
  SdFile dataFile;
  char sLine[256];
  int iNbChar;
  ActionScenario action;
  // Reset the list of actions
  lstScenario.clear();
  // Select root SD
  sd.chdir( "/");
  // Open file
  if (sd.exists(pFileName) and dataFile.open( pFileName, O_READ))
  {
    // Reading the lines of the file
    //Serial.printf("ReadScenarioFile, Lecture %s\n", pFileName);
    int l=0;
    do
    {
      // Lecture d'une ligne
      iNbChar = dataFile.fgets(sLine, 255);
      if (iNbChar > 0)
      {
        l++;
        //Serial.printf("ReadScenarioFile, reading %d char\n", iNbChar);
        // If it's not a comment line
        //if (sLine[0] != ';' and strlen(sLine) > 10 and strstr(sLine, ",") != NULL)
        if (strlen(sLine) > 10 and strstr(sLine, ",") != NULL)
        {
          // Line information decoding
          if (!DecodeLine( &action, sLine))
            // Erreur, on ne prend pas en compte la ligne
            LogFile::AddLog( LLOG, txtErrorFileBPS[paramsC.iLanguage], pFileName, l, sLine);
          else
          {
            // Ajout de l'action dans la liste
            lstScenario.push_back( ActionScenario());
            int i = lstScenario.size()-1;
            lstScenario[i].iAction         = action.iAction;
            lstScenario[i].bComment        = action.bComment;
            strcpy( lstScenario[i].sTime,    action.sTime);
            strcpy( lstScenario[i].sDirPath, action.sDirPath);
            strcpy( lstScenario[i].sWavFile, action.sWavFile);
          }
        }
      }
    }
    while (iNbChar > 0);
    dataFile.close();
    if (lstScenario.size() == 0)
      bOK = false;
    //LogFile::AddLog( LLOG, "Reading the scenario file %s, %d line", pFileName, lstScenario.size());
    /*for (int i=0; i<(int)lstScenario.size(); i++)
    {
      sprintf( sLine, "Line %d:", i+1);
      PrintAction( sLine, &(lstScenario[i]));
    }*/
  }
  else
  {
    LogFile::AddLog( LLOG, txtLogErrOpenBPS[paramsC.iLanguage], pFileName);
    bOK = false;
  }
    
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Writing a scenario file
//! \param *pFileName File name
bool CModeGeneric::WriteScenarioFile(
  char *pFileName
  )
{
  Serial.printf("WriteScenarioFile %s\n", pFileName);
  bool bOK = true;
#ifdef SD_FAT_TYPE
  FsFile dataFile;  // Info backup file
#else
  SdFile dataFile;
#endif
  char sLine[256];
  // Select root SD
  sd.chdir( "/");
  // Open file
  if (dataFile.open( pFileName, O_CREAT | O_WRITE))
  {
    for (int i=0; i<(int)lstScenario.size(); i++)
    {
      sprintf(sLine, "WriteScenarioFile %d", i);
      PrintAction(sLine, &lstScenario[i]);
      if (CodeLine( &lstScenario[i], sLine))
        dataFile.printf( sLine);
      else
      {
        bOK = false;
        break;
      }
    }
    dataFile.close();
  }
  else
    LogFile::AddLog( LLOG, "CModeGeneric::WriteScenarioFile impossible to open %s", pFileName);
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Automatic addition of a new scenario file
void CModeGeneric::AddNewBPS()
{
  bool bOK = false;
  // Reset the list of actions
  lstScenario.clear();
  // Select root SD
  sd.chdir( "/");
  // Finding a name that does not exist
  char fileBPS[50];
  for (char c='A'; c<='Z'; c++)
  {
    sprintf( fileBPS, "Scenario%c.bps", c);
    if (!sd.exists(fileBPS))
    {
      // Select this name
      strcpy( paramsC.sScenarioName, fileBPS);
      bOK = true;
      break;
    }
  }
  if (!bOK)
    // Select this name
    strcpy( paramsC.sScenarioName, "Scenarioxx.bps");
}
  
//-------------------------------------------------------------------------
//! \brief Decoding the fields of a row
//! \param *pAction Parameters of the action after decoding
//! \param *pLine Line to decode
//! \return true si OK
bool CModeGeneric::DecodeLine(
  ActionScenario *pAction,
  char *pLine
  )
{
/* ; Structure of a scenario file. The extension is .bps
 * ; Comments are possible by starting the line with ;
 * ; Empty lines are allowed
 * ; At the end of the file, we start again with the action from the beginning
 * 
 * ; It is possible to edit the scenario files directly with the BatPlayer software but also with a text editor on a computer..
 * 
 * ; List of possible actions and associated parameters
 * ; An action is composed of a type of action (PLAYFILE, PLAYDIR, WAIT or WAKEUP), 
 * ; a duration or an hour (WAKEUP) in the format hh: mm: ss (hour: minutes: seconds), 
 * ; a directory (PLAYDIR) or a path file (PLAYFILE)
 * 
 * ; Reading a file. If the duration is 0, read the entire file and go to the next action.
 * ; If the duration is different from 0, the file is either truncated or repeated to reach the duration.
 * PLAYFILE,00:00:00,/Barba/Barba.wav
 * 
 * ; Reading wav files from a directory.
 * ; If the duration is 0, read the files in the directory and go to the next action. Files are played in alphabetical order.
 * ; If the duration is different from 0, read the files in the directory until the duration is reached.
 * PLAYDIR,00:00:00,/Barba
 * 
 * ; Waiting for a duration. The BatPlayer goes into standby mode while waiting
 * WAIT,00:00:00
 * 
 * ; Go to sleep and wake up at the specified time
 * WAKEUP,00:00:00
 * 
 */
  bool bOK = true;
  int iNbField = 0, idx = 0;
  String sLine = pLine;
  String sFields[3];
  //Serial.printf("DecodeLine [%s]\n", pLine);
  // Init to default values
  pAction->iAction = SC_ERROR;
  pAction->sTime[0] = 0;
  pAction->sDirPath[0] = 0;
  pAction->sWavFile[0] = 0;
  pAction->bComment = false;
  if (sLine[0] == ';')
  {
    // Line in comment
    pAction->bComment = true;
    sLine = &pLine[1];
    //Serial.printf("DecodeLine ligne [%s] en commentaire, new line [%s]\n", pLine, sLine.c_str());
  }
  // We are looking for the 3 possible fields
  do
  {
    idx = sLine.indexOf(',');
    if (idx >= 0)
    {
      sFields[iNbField] = sLine.substring( 0, idx);
      sLine = sLine.substring( idx+1);
      iNbField++;
      if (iNbField >= 3)
        idx = 0;
    }
  }
  while (idx >= 0);
  // Retrieving the last field
  sFields[iNbField] = sLine.substring( 0, sLine.length()-1);
  iNbField++;
  /*Serial.printf("%d champs trouvés\n", iNbField);
  for (int i=0; i<iNbField; i++)
    Serial.printf("Champ %d [%s]\n", i+1, sFields[i].c_str());*/
  // Field decoding
  if (iNbField > 0)
  {
    // Decoding of the 1st compulsory field (Type of action)
    for (int i=0; i<SC_ERROR; i++)
    {
      if (sFields[0] == txtAction[i])
      {
        pAction->iAction = i;
        break;
      }
    }
  }
  else
  {
    if (!pAction->bComment)
      Serial.printf("DecodeLine, field 1 absent ! line [%s]\n", pLine);
    bOK = false;
  }
  if (bOK and iNbField > 1)
  {
    // Decoding of the second mandatory field (00:00:00 duration or hour)
    if (sFields[1].length() != 8)
      bOK = false;
    else
    {
      for (int i=0; i<8; i++)
      {
        if (i == 2 or i == 5)
        {
          if (sFields[1].charAt(i) != ':')
            bOK = false;
        }
        else
        {
          if (sFields[1].charAt(i) < '0' or sFields[1].charAt(i) > '9')
            bOK = false;
        }
      }
    }
    if (!bOK)
      Serial.printf("DecodeLine error field 2 [%s] line [%s]\n", sFields[1].c_str(), pLine);
    else
      strcpy( pAction->sTime, sFields[1].c_str());
  }
  else if (bOK)
  {
    Serial.printf("DecodeLine, filed 2 absent ! line [%s]\n", pLine);
    bOK = false;
  }
  if (bOK and pAction->iAction < ACTWAIT and iNbField > 2)
  {
    // Decoding of the third field (directory or path file)
    strcpy( pAction->sDirPath, sFields[2].c_str());
    // For PLAYDIR, checking that the directory exists
    if (pAction->iAction == PLAYDIR and !TestFiles( pAction->sDirPath, NULL))
    {
      // Invalid directory
      pAction->iAction = SC_ERROR;
      Serial.printf("DecodeLine error field 3 [%s], line [%s]\n", sFields[2].c_str(), pLine);
      bOK = false;
    }
    else if (pAction->iAction == PLAYFILE)
    {
      // Splitting the field to retrieve the directory and the file name
      idx = sFields[2].lastIndexOf('/');
      if (idx >= 0)
      {
        pAction->sDirPath[idx] = 0;
        strcpy(pAction->sWavFile, &(sFields[2].c_str()[idx+1]));
      }
      else
      {
        Serial.printf("DecodeLine field 3, error path file ! line [%s]\n", pLine);
        bOK = false;
      }
    }
  }
  else if (bOK and pAction->iAction < ACTWAIT)
  {
    Serial.printf("DecodeLine field 3 absent ! line [%s]\n", pLine);
    bOK = false;
  }
  //if (bOK) PrintAction((char *)"Decoding:", pAction);
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Decoding the fields of a row
//! \param *pAction Parameters of the action after decoding
//! \return true si OK
bool CModeGeneric::VerifyAction(
  ActionScenario *pAction
  )
{
  bool bOK = true;
  // Verify time
  if (strlen(pAction->sTime) != 8)
  {
    bOK = false;
    Serial.printf("VerifyAction strlen(sTime) != 8 [%s]\n", pAction->sTime);
  }
  else
  {
    for (int i=0; i<8; i++)
    {
      if (i == 2 or i == 5)
      {
        if (pAction->sTime[i] != ':')
        {
          Serial.printf("VerifyAction sTime : absent [%s]\n", pAction->sTime);
          bOK = false;
        }
      }
      else
      {
        if (pAction->sTime[i] < '0' or pAction->sTime[i] > '9')
        {
          Serial.printf("VerifyAction sTime : chiffre [%s]\n", pAction->sTime);
          bOK = false;
        }
      }
    }
  }
  if (bOK)
  {
    // Verify action
    switch (pAction->iAction)
    {
    case PLAYFILE:
      if (TestFiles( pAction->sDirPath, pAction->sWavFile))
        bOK = true;
      else
        Serial.printf("VerifyAction TestFiles = false [%s %s]\n", pAction->sDirPath, pAction->sWavFile);
      break;
    case PLAYDIR :
      bOK = sd.exists( pAction->sDirPath);
      if (!bOK)
        Serial.printf("VerifyAction pb path [%s %s]\n", pAction->sDirPath);
      break;
    case ACTWAIT :
    case WAKEUP  :
      break;
    default      :
      Serial.printf("VerifyAction pb action [%d]\n", pAction->iAction);
      bOK = false;
    }
  }
  return bOK;
}
    
//-------------------------------------------------------------------------
//! \brief Coding of the fields of a row
//! \param *pAction Parameters of the action to be coded
//! \param *pLine Line to code
//! \return true si OK
bool CModeGeneric::CodeLine(
  ActionScenario *pAction,
  char *pLine
  )
{
  char sLine[256];
  bool bOK = true;
  switch (pAction->iAction)
  {
  case PLAYFILE:
    sprintf( sLine, "%s,%s,%s/%s\n", txtAction[pAction->iAction], pAction->sTime, pAction->sDirPath, pAction->sWavFile);
    break;
  case PLAYDIR:
    sprintf( sLine, "%s,%s,%s\n", txtAction[pAction->iAction], pAction->sTime, pAction->sDirPath);
    break;
  case ACTWAIT:
  case WAKEUP:
    sprintf( sLine, "%s,%s\n", txtAction[pAction->iAction], pAction->sTime);
    break;
  default:
    bOK = false;
    sprintf( sLine, "%s\n", txtAction[SC_ERROR]);
  }
  if (pAction->bComment)
    strcpy(pLine, ";");
  else
    pLine[0] = 0;
  strcat(pLine, sLine);
  return bOK;
}
    
//-------------------------------------------------------------------------
//! \brief Return action in a string
//! \param *sTxt String to initialize
//! \param *iSize string size
//! \param *pAction Action to print
void CModeGeneric::GetStringAction(
  char *sTxt,
  int iSize,
  ActionScenario *pAction
  )
{
  char sTemp[128];
  switch (pAction->iAction)
  {
  case PLAYFILE:
    sprintf( sTemp, "%s,%s,%s/%s,%s", txtAction[pAction->iAction], pAction->sTime, pAction->sDirPath, pAction->sWavFile, txtValid[pAction->bComment]);
    break;
  case PLAYDIR:
    sprintf( sTemp, "%s,%s,%s,%s", txtAction[pAction->iAction], pAction->sTime, pAction->sDirPath, txtValid[pAction->bComment]);
    break;
  case ACTWAIT:
  case WAKEUP:
    sprintf( sTemp, "%s,%s,%s", txtAction[pAction->iAction], pAction->sTime, txtValid[pAction->bComment]);
    break;
  default:
    sprintf( sTemp, "%s", txtAction[SC_ERROR]);
  }
  if ((int)strlen(sTemp) < iSize-1)
    strcpy(sTxt, sTemp);
  else
  {
    strncpy( sTxt, sTemp, iSize-1);
    sTxt[iSize] = 0;
  }
}

//-------------------------------------------------------------------------
//! \brief Display of an action on the debug serial link
//! \param Text at the beginning of the line
//! \param *pAction Action to print
void CModeGeneric::PrintAction(
  char *sTxt,
  ActionScenario *pAction
  )
{
  char sTemp[128];
  GetStringAction( sTemp, 128, pAction);
  Serial.printf( "%s: %s\n", sTxt, sTemp);
}

//-------------------------------------------------------------------------
//! \brief Function test of the SD card. Resets the card if necessary and goes into error if the card stops responding
//! \return true if OK, false if SD error
bool CModeGeneric::isSDInit()
{
  bool bOK = true;
  // Warning, possible SD error (freeClusterCount = 0!)
  if (sd.vol()->freeClusterCount() == 0)
  {
    // Trace of the problem
    //Serial.printf("CModeVeille::isSDInit SD loss !");
#ifdef MMC_SDFAT
    if (!sd.begin())
#endif
#ifdef MMC_SDFAT_EXT
    if (!sd.begin())
#endif
#ifdef SD_FAT_TYPE
    if (!sd.begin(SdioConfig(FIFO_SDIO)))
#endif
      bOK = false;
  }
  if (!bOK)
  {
    // SD access problem
    CModeError::SetTxtError((char *)txtSDERROR[paramsC.iLanguage]);
    iNewMode = MERROR;
  }
  return bOK;
}

// Paramètres communs des modes
ParamModes CModeGeneric::paramsC;

// Tension de la batterie LiPo interne en V
double CModeGeneric::fNivBatLiPo;
// Tension de la batterie externe en V
double CModeGeneric::fNivBatExt;
// Niveau de charge de la batterie LiPo interne en %
short CModeGeneric::iNivBatLiPo;
// Niveau de charge de la batterie externe en %
short CModeGeneric::iNivBatExt;
// Mode debug
bool CModeGeneric::bDebug = false;
//! Liste des actions du scénario courant
std::vector<ActionScenario> CModeGeneric::lstScenario;
//! Indique qu'un MCP3221 est présent pour la meure de la tension batterie
bool CModeGeneric::bADCBat = false;
//! Gestionnaire d'un éventuel MCP3221 pour la mesure de la tension batterie
CADCVoltage CModeGeneric::ADCBat;
//! Indique une sélection de fichier
bool CModeGeneric::bNewSelFile = false;

//-------------------------------------------------------------------------
//! \class CModeCopyright
//! \brief Classe d'affichage des copyright

//-------------------------------------------------------------------------
//! \brief Constructeur
CModeCopyright::CModeCopyright()
{
}

//-------------------------------------------------------------------------
//! \brief Début du mode
void CModeCopyright::BeginMode()
{
  // Appel de la méthode parente
  CModeGeneric::BeginMode();
  // Init de la seconde courante à 61 pour forcer le réaffichage
  iCurrentSecond = 61;
  LogFile::AddLog( LLOG, "TeensyRecorder %s, Copyright 2018, Jean-Do. VRIGNAULT,  https://framagit.org/PiBatRecorderPojects/TeensyRecorders", VERSION);
}

//-------------------------------------------------------------------------
//! \brief Affichage du mode sur l'écran
//! Cette méthode est appelée régulièrement par le loop principal
//! à charge des différents modes d'afficher les informations nécessaires
void CModeCopyright::PrintMode()
{
  // Affichage toutes les secondes
  if (iCurrentSecond != second())
  {
    char Message[25];
    sprintf(Message, "BatPlayer %s", VERSION);
    iCurrentSecond = second();
    pDisplay->setFont(u8g2_font_6x13B_mf);  // Hauteur 12
    pDisplay->clearBuffer();
    pDisplay->setCursor(0,24);
    pDisplay->println(" Jean-Do. VRIGNAULT");
    pDisplay->setCursor(0,12);
    pDisplay->println("   Copyright 2018");
    pDisplay->setCursor(0,0);
    pDisplay->println(Message);
    pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 9
    pDisplay->setCursor(0,36);
    // https://framagit.org/PiBatRecorderPojects/bat-player
    //                 123456789012345678901
    pDisplay->println("https://framagit.org/");
    pDisplay->setCursor(0,45);
    pDisplay->println("/PiBatRecorderPojects");
    pDisplay->setCursor(0,54);
    pDisplay->println("/bat-player");
    pDisplay->sendBuffer();
  }
}

//-------------------------------------------------------------------------
//! \brief  Keyboard order processing
//! If the key is a mode change key, returns the requested mode
//! This method is called regularly by the main loop
//! By default, handles modifiers
//! \param[in] key Touch to treat
//! \return If a mode change, returns the requested mode, else return NOMODE
int CModeCopyright::KeyManager(
  unsigned short key
  )
{
  int iReturn = iNewMode;
  if (key != K_NO)
    // Sur toutes les touches, on passe en mode paramètres
    iReturn = MPARAMS;
  return iReturn;
}

//-------------------------------------------------------------------------
//! \class CADCVoltage
//! \brief Management class of an MCP3221 for reading battery voltages

#define MCP3X21_DEFAULT_ADDRESS 0x4D

#define MCP3221_CONVERSE 0x9B //10011011 NOTE IT ENDS IN 1, this is the READ ADDRESS. This is all this device does.
                                 //It opens a conversation via this specific READ address

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CADCVoltage::CADCVoltage()
{
  bInitOK = false;
}

//-------------------------------------------------------------------------
//! \brief Test if the MCP3221 is present
//! \return True if a MCP3221 is present
bool CADCVoltage::begin()
{
  bInitOK = false;
  // Test si présence MCP3221
  Wire.beginTransmission( MCP3X21_DEFAULT_ADDRESS);
  byte error = Wire.endTransmission();
  //Serial.printf("CADCVoltage::begin error %d\n", error);
  if (error == 0)
    bInitOK = true;
  return bInitOK;  
}

//-------------------------------------------------------------------------
//! \brief Read humidity
//! \return Batterie voltage in volt
float CADCVoltage::getVoltage()
{
  uint32_t uiT = micros();
  uint32_t data = 0;
  int iMax = 2;
  // n lectures puis moyenne
  for (int i=0; i<iMax; i++)
    data += read();
  data  = data / iMax;
  // VRef = 2.8V, Max ADC = 4095, resistor divisor is 2
  // float V = (2.8 * (float)data / 4095.0) * 2;
  float V = (float)data * 2.0 / 1000.0;
  //Serial.printf("getVoltage data %d, %4.1fV, %dµs\n", data, V, micros()-uiT);
  return V;
}

//-------------------------------------------------------------------------
//! \brief Read ADC MCP3221
//! \return ADC value
uint16_t CADCVoltage::read()
{
  char data[2];
  // Start I2C Transmission
  Wire.beginTransmission(MCP3X21_DEFAULT_ADDRESS);
  // Send a byte to start the conversation. It should be acknowledged
  Wire.write(MCP3221_CONVERSE);
  // Stop I2C Transmission
  Wire.endTransmission();
    
  // Request 2 bytes of data
  Wire.requestFrom(MCP3X21_DEFAULT_ADDRESS, 2);
    
  // Read 2 bytes of data
  data[0] = Wire.read();
  data[1] = Wire.read();
 
  //convert to 12 bit.
  uint16_t res;
  int _12_bit_var; // 2 bytes
  char _4_bit_MSnibble = data[0]; // 1 byte, example 0000 1000
  char _8_bit_LSByte   = data[1]; // 1 byte, example 1111 0000

  _12_bit_var = ((0x0F & _4_bit_MSnibble) << 8) | _8_bit_LSByte;   //example 100011110000
  res = _12_bit_var;

  return  res;
}
