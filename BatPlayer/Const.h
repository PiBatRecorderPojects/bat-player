/* 
 * File:   Const.h
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ; Structure of a scenario file. The extension is .bps
 * ; Comments are possible by starting the line with ;
 * ; Empty lines are allowed
 * ; At the end of the file, we start again with the action from the beginning
 * 
 * ; It is possible to edit the scenario files directly with the BatPlayer software but also with a text editor on a computer..
 * 
 * ; List of possible actions and associated parameters
 * ; An action is composed of a type of action (PLAYFILE, PLAYDIR, WAIT or WAKEUP), 
 * ; a duration or an hour (WAKEUP) in the format hh: mm: ss (hour: minutes: seconds), 
 * ; a directory (PLAYDIR) or a path file (PLAYFILE)
 * 
 * ; Reading a file. If the duration is 0, read the entire file and go to the next action.
 * ; If the duration is different from 0, the file is either truncated or repeated to reach the duration.
 * PLAYFILE,00:00:00,/Barba/Barba.wav
 * 
 * ; Reading wav files from a directory.
 * ; If the duration is 0, read the files in the directory and go to the next action. Files are played in alphabetical order.
 * ; If the duration is different from 0, read the files in the directory until the duration is reached.
 * PLAYDIR,00:00:00,/Barba
 * 
 * ; Waiting for a duration. The BatPlayer goes into standby mode while waiting
 * WAIT,00:00:00
 * 
 * ; Go to sleep and wake up at the specified time
 * WAKEUP,00:00:00
 * 
 */


// Constantes du projet

#ifndef CONST_H
#define CONST_H

#define VERSION "V0.30"
 /* V0.30 - Version for Teensy 3.6 and 4.1
  * - Using Teensyduino 1.59.0
  * - Using Arduino IDE 2.3.1
  * - On selection of a file, in reading mode, automatic positioning of the cursor on Reading
  * - Auto mode, gain reading correction (Thanks Lionel)
  * - Teensy 4.1 card management
  * - Battery voltage, MCP3221 use and check every 10s
  * - Saturation threshold at 4095 on T3.6 and +/-8191 on T4.1
  * - Fixed saving gain file of a wav file
 */
 /* V0.2 - Version with the following modifications
  * - Use of the ModesModiers library
  * - Management of wav files including with the uppercase extension
  * - Adding a Copyright menu
  * - Addition of "ConstChar.c" for multilingual management
  * - Addition of German and Dutch languages
  * - Set U8G2 V2.28.10
  * - Set SDFAT V2.0.4 for working with SDXC in FAT64 (exFAT). Formatting to FAT32 is no longer necessary
  * - Added automatic play of scenarios
  */
/* V0.11 - Version with the following modifications
 * - Management of several types of screens (SSD1306 et SH1106)
 * - Buffer of 4096 so as not to exceed 5ms of IT processing in CReader.cpp, otherwise, some screens do not like
 */
// V0.10 - Initial version of the BatPlayer

#define MAXLINESWAVE 6  // Nombre de lignes d'affichage des wav dans le mode MFILES

// Operating modes
enum MODEFONC {
  MPLAY   = 1,  // Playback mode of a recorded file
  MFILES  = 2,  // Select a wav file mode
  MPARAMS = 3,  // Parameters mode
  MCRIGHT = 4,  // Copyright mode
  MMODIFA = 5,  // Mode of modification of the scenario file
  MBPSSEL = 6,  // Scenario file selection mode
  MAUTOP  = 7,  // Automatic scenario reading mode
  MERROR  = 8,  // Error mode
  NOMODE  = 9,  // Indeterminate mode
};

// Enuméré du choix du mode de fonctionnement
enum ENUMMODEFONC {
  IDXMODEPLAY   = 0,  // Mode d'écoute d'un fichier enregistré
  IDXMODEFILES  = 1,  // Mode de sélection d'un fichier
  IDXMODEPARAMS = 2,  // Mode paramètres
  IDXMODEMAX    = 3   // Mode indéterminé
};

// Type d'écran
enum SCREENTYPE {
  ST_SSD1306 = 0,
  ST_SH1106  = 1,
  ST_MAX     = 2
};

// Action type of a scenario
enum ACTIONTYPE {
  PLAYFILE  = 0,
  PLAYDIR   = 1,
  ACTWAIT   = 2,
  WAKEUP    = 3,
  SC_ERROR  = 4,
  ACTIONMAX = 5
};

// Parameters of a timeline
struct ActionScenario
{
  int  iAction;                            // Type de l'action (enum ACTIONTYPE)
  char sTime[10];                          // Duration or start time of the action (HH:MM:SS)
  char sDirPath[255];                      // Directory path
  char sWavFile[255];                      // Wav file name
  bool bComment;                           // Action in comment
};

// Paramètres communs aux modes
struct ParamModes
{
  int  iMode;                              // Mode courant
  bool bLumiere;                           // Luminositée écran (false=normal, true=lumière faible)
  char sCurrentDir[255];                   // Répertoire courant
  char sWaveName[255];                     // Nom du fichier wav courant
  int  iSelect;                            // Indice du fichier sélectionné
  int  iModePlay;                          // Mode de lecture
  int  iLanguage;                          // Langue utilisée
  int  iScreenType;                        // Type d'écran utilisé
  bool bModeAuto;                          // Mode de lecture manuel (false) ou auto (true)
  char sScenarioName[255];                 // Nom du fichier de scénario à jouer
  bool bFiltre;                            // Avec ou sans filtre passe haut
  int  iDefGain;                           // Gain par défaut
};

// Paramètres spécifiques du mode Play
struct ParamModePlay
{
  int  iEtatPlay;        // Etat du mode Play (0=pause, 1=lecture)
  int  iPosPlay;         // Position en secondes de la lecture du fichier courant
  int  iDurPlay;         // Durée max en secondes du fichier courant
  int  iPosPlayPc;       // Position en pourcent de la lecture du fichier courant
  int  iFe;              // Fréquence d'échantillonnage du fichier courant en kHz
  char cSaturation;      // Indique si le fichier en lecture présente une saturation 12 bits
  int  iOptimisation;    // Atténuation éventuelle à utiliser lors de la lecture du fichier
  int  iOptimFiltre;     // Atténuation éventuelle avec filtre à utiliser lors de la lecture du fichier
};

// Index des paramètres communs aux modes
enum IDXPARAMSCOM {
  IDXBAT    = 0,  // Etat batterie
  IDXLUM    = 1,  // Luminosité
  IDXMODE   = 2   // Mode de fonctionnement
};

// Index des paramètres spécifiques du mode Play
enum IDXPARAMSPLAY {
  IDXPLAY   =  3,  // Start/Stop lecture
  IDXBARRE  =  4,  // Barre graphe d'avancement de la lecture
  IDXTMAX   =  5,  // Durée du fichier wav
  IDXTPOS   =  6,  // Positition temporelle de la lecture
  IDXFILE   =  7,  // Nom du fichier wav
  IDXFE     =  8,  // Fréquence d'échantillonnage du fichier
  IDXMPLAY  =  9,  // Mode de lecture des fichiers
  IDXDIR    = 10,  // Nom du répertoire courant
  IDXATTEN  = 11,  // Atténuation du fichier sélectionné
  IDXATTENF = 12,  // Atténuation avec filtre du fichier sélectionné
  IDXFLT    = 13   // Avec ou sans filtre
};

// Index des paramètres spécifiques du mode Scénario
enum IDXPARAMSAUTO {
  IDXSCPLAY   =  3,  // Start / Stop reading
  IDXSCBARRE  =  4,  // Reading progress bar
  IDXSCTMAX   =  5,  // Length of wav file
  IDXSCTPOS   =  6,  // Playback time position
  IDXSCFILE   =  7,  // Name of the wav file
  IDXSCFE     =  8,  // File sampling frequency
  IDXSCMPLAY  =  9,  // File playback mode
  IDXSCDIR    = 10,  // Current directory name
  IDXSCATTEN  = 11,  // Attenuation of the selected file
  IDXSCPARAMS = 12,  // Button to go back to parameters mode (instead of the standard button to go directly to parameters)
  IDXSCLINE   = 13,  // Line number in execution
  IDXSNBL     = 14,  // Number of lines to run
  IDXSCDUR    = 15,  // Waiting time (seconds) for the next play action
  IDXSCWAIT   = 16,  // Waiting time (seconds) for wait action
  IDXSCWAKEUP = 17,  // Wakeup time for wakeup action
  IDXSCWSTBY  = 18,  // Time before standby
  IDXSCCLIC   = 19,  // Info clic
  IDXSCERROR  = 20   // Eventuelle erreur
};

// Index des paramètres spécifiques du mode Files
enum IDXPARAMSFILES {
  IDXFILE1  = 3,  // Ligne du fichier 1
  IDXFILE2  = 4,  // Ligne du fichier 2
  IDXFILE3  = 5,  // Ligne du fichier 3
  IDXFILE4  = 6,  // Ligne du fichier 4
  IDXFILE5  = 7,  // Ligne du fichier 5
  IDXFILE6  = 8   // Ligne du fichier 6
};

// Index des paramètres spécifiques du mode Paramètres
enum IDXPARAMS {
  IDXMANU   =  3,  // Manual or auto read mode
  IDXSELSC  =  4,  // Selecting the scenario file in auto mode
  IDXGESTSC =  5,  // Editing the selected scenario file
  IDXNEWBPS =  6,  // Addition of a new scenario file (automatic name ScenarioA.bps, ScenarioB.bps ...)
  IDXDATE   =  7,  // Update of the current date
  IDXHEURE  =  8,  // Update of the current time
  IDXBATP   =  9,  // Internal battery charge level
  IDXEXTP   = 10,  // External battery charge level
  IDXSD     = 11,  // SD card occupancy
  IDXPFLT   = 12,  // Play with filter or not
  IDXDEFG   = 13,  // Default gain
  IDXLANG   = 14,  // Language
  IDXCRIGHT = 15   // Copyright menu
};

// Index of specific parameters for the modification of an action
enum IDXMODIFACTION {
  IDXMARETURN =  0,  // Return to previous mode
  IDXMANEXT   =  1,  // Next action
  IDXMAPREV   =  2,  // Previous action
  IDXMADEL    =  3,  // Del
  IDXMAINSERT =  4,  // Insert after
  IDXMALINE   =  5,  // Line number
  IDXMANBLINE =  6,  // Number of lines
  IDXMAVALID  =  7,  // Valid line or not (comment)
  IDXMATYPE   =  8,  // Type of action
  IDXMADUR    =  9,  // Duration of action
  IDXMATIME   = 10,  // Time of action
  IDXMADIR    = 11,  // Action dir
  IDXMAFILE   = 12   // Name of the action's wav file
};

// Etats de lecture
enum STATUSPLAY {
  SPAUSE = 0,  // En pause
  SPLAY  = 1   // Lecture en cours
};

// N° des pins du bouton 5 positions
#define PINPUSH  7
#define PINPUSHB 9
#define PINRIGHT 3
#define PINLEFT  8
#define PINUP    2
#define PINDOWN  6

// Définition de la broche de mesure de la batterie interne
#define PINA2_LIPO  2
// Définition de la broche de mesure de la batterie externe
#define PINA1_EXT   1

//! Définition du bit de reconnaissance du type d'appareil
#define  OPT_B0 34

//! Définition du bit d'arrêt de l'ampli
#define  BIT_MUTE 33

// Modes de lecture
enum MODEPLAY {
  PLAYONE    = 0,  // Lecture du fichier courant puis stop
  PLAYLOOP   = 1,  // Lecture en boucle du fichier courant
  PLAYFILES  = 2,  // Lecture des fichiers du répertoire puis stop
  PLAYFLOOP  = 3,  // Lecture en boucle des fichiers du répertoire
  PLAYMAX    = 4
};

// Atténuation de lecture
enum ATTENUATIONPLAY {
  M30DB  = 0,     // Atténuation de moins 30dB
  M24DB  = 1,     // Atténuation de moins 24dB
  M18DB  = 2,     // Atténuation de moins 18dB
  M12DB  = 3,     // Atténuation de moins 16dB
  M6DB   = 4,     // Atténuation de moins 6dB
  Z0DB   = 5,     // Pas d'atténuation
  P6DB   = 6,     // Augmentation de plus 6dB
  P12DB  = 7,     // Augmentation de plus 16dB
  P18DB  = 8,     // Augmentation de plus 18dB
  P24DB  = 9,     // Augmentation de plus 24dB
  P30DB  = 10,    // Augmentation de plus 30dB
  MAXDB  = 11
};

// Largeur de l'écran en pixel
#define SCREENWIDTH 128
// Hauteur de l'écran en pixel
#define SCREENHIGHT 64
// Nombre de lignes sur l'écran
#define MAXLINES 7
// Nombre max de caractères sur une ligne
#define MAXCHARLINE 21
// Hauteur en pixel d'une ligne de caractères
#define HLINEPIX 9
// Largeur d'un caractère en pixel
#define LCHARPIX 6
// Indique la présence des boutons Left/Right
#define BLEFTRIGHT true

// Taille des buffers d'échantillons
#define MAXSAMPLE 4096
//#define MAXSAMPLE 8192

// Taille max du nom de l'enregistreur (type + numéro de série)
#define MAXNAMERECORDER 16

//! Temps d'attente en secondes pour les affichages momentanés de veille et acquisition
#define TIMEATTENTEAFF 15

#endif
