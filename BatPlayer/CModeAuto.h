/* 
 * File:   CModeAuto.h
   BatPlayer Copyright (c) 2020 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "CReader.h"
#include "CModeGen.h"

#ifndef CMODEAUTO_H
#define CMODEAUTO_H

// Fonctions pour le reset du CPU en cas de problème
#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
#define CPU_RESTART_VAL 0x5FA0004
#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Play
-----------------------------------------------------------------------------*/
class CModeAuto : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  // Constructeur
  CModeAuto();

  //-------------------------------------------------------------------------
  // Destructeur
  virtual ~CModeAuto();
  
  //-------------------------------------------------------------------------
  // Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  // Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  // On passe à l'action suivante
  bool NextAction();

  //-------------------------------------------------------------------------
  // Lancement d'une action lecture d'un fichier
  bool SetPlayFile();
  
  //-------------------------------------------------------------------------
  // Lancement d'une action lecture d'un répertoire
  bool SetPlayDir();
  
  //-------------------------------------------------------------------------
  // Lancement d'une action de type attente
  bool SetWait();
  
  //-------------------------------------------------------------------------
  // Lancement d'une action de type réveil a une certaine heure
  bool SetWakeup();
    
  //-------------------------------------------------------------------------
  // Attente de l'heure de réveil
  bool Standby();

  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief To display information outside of modifiers
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  // Lecture du choix d'atténuation d'un fichier
  // Dans NonFichierwav.txt, récupère l'indice d'atténuation
  void ReadAttenuation();

  //-------------------------------------------------------------------------
  // Init du fichier sélectionné
  // Retourne true si fichier OK
  bool InitWavFile();

  //-------------------------------------------------------------------------
  // Init du fichier suivant dans la lecture d'un répertoire
  // Retourne true si fichier OK, false si problème ou fin du répertoire
  bool NextWavFile(
    bool bBoucle  // true si boucle du répertoire
    );

  //-------------------------------------------------------------------------
  // Lance la lecture du fichier
  bool StartRead();

  //-------------------------------------------------------------------------
  // Stoppe la lecture du fichier
  void StopRead();

  //-------------------------------------------------------------------------
  //! \brief Cohérence entre les différents paramètres
  void Coherenceparams();
  
protected:
  // Paramètres du mode Play
  ParamModePlay paramsP;

  // Gestionnaire de lecture
  CReader Reader;

  // Indice du fichier en cours de lecture pour la lecture d'un répertoire
  int iIndicePlay;

  // Affichage du répertoire courant
  char sCurrentDir[MAXCHARLINE+1];

  // Affichage du fichier courant
  char sCurrentFile[MAXCHARLINE+1];

  // Heure, minutes et secondes du temps d'attente ou de l'heure du réveil
  int iHour, iMinutes, iSeconds;
  
  // Type d'action et état du scénario
  int iStatus;

  // Booléen pour le passage en mode parmètres
  bool bParams;

  // Ligne d'exécution du scénario
  int iLine;
  int iLinePlus;

  // Nombre de lignes du scénario
  int iNbLine;

  // Décompteur du temps de lecture
  int iDuration;

  // Chaine de l'attente d'une action wait
  char sStrWait[MAXCHARLINE+1];

  // Chaine de l'attente d'une action wakeup
  char sStrWakeup[MAXCHARLINE+1];

  // Chaine de l'attente du passage en veille
  char sStrWaitStandby[MAXCHARLINE+1];

  // Chaine d'info pour sortir de veille
  char sStrInfoClic[MAXCHARLINE+1];

  // Chaine d'une éventuelle erreur
  char sStrError[MAXCHARLINE+1];

  // Action courante
  ActionScenario currentAction;

  // Attente pour passer en veille
  int iAttenteVeille;

  // Seconde courante
  int iCurrentSeconde;
};

#endif // CMODEAUTO_H
