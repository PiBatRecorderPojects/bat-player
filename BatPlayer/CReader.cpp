/* 
 * File:   CReader.cpp
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Const.h"
#include "CReader.h"

// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef SD_FAT_TYPE
  extern SdFat sd;
#endif
#ifdef MMC_SDFAT
  extern SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  extern SdFatSdioEX sd;
#endif

#if defined(__IMXRT1062__) // Teensy 4.1
  // Sur Teensy 4.1, définition des buffers DMA en RAM2, donc forcément static
  DMAMEM __attribute__((aligned(32))) int16_t CReader::samplesDMA[MAXSAMPLE*4];
#endif

//! Filtre passe haut à 50kHz de fréquence de coupure pour une Fe à 384kHz
const double FHighPass384kHz[64] = {
   0,                     0.00000000941612037243757, 0.000000973117375122149, 0.00000473076970483195, 0.0000107741097596535,  0.0000152164366792839,
   0.0000191866345155929, 0.0000219783285749218,     0.0000254095356051619,   0.0000287807054516873,  0.0000360454404081704,  0.0000493449154527893,
   0.0000773763754026613, 0.000127308211931336,      0.000213863691862338,    0.00034749452025358,    0.000542151430062288,   0.000795472829754338,
   0.00109156749897613,   0.00136723269969957,       0.00150671025111844,     0.00129043208442715,    0.000387741769965788,  -0.00170111230564958,
  -0.00562106826357505,  -0.0121819725417736,       -0.0222170103273674,     -0.036468814667251,     -0.0551752304709291,    -0.0777112321347814,
  -0.101711728064123,    -0.122566715424618,         0.868149250540119,      -0.122566715424618,     -0.101711728064123,     -0.0777112321347814,
  -0.0551752304709291,   -0.036468814667251,        -0.0222170103273675,     -0.0121819725417736,    -0.00562106826357506,   -0.00170111230564958,
   0.000387741769965788,  0.00129043208442715,       0.00150671025111844,     0.00136723269969957,    0.00109156749897613,    0.00079547282975434,
   0.000542151430062288,  0.00034749452025358,       0.000213863691862338,    0.000127308211931336,   0.0000773763754026615,  0.0000493449154527893,
   0.0000360454404081704, 0.0000287807054516873,     0.0000254095356051619,   0.0000219783285749217,  0.0000191866345155929,  0.0000152164366792838, 
   0.0000107741097596533, 0.00000473076970483182,    0.000000973117375122197, 0.00000000941612037243757
  };

//-------------------------------------------------------------------------
//! \class CFIRFilter
//! \brief Classe qui implémente le calcul d'un filtre FIR via le DSP
//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
CFIRFilter::CFIRFilter()
{
}

//-------------------------------------------------------------------------
//! \brief Initialisation des coefficients du filtre
//! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
//! \param pfCoef Tableau des n coefficients du filtre
//! \return Retourne true si initialisation OK
bool CFIRFilter::InitFIRFilter(
  int iNTaps,
  const double *pfCoef
  )
{
  //Serial.printf("CFIRFilter::InitFIRFilter iNTaps %d\n", iNTaps);
  bool bOK = true;
  // Recherche du max en valeur absolue
  double fMax = -33000;
  for (int i=0; i<iNTaps; i++)
  {
    if (fabs(pfCoef[i]) > fMax)
      fMax = pfCoef[i];
  }
  // Calcul du coefficient de transformation en entier
  // pour ne pas dépasser le domaine 32767 et garder un max de précision
  double coef = 32700.0 / fMax;
  // Calcul des coefficients en format q15_t (int16_t)
  for (int i=0; i<iNTaps; i++)
  {
    double newCoef = pfCoef[i] * coef;
    lstCoeff[i] = (q15_t) newCoef;
    //Serial.printf("Tap %d, %f, %d\n", i, pfCoef[i], (int16_t)lstCoeff[i]);
  }
  // Initialise l'instance FIR (ARM DSP Math Library)
  if (arm_fir_init_q15(&fir_inst, iNTaps, (q15_t *)lstCoeff, (q15_t *)StateQ15, BLOCK_SAMPLES) != ARM_MATH_SUCCESS)
  {
    // Erreur d'init du filtre
    Serial.println("Erreur arm_fir_init_q15 !!!");
    bOK = false;
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Initialisation des coefficients du filtre
//! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
//! \param pfCoef Tableau des 32 coefficients du filtre
//! \return Retourne true si initialisation OK
bool CFIRFilter::InitFIRFilter(
  int iNTaps,
  const int16_t *piCoef
  )
{
  bool bOK = true;
  for (int i=0; i<iNTaps; i++)
    lstCoeff[i] = (q15_t) piCoef[i];
  // Initialise l'instance FIR (ARM DSP Math Library)
  if (arm_fir_init_q15(&fir_inst, iNTaps, (q15_t *)lstCoeff, (q15_t *)StateQ15, BLOCK_SAMPLES) != ARM_MATH_SUCCESS)
  {
    // Erreur d'init du filtre
    //Serial.println("Erreur arm_fir_init_q15 !!!");
    bOK = false;
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Applique le filtre sur un tableau d'échantillons
//! \param pFirIn Tableau des échantillons en entrée du filtre (taille BLOCK_SAMPLES)
//! \param pFirOut Tableau des échantillons en sortie du filtre (taille BLOCK_SAMPLES)
void CFIRFilter::SetFIRFilter(
  q15_t *pFirIn,
  q15_t *pFirOut
  )
{
  // Application du filtre par le DSP
  arm_fir_fast_q15(&fir_inst, pFirIn, pFirOut, BLOCK_SAMPLES);
}

//-------------------------------------------------------------------------
// Classe pour la gestion de la lecture d'un fichier wav
// Gère la lecture du fichier wav et l'envoi des échantillons vers le DAC0

//-------------------------------------------------------------------------
// Constructeur (initialisation de la classe)
CReader::CReader() : restitution( (uint16_t *)samplesDMA, MAXSAMPLE, 384000)
{
  if (pInstance != NULL)
  {
    Serial.println("Erreur, une seule instance possible pour la classe CReader !");
  }
  else
  {
    // Init des variables
    pInstance     = this;
    bFiltre       = false;
    bReading      = false;
    bReadingInProgress = false;
    bNeedToStop   = false;
    bExp10        = false;
    iFe           = 0;
    lWaveLength   = 0;
    lWaveEchRead  = 0;
    iWaveDuration = 0;
    iWavePos      = 0;
    iPosLecture   = 0;
    iDec          = 0;
    sizeBuffer    = MAXSAMPLE * sizeof(uint16_t);
    sPathWaveFile[0] = 0;
    P1 = (volatile uint16_t *)samplesDMA;
#if defined(__MK66FX1M0__) // Teensy 3.6
    P2 = &samplesDMA[MAXSAMPLE];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    //! Sur Teensy 4.1, buffer stéréo
    P2 = (uint16_t *)&samplesDMA[MAXSAMPLE*2];
#endif
    bSaturation   = false;
    bSignalFaible = false;
    firFilter.InitFIRFilter( 64, FHighPass384kHz);
  }
}

//-------------------------------------------------------------------------
// Destructeur (initialisation de la classe)
CReader::~CReader()
{
  pInstance = NULL;
}

/*void Test_eWav(int16_t  eWav, int iDec)
{
  uint32_t ech;
  Serial.print("Test eWav=");
  Serial.println(eWav);
  if (iDec == 0)
    ech = eWav + 2048;
  else if (iDec < 0)
    ech = (eWav  >> abs(iDec)) + 2048;
  else
    ech = (eWav  << abs(iDec)) + 2048;
  Serial.print("ech = ");
  Serial.println(ech);
  Serial.println(" ");
}*/

//-------------------------------------------------------------------------
// Initialisation du fichier à lire (ouverture du fichier)
// Retourne true si ouverture OK
bool CReader::SetWaveFile(
  bool Bexp10,        // Indique si lecture en expansion de temps x10 (true)
  char *pPathWavFile  // Pointeur vers le nom du fichier
  )
{
  //Serial.printf("CReader::SetWaveFile(%d, %s)\n", Bexp10, pPathWavFile);
  bool bOK = false;
  // Mémo du nom du fichier
  strcpy( sPathWaveFile, pPathWavFile);
  // Mémo de l'expansion de temps
  bExp10 = Bexp10;
  // Ouverture du fichier wave sans expansion de temps
  if (waveFile.OpenWaveFileForRead( sPathWaveFile, false))
  {
    // Init de la fréquence d'échantillonnage
    iFe = waveFile.GetSampleRate();
    // Prise en compte de l'expansion de temps
    if (iFe >= 192000 and bExp10)
      // Pour des Fe égale ou plus importante que 192kHz et expansion demandée, on divise la Fe par 10
      iFe /= 10;
    else if (iFe < 192000 and !bExp10)
      // Pour des Fe inférieure à 192kHz et expansion non demandée, on est en face d'un fichier en expansion de temps
      // On multiplie la Fe par 10
      iFe *= 10;
    lWaveLength   = waveFile.GetDataLength() / sizeof(int16_t);
    float fDur    = (float)lWaveLength / (float)iFe;
    iWaveDuration = (int)fDur;
    lWaveEchRead  = 0;
    iWavePos      = 0;
    iPosLecture   = 0;
    bSaturation   = false;
    bSignalFaible = false;
    iNbSaturation = 0;
    iMaxSaturation= 0;
    iMaxEch       = 0;
    waveFile.SetPosRead(0);
    bOK = true;
    Serial.printf("CReader::SetWaveFile [%s] Fe %d\n", pPathWavFile, iFe);
  }
  else
  {
    iWaveDuration = 0;
    lWaveEchRead  = 0;
    iWavePos      = 0;
    iPosLecture   = 0;
    bSaturation   = false;
    bSignalFaible = false;
    iNbSaturation = 0;
    iMaxSaturation= 0;
    iMaxEch       = 0;
    Serial.printf("CReader::SetWaveFile ouverture %s impossible !\n", sPathWaveFile);
  }
 /* Test_eWav(0, 0);
  Test_eWav(-2048, 0);
  Test_eWav(2048, 0);
  Test_eWav(4096, -1);
  Test_eWav(-4096, -1);
  Test_eWav(16395, -5);
  Test_eWav(-16395, -5);*/
  return bOK;
}

//-------------------------------------------------------------------------
// Ferme le fichier wav
void CReader::CloseWavefile()
{
  //Serial.printf("CReader::CloseWavefile()\n");
  if (bReading)
    StopRead();
  waveFile.CloseWavefile();
}
  
//-------------------------------------------------------------------------
// Lance la lecture du fichier sélectionné
// Retourne true si lecture OK
bool CReader::StartRead()
{
  //Serial.println("CReader::StartRead");
  // On autorise l'ampli
  digitalWrite( BIT_MUTE, HIGH);
  // Initialisation de la Fe de restitution
  restitution.setFe( iFe);
  Serial.printf("CReader::StartRead Fe %d, décalage=%d", iFe, iDec);
  if (bFiltre)
    Serial.printf(", Avec filtre\n");
  else
    Serial.printf(", Sans filtre\n");
  // Remplissage des buffers avant lancement
  if (SetBuffer( (uint16_t *)P1) and SetBuffer( (uint16_t *)P2))
  {
    // Lancement de la restitution
    iNbSaturation  = 0;
    bReading       = true;
    bReadingInProgress = false;
    uiWaitStartRead = millis();
    bSaturation    = false;
    //bSignalFaible  = false;
    iNbSaturation  = 0;
    iMaxSaturation = 0;
    iMaxEch        = 0;
    iMaxWav        = 0;
    restitution.start();
    //Serial.println("restitution.start OK");
    //delay(100);
  }
  else
    Serial.println("CReader::StartRead erreur sur SetBuffer !!");
  return bReading;
}

//-------------------------------------------------------------------------
// Stoppe la lecture du fichier en lecture
// Retourne true si arret OK
bool CReader::StopRead()
{
  //Serial.println("CReader::StopRead");
  // On coupe l'ampli
  digitalWrite( BIT_MUTE, LOW);
#if defined(__IMXRT1062__) // Teensy 4.1
  __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  unsigned char sreg_backup = SREG;
  cli();
#endif
  // Stoppe la restitution
  bReading      = false;
  bReadingInProgress = false;
  bNeedToStop   = false;
  restitution.stop();
  lWaveEchRead  = 0;
  iWavePos      = 0;
  iPosLecture   = 0;
  if (iNbSaturation > iMaxSaturation)
    iMaxSaturation = iNbSaturation;
  waveFile.SetPosRead(0);
#if defined(__IMXRT1062__) // Teensy 4.1
  __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  SREG = sreg_backup;
#endif
  return bReading;
}

//-------------------------------------------------------------------------
// Fonction d'initialisation d'un buffer avec les échantillons du fichier wave
// Retourne true si OK
bool CReader::SetBuffer(
  uint16_t *pBuffer   // Pointeur sur le buffer à initialiser
  )
{
  /*unsigned long uiTA, uiTB, uiTC;
  uiTA = millis();*/
  bool bOK = false;
  int32_t ech;
#if defined(__IMXRT1062__) // Teensy 4.1
  // Mémo du pointeur de début du buffer DMA
  uint16_t *pMemBuffer = pBuffer;
#endif
  //Serial.printf("CReader::SetBuffer iDec %d, bAttenuation %d\n", iDec, bAttenuation);
  // Lecture d'un bloc d'échantillon depuis le fichier wave
  unsigned long lRead = waveFile.WavefileRead( samplesWave, MAXSAMPLE);
  //uiTB = millis();
  if (lRead > 0)
    lWaveEchRead += MAXSAMPLE;
  if (lRead == sizeBuffer)
  {
    if (!bFiltre)
    {
      // Passage des échantillons au buffer DMA
      for (int i=0; i<MAXSAMPLE; i++)
      {
        if (abs(samplesWave[i]) > iMaxWav)
          iMaxWav = abs(samplesWave[i]);
#if defined(__MK66FX1M0__) // Teensy 3.6
        // On passe dans le domaine non signé de -2048/+2047 à 0/4095 avec aténuation ou augmentation
        if (iDec == 0)
          // Pas de changement de niveau
          ech = samplesWave[i] + 2048;
        else if (bAttenuation)
          // Atténuation par un décalage de n bits
          ech = (samplesWave[i] >> iDec) + 2048;
        else
          // Amplification par un décalage de n bits
          ech = (samplesWave[i] << iDec) + 2048;
        // Mémo de l'échantillon max
        if (iMaxEch < ech)
          iMaxEch = ech;
        // Test s'il y a saturation (vérification du domaine 0-4095)
        if (ech > 4095)
        {
          iNbSaturation++;
          if (iNbSaturation > lWaveLength/100)
            bSaturation = true;
          ech = 4095;
        }
        else if (ech < 0)
        {
          iNbSaturation++;
          if (iNbSaturation > lWaveLength/100)
            bSaturation = true;
          ech = 0;
        }
        // Init du buffer
        *pBuffer++ = (uint16_t)ech;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
        // On reste dans le domaine -32768/+32767 avec aténuation ou augmentation
        if (iDec == 0)
          // Pas de changement de niveau
          ech = samplesWave[i];
        else if (bAttenuation)
          // Atténuation par un décalage de n bits
          ech = (samplesWave[i] >> iDec);
        else
          // Amplification par un décalage de n bits
          ech = (samplesWave[i] << iDec);
        // Mémo de l'échantillon max
        if (iMaxEch < ech)
          iMaxEch = ech;
        // Test s'il y a saturation (sur T4.1 -16383/+16383)
        if (abs(ech) > 16383)
        {
          iNbSaturation++;
          if (iNbSaturation > lWaveLength/100)
            bSaturation = true;
          if (ech < 0)
            ech = -16383;
          else
            ech = 16383;
        }
        // Init du buffer stéréo
        *pBuffer++ = (int16_t)ech;
        *pBuffer++ = (int16_t)ech;
#endif
      }
    }
    else
    {
      // Lecture avec le filtre passe haut
      for (int i=0; i<MAXSAMPLE; )
      {
        // Echantillons signés pour filtre
        for (int j=0; j<BLOCK_SAMPLES; j++, i++)
            samplesFirIn[j] = (q15_t)(samplesWave[i]);
        // Application du filtre par le DSP
        firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut);
        // Traitement des échantillons filtrés
        for (int j=0; j<BLOCK_SAMPLES; j++)
        {
          if (abs(samplesFirOut[j]) > iMaxWav)
            iMaxWav = abs(samplesFirOut[j]);
#if defined(__MK66FX1M0__) // Teensy 3.6
          // On passe dans le domaine non signé de -2048/+2047 à 0/4095 avec aténuation ou augmentation
          if (iDec == 0)
            // Pas de changement de niveau
            ech = samplesFirOut[j] + 2048;
          else if (bAttenuation)
            // Atténuation par un décalage de n bits
            ech = (samplesFirOut[j] >> iDec) + 2048;
          else
            // Amplification par un décalage de n bits
            ech = (samplesFirOut[j] << iDec) + 2048;
          // Mémo de l'échantillon max
          if (iMaxEch < ech)
            iMaxEch = ech;
          // Test s'il y a saturation (vérification du domaine 0-4095)
          if (ech > 4095)
          {
            iNbSaturation++;
            if (iNbSaturation > lWaveLength/100)
              bSaturation = true;
            ech = 4095;
          }
          else if (ech < 0)
          {
            iNbSaturation++;
            if (iNbSaturation > lWaveLength/100)
              bSaturation = true;
            ech = 0;
          }
          // Init du buffer
          *pBuffer++ = (uint16_t)ech;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
          // On reste dans le domaine -32768/+32767 avec aténuation ou augmentation
          if (iDec == 0)
            // Pas de changement de niveau
            ech = samplesFirOut[j];
          else if (bAttenuation)
            // Atténuation par un décalage de n bits
            ech = (samplesFirOut[j] >> iDec);
          else
            // Amplification par un décalage de n bits
            ech = (samplesFirOut[j] << iDec);
          // Mémo de l'échantillon max
          if (iMaxEch < abs(ech))
            iMaxEch = abs(ech);
          // Test s'il y a saturation (sur T4.1 -16383/+16383)
          if (abs(ech) > 16383)
          {
            iNbSaturation++;
            if (iNbSaturation > lWaveLength/100)
              bSaturation = true;
            if (ech < 0)
              ech = -16383;
            else
              ech = 16383;
          }
          // Init du buffer stéréo
          *pBuffer++ = (int16_t)ech;
          *pBuffer++ = (int16_t)ech;
#endif
        }
      }      
    }
    bOK = true;
  }
  else
  {
    if (lRead <= 0 or lWaveEchRead < lWaveLength)
      // Erreur de lecture
      Serial.printf("CReader::SetBuffer Erreur lRead %d, sizeBuffer %d wave read %d, wave length %d\n", lRead, sizeBuffer, lWaveEchRead, lWaveLength);
    else
    {
      // Fin du fichier, vérification si signal faible
      // Signalfaible si max plus petit que 1/4 du max d'une demi alternance
      bSignalFaible = false;
#if defined(__MK66FX1M0__) // Teensy 3.6
      if (iMaxEch < 2047+512)
        bSignalFaible = true;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
      if (iMaxEch < 8192)
        bSignalFaible = true;
#endif
      if (bSaturation)
        Serial.printf("CReader::SetBuffer iMaxEch %d\n", iMaxEch);
    }
  }
  /*uiTC = millis();
  Serial.printf("IT=%d,%d ms\n", uiTB-uiTA, uiTC-uiTB);*/
#if defined(__IMXRT1062__) // Teensy 4.1
  // Vide les données du cache vers la mémoire pour l'accès DMA
  //arm_dcache_delete( pMemBuffer, MAXSAMPLE*2*sizeof(int16_t));
  arm_dcache_flush( pMemBuffer, MAXSAMPLE*2*sizeof(int16_t));
  //arm_dcache_flush_delete( pMemBuffer, MAXSAMPLE*2*sizeof(int16_t));
#endif
  // Calcul de la position de lecture
  float fDur    = (float)lWaveEchRead / (float)iFe;
  iWavePos      = (unsigned int)fDur;
  if (iWavePos > iWaveDuration)
    iWavePos = iWaveDuration;
  fDur = (float)iWavePos * 100.0 / (float)iWaveDuration;
  iPosLecture   = (unsigned short)fDur;
  if (iPosLecture > 100)
    iPosLecture = 100;
  return bOK;
}
  
//-------------------------------------------------------------------------
// Fonction de traitement des interruption DMA pour remplir les buffers
void CReader::OnITDMA()
{
  /*unsigned long uiTA, uiTB, uiTC;
  uiTA = millis();
  uiTB = uiTA;*/
  //digitalWrite(LED_BUILTIN, HIGH);
  bReadingInProgress = true;
  // Lecture de l'adresse courante de lecture du DMA
  uint32_t daddr = (uint32_t)restitution.GetCurrentDMAaddr();
  // Par défaut, 1er buffer
  uint16_t *P = (uint16_t *)P1;
  if (daddr < (uint32_t)P2)
    // Le DMA est dans le 1er buffer, on s'occupe du second
    P = (uint16_t *)P2; 
  if (bReading and !bNeedToStop)
  {
    // Initialisation du buffer
    bool bOK = SetBuffer( P);
    //uiTB = millis();
    if (!bOK)
      // Fin du fichier, on demande de stopper la restitution
      bNeedToStop = true;
  }
  /*uiTC = millis();
  Serial.printf("IT=%d,%d ms\n", uiTB-uiTA, uiTC-uiTB);*/
  // RAZ IT DMA
  restitution.ClearDMAIsr();
  //digitalWrite(LED_BUILTIN, LOW);
}

/*------------------------------------------------------------------------------------------------------
 * Routine d'interruption du DMA d'acquisition, un buffer d'échantillons est disponible
 * Ne pas passer trop de temps dans le traitement d'une IT
 *------------------------------------------------------------------------------------------------------*/
#if defined(__MK66FX1M0__) // Teensy 3.6
void dma_ch3_isr()
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
void dma_I2S_isr()
#endif
{
  // Appel de la fonction de traitement
  CReader::pInstance->OnITDMA();
}

//-------------------------------------------------------------------------
// Initialisation de l'atténuation de lecture
void CReader::SetAttenuation(
  int iCodeAtt  // Code de l'atténuation
  )
{
  switch (iCodeAtt)
  {
  case P30DB: iDec = 5; bAttenuation = false; break;      // Gain +30dB
  case P24DB: iDec = 4; bAttenuation = false; break;      // Gain +24dB
  case P18DB: iDec = 3; bAttenuation = false; break;      // Gain +18dB
  case P12DB: iDec = 2; bAttenuation = false; break;      // Gain +12dB
  case P6DB : iDec = 1; bAttenuation = false; break;      // Gain +6dB
  case Z0DB : iDec = 0; bAttenuation = true;  break;      // Pas d'atténuation
  case M6DB : iDec = 1; bAttenuation = true;  break;      // Atténuation -6dB
  case M12DB: iDec = 2; bAttenuation = true;  break;      // Atténuation -12dB
  case M18DB: iDec = 3; bAttenuation = true;  break;      // Atténuation -18dB
  case M24DB: iDec = 4; bAttenuation = true;  break;      // Atténuation -24dB
  case M30DB: iDec = 5; bAttenuation = true;  break;      // Atténuation -30dB
  }
}
  
//-------------------------------------------------------------------------
//! \brief Gestion des tâches de fonds
void CReader::OnLoop()
{
  if (bNeedToStop)
    // Fin du fichier, on stoppe la restitution
    StopRead();
  else if (bReading and !bReadingInProgress and millis()-uiWaitStartRead > 200)
  {
    // Sur T4.1, béquille pour relancer la lecture lorsque celle-ci ne démarre pas
    digitalWrite(LED_BUILTIN, HIGH);
    StopRead();
    delay(100);
    StartRead();
    digitalWrite(LED_BUILTIN, LOW);
  }
}

// Pointeur sur l'instance unique de la classe
CReader *CReader::pInstance = NULL;
