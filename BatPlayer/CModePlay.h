/* 
 * File:   CModePlay.h
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "CModeGen.h"
#include "CReader.h"

#ifndef CMODEPLAY_H
#define CMODEPLAY_H

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Play
-----------------------------------------------------------------------------*/
class CModePlay : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  // Constructeur
  CModePlay();

  //-------------------------------------------------------------------------
  // Destructeur
  virtual ~CModePlay();
  
  //-------------------------------------------------------------------------
  // Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  // Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief To display information outside of modifiers
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  // Sauvegarde du choix d'atténuation d'un fichier
  // Dans NonFichierwav.txt, sauvegarde de l'indice d'atténuation
  void SaveAttenuation();

  //-------------------------------------------------------------------------
  // Lecture du choix d'atténuation d'un fichier
  // Dans NonFichierwav.txt, récupère l'indice d'atténuation
  void ReadAttenuation();

  //-------------------------------------------------------------------------
  // Init du fichier sélectionné
  // Retourne true si fichier OK
  bool InitWavFile();

  //-------------------------------------------------------------------------
  // Init du fichier suivant dans la lecture d'un répertoire
  // Retourne true si fichier OK, false si problème ou fin du répertoire
  bool NextWavFile(
    bool bBoucle  // true si boucle du répertoire
    );

  //-------------------------------------------------------------------------
  // Lance la lecture du fichier
  void StartRead();

  //-------------------------------------------------------------------------
  // Stoppe la lecture du fichier
  void StopRead();

  //-------------------------------------------------------------------------
  //! \brief Cohérence entre les différents paramètres
  void Coherenceparams();
  
  //-------------------------------------------------------------------------
  //! \brief Gestion des tâches de fonds
  virtual void OnLoop();

protected:
  // Paramètres du mode Play
  ParamModePlay paramsP;

  // Gestionnaire de lecture
  CReader Reader;

  // Indice du fichier en cours de lecture pour la lecture d'un répertoire
  unsigned int iIndicePlay;

  // Affichage du répertoire courant
  char sCurrentDir[MAXCHARLINE+1];

  // Affichage du fichier courant
  char sCurrentFile[MAXCHARLINE+1];
};

#endif // CMODEPLAY_H
