/* 
 * File:   ConstChar.c
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.
   Many thanks to Edwin Houwertjes for the translations in German and Dutch.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiersConfig.h"

//------------------------------------------------------
// Strings used in the software to easily add a language
//------------------------------------------------------

/*
  To add a language, you must:
  - In ModModifier library, add the language to the LANGUAGE enum at the start of the ModesModifiersConfig.h file
  - In ModModifier library, add to each string of the ConstChar.c file the translation of the message in the new language
  - Add to each string of the this ConstChar.c file the translation of the message in the new language
  Order of languages :
  LENGLISH    = 0
  LFRENCH     = 1
  LGERMAN     = 2
  LDUTCH      = 3
*/

//***************************************************************************************
// WARNING, Messages intended for the screen, the font is poor in characters with accents
//          Messages for the Log file (last part of this file), no problem with accents
//***************************************************************************************

//------------------------------------------------------
// Parameters screen
//------------------------------------------------------
// Character strings of the list of the modes menu Play -> Files -> Params -> Play
const char *txtMODES[MAXLANGUAGE][3]      = {{"->Files   ", "->Params. ", "->Player  "},
                                             {"->Fichiers", "->Params. ", "->Lecteur "},
                                             {"->Datei   ", "->Paramet.", "->Spieler "},
                                             {"->Bestand ", "->Paramet.", "->Afspelen"}};
// Date parameter
const char *txtIDXDATE[]        = {" Date  %s ",
                                   " Date  %s ",
                                   " Datum %s ",
                                   " Datum %s "};
// Hour parameter
const char *txtIDXHEURE[]       = {" Hour  %s",
                                   " Heure %s",
                                   " Uhr   %s",
                                   " Tijd  %s"};
// Internal battery parameter
const char *txtIDXBAT[]         = {" Int. Batt. %s",
                                   " Batt. Int. %s",
                                   " Int. Batt. %s",
                                   " Int. Batt. %s"};
// External battery parameter
const char *txtIDXEXT[]         = {" Ext. Batt. %s",
                                   " Batt. Ext. %s",
                                   " Ext. Batt. %s",
                                   " Ext. Batt. %s"};
// SD Card status parameter
//                                   SD 3.5GO 30%
const char *txtIDXSD[]          = {" SD % 3.0fGO %d%%    ",
                                   " SD % 3.0fGO %d%%    ",
                                   " SD % 3.0fGO %d%%    ",
                                   " SD % 3.0fGO %d%%    "};
// Language parameter
const char *txtIDXLANG[]        = {" Language: %s",
                                   " Langue :  %s",
                                   " Sprache:  %s",
                                   " Taal:     %s"};
// Language parameter
const char *txtIDXFILT[]        = {" Linear filter  %s",
                                   " Filt. lineaire %s",
                                   " Linear filter  %s",
                                   " Linear filter  %s"};
// Copyright parameter
//                                  012345678901234567890
const char *txtIDXCOPYR[]       = {" Copyright ...",
                                   " Copyright ...",
                                   " Copyright ...",
                                   " Copyright ..."};
// Script sélection parameter
const char *txtIDXSELSC[]       = {" BPS %s",
                                   " BPS %s",
                                   " BPS %s",
                                   " BPS %s"};
// String for language enum
const char *txtLanguage[MAXLANGUAGE][MAXLANGUAGE] = {{"English", "Francais", "Deutsch", "Nederlands",},
                                                     {"English", "Francais", "Deutsch", "Nederlands",},
                                                     {"English", "Francais", "Deutsch", "Nederlands",},
                                                     {"English", "Francais", "Deutsch", "Nederlands",}};
// String for operating mode enum
const char *txtManuAuto[MAXLANGUAGE][2] = {{"Manual mode", "Auto. mode"},
                                           {"Mode manuel", "Mode auto."},
                                           {"Man. Modus" , "Auto.Modus"},
                                           {"Handm.modus", "Auto.modus"}};
// String for boolean Yes/No
const char *txtFILT[MAXLANGUAGE][2]       = {{"No " , "Yes"},
                                             {"Non" , "Oui"},
                                             {"Nein", "Ja " },
                                             {"Nee ", "Ja " }};
// String for boolean Y/N
const char *txtYN[MAXLANGUAGE][2]         = {{"N", "Y"},
                                             {"N", "O"},
                                             {"N", "J"},
                                             {"N", "J"}};
// String for "No file" message
const char *txtNoFile[]         = {" No file!            ",
                                   " Pas de fichier !    ",
                                   " Keine datei !       ",
                                   " Geen bestanden !    "};
//------------------------------------------------------
// Player screen
//------------------------------------------------------
// String for playing enum
const char *txtMODESPLAY[MAXLANGUAGE][4]   = {{"Play Fi.", "Loop Fi.", "Play Dir", "Loop Dir"},
                                              {" Normal ", " Boucle ", "R\xe9pert. ", "R\xe9per.B."},
                                              {"Spiel d.", "Wied. da", "Sp. ordn", "Wied. or"},
                                              {"Speel b.", "Herh. b.", "Spl. map", "Herh.map"}};
// String for Attenuation enum
const char *txtAttenuation[MAXLANGUAGE][11] = {{"-30dB", "-24dB", "-18dB", "-12dB", " -6dB", "  0dB", " +6dB", "+12dB", "+18dB", "+24dB", "+30dB"},
                                               {"-30dB", "-24dB", "-18dB", "-12dB", " -6dB", "  0dB", " +6dB", "+12dB", "+18dB", "+24dB", "+30dB"},
                                               {"-30dB", "-24dB", "-18dB", "-12dB", " -6dB", "  0dB", " +6dB", "+12dB", "+18dB", "+24dB", "+30dB"},
                                               {"-30dB", "-24dB", "-18dB", "-12dB", " -6dB", "  0dB", " +6dB", "+12dB", "+18dB", "+24dB", "+30dB"}};
// Sample frequency parameter
const char *txtFE[]        = {"SF%03dkHz",
                              "FE%03dkHz",
                              "SF%03dkHz",
                              "SF%03dkHz"};
// Loud Speaker filter parameter (not use !)
const char *txtIDXFLT[]    =  {"[LS Filt. %s]",
                               "[Filt. HP %s]",
                               "[LS Filt. %s]",
                               "[LS Filt. %s]"};
// String for LS filter enum (not use !)
const char *txtFLT[MAXLANGUAGE][2]   = {{"No ", "Yes"},
                                        {"Non", "Oui"},
                                        {"Nein", "Ja"},
                                        {"Nee ", "Ja"}};
//------------------------------------------------------
// Error mode messages (max 21 chr)
//------------------------------------------------------
//                             123456789012345678901
// Low battery
const char *txtLOWBAT[]    = {"Battery too weak!"    ,
                              "Batterie trop faible!",
                              "Batterie zu Schwach !",
                              "Batterij te zwak !"   };
// Error SD card
const char *txtSDERROR[]   = {"SD card unreadable!"  ,
                              "Carte SD illisible !" ,
                              "SG karte unleserlich!",
                              "SD kaart onleesbaar !"};
// Error software
const char *txtSWError[]   = {"Software error !"     ,
                              "Erreur logiciel !"    ,
                              "Software  Fehler !"   ,
                              "Software fout !"      };

//------------------------------------------------------
// Log file messages (no problem with accents)
//------------------------------------------------------
// Character strings for the change mode log
const char *txtLogPlay[]        = {"### Switch to Player mode",
                                   "### Passage en mode Lecteur",
                                   "### Wechseln zur Player-Modus",
                                   "### Schakelen naar Afspeelmodus"};
const char *txtLogFiles[]       = {"### Switch to sel wav files mode",
                                   "### Passage en mode sélection fichier wav",
                                   "### Wechseln zur Sel. WAV-Dateimodus",
                                   "### Schakelen naar selecteer wav bestand"};
const char *txtLogParams[]      = {"### Switch to Settings mode",
                                   "### Passage en mode Paramètres",
                                   "### Wechseln zur Einstellungsmodus",
                                   "### Schakelen naar instellingen"};
const char *txtLogError[]       = {"### Switch to Error mode",
                                   "### Passage en mode Erreur",
                                   "### Wechseln zur Fehlermodus",
                                   "### Schakelen naar foutmodus"};
const char *txtLogIndet[]       = {"### Switch to indeterminate mode!",
                                   "### Passage en mode indéterminé !",
                                   "### Zur unbestimmten Modus wechseln!",
                                   "### Schakel naar onbestemde modus!"};
const char *txtLogSFiles[]      = {"### Switch to sel bps files mode",
                                   "### Passage en mode sélection fichier bps",
                                   "### Wechseln zur sel. BPS-Dateimodus",
                                   "### Schakel naar BPS bestand modus"};
const char *txtLogCopyright[]   = {"### Switch to copyright mode",
                                   "### Passage en mode copyright",
                                   "### Wechseln zur copyright modus",
                                   "### Schakel naar copyright mode"};
const char *txtLogModifAct[]    = {"### Switch to Modif Action mode",
                                   "### Passage en mode modification des actions",
                                   "### In den Aktionsmodus wechseln",
                                   "### Wisselen naar active modus"};
const char *txtLogSelBPS[]      = {"### Switch to scenario file selection mode",
                                   "### Passage en mode sélection du fichier BPS",
                                   "### Wechselen in de Szenario datei selection modus",
                                   "### Schakelen naar scenario bestand selectiesmodus"};
const char *txtLogAutoMode[]    = {"### Switch to auto play mode",
                                   "### Passage en mode lecture auto",
                                   "### In den Auto-Play-Modus wechseln",
                                   "### Schakel naar automatisch afspelen"};
// Start BatPlayer
const char *txtSTARTBP[]   = {"Starting Bat Player %s serial number %lu, %s, CPU %d",
                              "Démarrage Bat Player %s numéro de série %lu, %s, CPU %d",
                              "Starten Bat Player %s seriennummer %lu, %s, CPU %d",
                              "Starten Bat Player %s serienummer %lu, %s, CPU %d"};
// Error SDFat config
const char *txtSDFATCFG[]  = {"Optimization SdFat: SdFatConfig.h put MAINTAIN_FREE_CLUSTER_COUNT to 1!",
                              "Optimisation SdFat: SdFatConfig.h mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 !",
                              "Optimierung SdFat: SdFatConfig.h mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 !",
                              "Optimalisatie SdFat: SdFatConfig.h mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 !"};
// Log on CModePlay
const char *txtREAD[]      = {"Playing the selected file",
                              "Lecture du fichier sélectionné",
                              "Spiel ausgewählte Datei",
                              "Speel gekozen bestand"};
const char *txtLOOP[]      = {"Loop playback of the selected file",
                              "Lecture en boucle du fichier sélectionné",
                              "Schleifenwiedergabe der ausgewählten Datei",
                              "Doorlopend afspelen van bestand"};
const char *txtRDIR[]      = {"Playing directory files",
                              "Lecture des fichiers du répertoire",
                              "Wiedergabe des ausgewählten Ordners",
                              "Afspelen van gekozen map"};
const char *txtRLDIR[]     = {"Repeat playback of directory files",
                              "Lecture en boucle des fichiers du répertoire",
                              "Schleifenwiedergabe des ausgewählten Ordners",
                              "Doorlopend afspelen van gekozen map"};
const char *txtLOGERDIR[]  = {"Reader.SetWaveFile directory selection error [%s]",
                              "Reader.SetWaveFile erreur sélection répertoire [%s]",
                              "Reader.SetWaveFile Verzeichnisauswahlfehler [%s]",
                              "Reader.SetWaveFile map selectie fout [%s]"};
const char *txtLOGERFILE[] = {"Reader.SetWaveFile error reading file [%s]",
                              "Reader.SetWaveFile erreur lecture fichier [%s]",
                              "Reader.SetWaveFile Fehler beim Lesen der Datei [%s]",
                              "Reader.SetWaveFile leesfout bestand [%s]"};
const char *txtLOGSTOPR[]  = {"Stop playing [%s/%s] %s",
                              "Arrêt lecture [%s/%s] %s",
                              "Stop Wiedergabe [%s/%s] %s",
                              "Stop afspelen [%s/%s] %s"};
const char *txtLOGSATUR[]  = {"Saturation !",
                              "Saturation !",
                              "Sättigung !",
                              "Verzadiging !"};
const char *txtLOGWEAK[]   = {"Signal weak!",
                              "Signal faible !",
                              "Signal schwach! !",
                              "Signaal zwak !"};
const char *txtLOGERATT[]  = {"CModePlay::SaveAttenuation unable to create file [%s]",
                              "CModePlay::SaveAttenuation impossible de créer le fichier [%s]",
                              "CModePlay::SaveAttenuation Datei kann nicht erstellt werden [%s]",
                              "CModePlay::SaveAttenuation bestand maken mislukt [%s]"};
const char *txtLOGSTARTR[] = {"Start playing [%s/%s] Attenuation %s",
                              "Lancement lecture [%s/%s] Atténuation %s",
                              "Start wiedergabe [%s/%s] Dämpfung %s",    
                              "Start afspelen [%s/%s] Verzwakking %s"}; 
const char *txtLOGSTOPRO[] = {"Operator stop playing [%s/%s] %s",
                              "Arrêt lecture opérateur [%s/%s] %s",
                              "Bediener hört auf zu spielen [%s/%s] %s",
                              "Stop afspelen door bedienaar [%s/%s] %s"};

//                                  123456789012345678901
// Default gain                      Gain par def. -24dB
const char *txtIDXDEFG[]        = {" Default gain: %s",
                                   " Gain par def. %s",
                                   " Standard gain:%s",
                                   " Stand. gain:  %s"};
//                                  123456789012345678901
// Addition of a new scenario file
const char *txtIDXNEWBPS[]      = {" New scenario file  ",
                                   " Nouveau fichier BPS",
                                   " Neues Szenario file",
                                   " Niew scenario best."};
// Scenario management
const char *txtIDXMARETURN[]= {"[Return]",
                               "[Retour]",
                               "[Zuruck]",
                               "[Terug ]"};
const char *txtIDXMADEL[]   = {"[Del  %s]",
                               "[Sup. %s]",
                               "[L\xf6sch%s]",
                               "[Verw.%s]"};
const char *txtIDXMAINSERT[]= {"[Inse. after]",
                               "[Inse. apres]",
                               "[Einf. hint.]",
                               "[Invoegen na]"};
const char *txtIDXMALINE[]  = {"Line  %03d",
                               "Ligne %03d",
                               "Linie %03d",
                               "Lijn  %03d"};
const char *txtIDXMATYPE[]  = {"[Type: %s]",
                               "[Type: %s]",
                               "[Art:  %s]",
                               "[Type: %s]"};
const char *txtIDXMADUR[]   = {"[Duration: %s]",
                               "[Duree:    %s]",
                               "[Dauer:    %s]",
                               "[Duur:     %s]"};
const char *txtIDXMAHOUR[]  = {"[Time:     %s]",
                               "[Heure:    %s]",
                               "[Zeit:     %s]",
                               "[Tijd:     %s]"};
const char *txtIDXSCWSTBY[] = {"Standby in %ds ",
                               "Veille dans %ds",
                               "Standby in %ds ",
                               "Standby in %ds "};
const char *txtActions[MAXLANGUAGE][4]= {{"Play file", "Play dir ", "Wait     ", "Wake up  "},
                                         {"Lect fich", "Lect rept", "Attente  ", "Reveil   "},
                                         {"Spiel da.", "Spl. ord.", "Warte    ", "Erwachen "},
                                         {"Speel be.", "Spl. map ", "Wacht    ", "Ontwaken "}};
const char *txtVALID[MAXLANGUAGE][2]  = {{"Valid", "Inval"},  // Valid / Invalid
                                         {"Valid", "Inval"},
                                         {"Valid", "Inval"},
                                         {"Valid", "Inval"}};
//                                 123456789012345678901
const char *txtErrorPlayFile[] = {"Error play file  ",
                                  "Erreur lecture F.",
                                  "Wiedergabe Fehler",
                                  "Fout bij afspelen"};
const char *txtErrorPlayDir[]  = {"Error play dir   ",
                                  "Erreur lecture D.",
                                  "Fehfer spl. ord. ",
                                  "Fout afspelen map"};
// Error SDFat begin
const char *txtSDFATBEGIN[]= {"SdFatSdio begin() failed !!! sd.card()->errorCode() = %d",
                              "SdFatSdio begin() erreur !!! sd.card()->errorCode() = %d",
                              "SdFatSdio begin() Fehler !!! sd.card()->errorCode() = %d",
                              "SdFatSdio begin() fout   !!! sd.card()->errorCode() = %d"};
const char *txtErrorFileBPS[]= {"Playing the scenario file %s, error on line %d [%s]",
                                "Lecture du fichier scénarioe %s, erreur sur la ligne %d [%s]",
                                "Lesen der Szenariodatei %s, Fehler auf Regel %d [%s]",
                                "Scenario bestand lezen: %s, fout op regel %d [%s]"};
// Script management parameter
//                                  0123456789012345678901
const char *txtIDXGESTSC[]      = {" Script modification",
                                   " Modifica. Scenarios",
                                   " Szenariomanagement ",
                                   " Scenario instelling"};
//                               123456789012345678901
const char *txtClicToWakeup[]= {"Clic to wakeup",
                                "Clic pour reveiller",
                                "Klick zur aufzuwachen",  
                                "Klik voor ontwaken"};  
const char *txtSCPARAMS[MAXLANGUAGE][2] = {{"->Params. ", "->Auto.  "},
                                           {"->Params. ", "->Auto.  "},
                                           {"->Paramet.", "->Auto.  "},
                                           {"->Paramet.", "->Auto.  "}};
//                                 12345678901234567890
const char *txtWaitSCH[]       = {"Waiting %ds",
                                  "Attente %ds",
                                  "Warten  %ds",
                                  "Wachten %ds"};
const char *txtWakeupSCH[]     = {"Wakeup at %s",
                                  "Reveil a %s",
                                  "Wach auf um %s",
                                  "Ontwaken op %s"};
const char *txtLogPlayFile[]   = {"Action %s, play file during %d s",
                                  "Action %s, lecture fichier pendant %d s",
                                  "Action %s, Datei abspielen für %d s",
                                  "Actie  %s, speel bestand gedurende %d s"};
const char *txtLogErPlayFile[] = {"Action %s, error on play file",
                                  "Action %s? Erreur sur lecture fichier",
                                  "Action %s, Fehler datei spielen",
                                  "Actie %s, Fout bijspelen bestand"};
const char *txtLogPlayDir[]    = {"Action %s, play dir during %d s",
                                  "Action %s, lecture répertoire pendant %ds",
                                  "Action %s, Spiel Ordner für %d s",
                                  "Actie %s, Speel map gedurende %d s"};
const char *txtLogErPlayDir[]  = {"Action %s, error on play dir",
                                  "Action %s, error sur lecture répertoire",
                                  "Action %s, Fehler abspielen Ordner",
                                  "Actie %s, Fout bij afspelen map"};
const char *txtLogWait[]       = {"Action %s, Wait during %ds",
                                  "Action %s, attente pendant %ds",
                                  "Action %s, warte während %ds",
                                  "Actie %s, Wacht gedurende %ds"};
const char *txtLogWakeup[]     = {"Action %s, wakeup at %s",
                                  "Action %s, réveil à %s",
                                  "Action %s, erwachen um %s",
                                  "Actie %s, ontwaken om %s"};
const char *txtLogErrOpenBPS[] = {"ScenarioFile impossible to open [%s]",
                                  "ScenarioFile impossible d'ouvrir [%s]",
                                  "ScenarioDatei kann nicht geöffnet werden [%s]",
                                  "Scenariobestand onmogelijk te openen [%s]"};
const char *txtLogOpenBPS[]    = {"Running file %s",
                                  "Exécution du fichier %s",
                                  "Ausführen der Datei %s",
                                  "Uitvoeren van bestand %s"};
                                  
