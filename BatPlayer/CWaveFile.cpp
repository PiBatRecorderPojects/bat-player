/* 
 * File:   CWaveFile.cpp
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//-------------------------------------------------------------------------
// Classe pour la gestion d'un fichier wave

#include "CWaveFile.h"
#include "ModesModifiers.h"

// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef SD_FAT_TYPE
  extern SdFat sd;
#endif
#ifdef MMC_SDFAT
  extern SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  extern SdFatSdioEX sd;
#endif

//-------------------------------------------------------------------------
// Constructeur (initialisation de la classe)
CWaveFile::CWaveFile()
{
  nbEch = 0;
  fFe = 192000.0;
  tEch = 1.0 / fFe;
  bWrite = false;
  iDecimation = 1;
}

//-------------------------------------------------------------------------
// Destructeur (fermeture du fichier)
CWaveFile::~CWaveFile()
{
#ifdef SD_FAT_TYPE
  if (wavfile.isOpen())
#else
  if (wavfile)
#endif
    // Fermeture du fichier
    CloseWavefile();
}

//-------------------------------------------------------------------------
// Ouverture du fichier en écriture
// Returne true si OK et false en cas d'erreur
bool CWaveFile::OpenWaveFileForWrite(
    unsigned int iFe,     // Fréquence déchantillonnage en Hz
    const char *filepath, // Path et nom du fichier à créer
    bool bExp10           // Indique que la Fe est divisée par 10 pour expansion de temps
    )
{
  bool bOK = false;
#ifdef SD_FAT_TYPE
  if (wavfile.isOpen())
#else
  if (wavfile)
#endif
    // Fermeture du fichier
    CloseWavefile();
  
  // Préparation de la structure
  int samples_per_second = iFe / 10;
  if (not bExp10)
    samples_per_second = iFe;
  int bits_per_sample = 16;

  strncpy(header.riff_tag,"RIFF",4);
  strncpy(header.wave_tag,"WAVE",4);
  strncpy(header.fmt_tag, "fmt ",4);
  strncpy(header.data_tag,"data",4);

  header.riff_length = 0;
  header.fmt_length = 16;
  header.audio_format = 1;
  header.num_channels = 1;
  header.sample_rate = samples_per_second;
  header.byte_rate = samples_per_second*(bits_per_sample/8);
  header.block_align = bits_per_sample/8;
  header.bits_per_sample = bits_per_sample;
  header.data_length = 0;
  fFe = (float)iFe;
  tEch = 1.0 / fFe;

  // Ouverture du fichier
  if (wavfile.open(filepath, O_RDWR | O_CREAT))
  {
    // Ecriture de l'entête
    wavfile.write( (char *)&header, sizeof(header));
    wavfile.flush();
    bOK = true;
    bWrite = true;
  }
  else
  {
    Serial.print("CWaveFile::OpenWaveFileForWrite(");
    Serial.print(filepath);
    Serial.println(") erreur creation fichier !!!");
  }
  
  return bOK;
}

//-------------------------------------------------------------------------
// Ouverture du fichier en lecture
// Returne true si OK et false en cas d'erreur
bool CWaveFile::OpenWaveFileForRead(
    const char *filepath, // Path et nom du fichier à créer
    bool bExpT            // True pour une expansion de temps de 10, false pour 1
    )
{
  bool bOK = false;
#ifdef SD_FAT_TYPE
  if (wavfile.isOpen())
#else
  if (wavfile)
#endif
    // Fermeture du fichier
    CloseWavefile();
  
  // Ouverture du fichier
  if (wavfile.open( filepath, O_READ))
  {
    // Lecture de l'entête
    memset(&header, 0, sizeof(wavfile_header));
    int iNbRead = wavfile.read( &header, sizeof(wavfile_header));
    if (iNbRead > 0)
      bOK = true;
    //Serial.printf("CWaveFile::OpenWaveFileForRead iNbRead %d, header %d, size %d\n", iNbRead, sizeof(wavfile_header), wavfile.fileSize());
  }
  bWrite = false;
  bExpTime = bExpT;
  if (!bOK)
  {
    Serial.print("CWaveFile::OpenWaveFileForRead(");
    Serial.print(filepath);
    Serial.println(") erreur ouverture fichier !!!");
  }
  
  return bOK;
}

//-------------------------------------------------------------------------
// Fermeture du fichier
void CWaveFile::CloseWavefile()
{
#ifdef SD_FAT_TYPE
  if (wavfile.isOpen())
#else
  if (wavfile)
#endif
  {
    if (bWrite)
    {
      // Calcul de la taille
      int file_length = wavfile.size();
      // Mise à jour de l'entête
      int data_length = file_length - sizeof(struct wavfile_header);
      wavfile.seek( sizeof(struct wavfile_header) - sizeof(int));
      wavfile.write((char *)&data_length, sizeof(data_length));
      int riff_length = file_length - 8;
      wavfile.seek( 4);
      wavfile.write((char *)&riff_length, sizeof(riff_length));
    }
    // Fermeture du fichier
    wavfile.close();
    nbEch = 0;
  }
}
  
//-------------------------------------------------------------------------
// Ecriture des échantillons
// Retourne le nombre total des échantillons du fichier, 0 si erreur
unsigned long CWaveFile::WavfileWrite(
  int16_t data[],       // Pointeur sur le buffer des échantillons à écrire
  unsigned int length   // Nombre d'échantillons à écrire
  )
{
  unsigned long iReturn = 0;
#ifdef SD_FAT_TYPE
  if (wavfile.isOpen())
#else
  if (wavfile)
#endif
  {
    // Ecriture des échantillons dans le fichier
    wavfile.write( (char *)data, sizeof(int16_t)*length);
    nbEch += (long)length;
    iReturn = nbEch;
  }
  else
    Serial.println("CWaveFile::WavfileWrite fichier non initialisé !");    
  return iReturn;
}

//-------------------------------------------------------------------------
// Lecture des échantillons
// Retourne la longueur effectivement lue
long CWaveFile::WavefileRead(
  short *pData, // Pointeur sur les données à lire
  long length   // Longueur à lire
  )
{
  long lRead = 0;
#ifdef SD_FAT_TYPE
  if (wavfile.isOpen())
#else
  if (wavfile)
#endif
  {
    // Lecture des données
    if (iDecimation == 1)
      lRead = wavfile.read( pData, sizeof(int16_t)*length);
    else if (iDecimation > 1)
    {
      // Lecture de n fois plus de données dans un buffer partiel
      int16_t tmpBuff[length*iDecimation];
      lRead = wavfile.read( tmpBuff, sizeof(int16_t)*length*iDecimation);
      if (lRead > 0)
      {
        // Init du buffer de sortie avec un échantillon sur deux pour une décimation de 2
        int j, i;
        for (j=0, i=0; j<lRead; i++, j+=iDecimation)
          pData[i] = tmpBuff[j];
        lRead = i;
      }
    }
  }
  else
    Serial.println("CWaveFile::WavefileRead fichier non initialisé !");   
  //Serial.printf("CWaveFile::WavfileRead (decim %d, length %d) = %d\n", iDecimation, length, lRead);
    
  return lRead;
}

//-------------------------------------------------------------------------
// Retourne la durée d'enregistrement en cours en secondes
float CWaveFile::GetRecordDuration()
{
  // Calcul de la durée à partir du nombre d'échantillons et de la durée d'un échantillon
  // Prend en compte la durée réelle sans l'expansion de temps
  // Donc avec la fréquence d'échantillonnage
  return (float)nbEch / fFe;
}

//-------------------------------------------------------------------------
// Retourne la durée de lecture totale en secondes
float CWaveFile::GetPlayDuration()
{
  // Calcul de la durée à partir du nombre d'échantillons et de la durée d'un échantillon
  // Attention, prend en compte la durée avec ou sans l'expansion de temps
  // Donc avec 38.4kHz ou 384kHz de fréquence d'échantillonnage
  //int iNbEch = header.data_length/sizeof(short);
  int iNbEch = wavfile.fileSize() - sizeof(wavfile_header);
  float fDur = 0.0;
  int rate = header.sample_rate;
  if (bExpTime)
    rate /= 10;
  fDur = (float)iNbEch / (float)rate;

  return fDur;
}

//-------------------------------------------------------------------------
// Positionne le fichier en lecture à +/- Coef de sa durée totale
// Retourne false si la durée du fichier est dépassée ou inférieure à 0
bool CWaveFile::ReadNext(
  int iNext,  // +1 pour avancer, -1 pour reculer
  int iCoef // Coefficient d'avancement
  )
{
  long lMaxL = header.data_length / sizeof(int16_t);
  long lNext = lMaxL / iCoef;
  long lNb   = lNext * iNext * sizeof(int16_t) * iDecimation;
/*#ifdef SD_FAT_TYPE
  if (wavfile.isOpen() and wavfile.seek( lNb) and wavfile.seekCur( lNb) == true)
#else*/
  if (wavfile != 0 and wavfile.seekCur( lNb) == true)
//#endif
    return true;
  return false;
}

//-------------------------------------------------------------------------
// Positionne le fichier en lecture à une durée précise
// Retourne -1 si la durée du fichier est dépassée ou inférieure à 0
// et le nombre d'échantillons lus sinon
long CWaveFile::SetPosRead(
  unsigned long lPos  // Nombre d'échantillons ou se positionner
  )
{
  long lNb   = (lPos * sizeof(uint16_t) * iDecimation) + sizeof(wavfile_header);
#ifdef SD_FAT_TYPE
  if (wavfile.isOpen() and wavfile.seek( lNb) == 0)
#else
  if (wavfile != 0 and wavfile.seek( lNb) == 0)
#endif
    return (long)lPos;
  return -1;
}
  
//-------------------------------------------------------------------------
// Retourne la taille des données du fichier
unsigned long CWaveFile::GetDataLength()
{
  //return header.data_length;
  return wavfile.fileSize() - sizeof(wavfile_header);
}

//-------------------------------------------------------------------------
// Initialise le taux de décimation en lecture
void CWaveFile::SetDecimation(
  int iDecim  // Valeurs possibles : 1, 2 et 3
  )
{
  if (iDecim >= 1 and iDecim <= 3)
    iDecimation = iDecim;
}
