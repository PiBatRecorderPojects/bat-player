/* 
 * File:   CReader.h
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "arm_math.h"
#include "CWaveFile.h"
#include "Restitution.h"

#ifndef CREADER_H
#define CREADER_H

// Filtre accentuateur des fréquences au-dessus de 40kHz (+1dB chaque 10kHz)
const double IPlayer384k[64] = {
   0.0197313905825475,    -0.00912531360363308,    0.0134497392444181,    -0.0131150861624214,
   0.00857590184718223,   -0.00946493926513728,    0.0129458159640855,    -0.0122940401822232,
   0.0103341304251117,    -0.0135436302393533,     0.0211519903140749,    -0.0272566321933413,
   0.0287495020819946,    -0.0277530194967157,     0.027946796123301,     -0.0308943718889358,
   0.0359640119494685,    -0.0419269633767543,     0.0483398239737098,    -0.0555493763287321,
   0.064231857633522,     -0.0752867938509655,     0.0895085011398814,    -0.108119813510435,
   0.133113974587414,     -0.167356393375324,      0.216201369074152,     -0.289213054281089,
   0.404539225153469,     -0.603022642003208,      0.990000455984005,     -1.94130361746655,
   9.01901757157347,      -4.24381698464721,      -24.4705525219591,       46.6614714774939,
  -25.9110282805523,      -22.3194606873754,       48.8338606191727,      -32.3340855286791,
  -3.00393189306842,       23.4927084080529,      -19.7593715024007,       5.02940324715557,
   5.48434145548008,      -7.10778079495707,       3.68408016788494,      -0.193358635194671,
  -1.25688759825221,       1.09043843992146,      -0.440197239158831,     -0.0202694329188826,
   0.168022171491237,     -0.138022799912951,      0.0730700768177408,    -0.0258138830431079,
   0.0107197466999167,    -0.00829411531332453,    0.00790465864153048,   -0.0179262556373706,
   0.00708406080649182,   -0.0187749541194006,     0.0184624834766185,    -0.00887790128012896
};

const double IPlayer384k0_5dB[64] = {
  -0.000389126835754753,  -0.00180480631184398,   -0.00112638516708588,   -0.00062050571373125,
   0.00126536301361587,    0.0023530125033235,     0.0049353337925407,    -0.000283058377212316,
  -0.00227558842200674,   -0.00483641959823019,   -0.00755815426277803,   -0.0031538205728867,
   0.0106634342176707,     0.00159013836396866,    0.0103904733848224,     0.0129679283357128,
  -0.0204742934700352,    -0.0108595433436798,     0.028642778722521,     -0.0659879071350367,
  -0.0142503541172475,     0.121315144435559,     -0.0448933223541001,    -0.0532604911396728,
   0.206468864866948,     -0.3398464065421,       -0.179408760725462,      0.895269200957204,
   0.024989974521711,     -1.80222770917947,       2.34990406510838,       9.63321184909933,
  -10.7579461537652,      -15.2946500162803,       19.0807980815695,       14.7301362023741,
  -22.6743581994612,      -9.03307570165568,       20.9575997354401,       2.54498943226767,
  -15.7278552136526,       1.50398290769149,       9.57037105124395,      -2.7930455843993,
  -4.55705321802134,       2.4256870574917,        1.58085604218934,      -1.51835653129821,
  -0.300530485483347,      0.691591402737999,     -0.0618581484423432,    -0.203174139575948,
   0.101620612008763,      0.0201773277002756,    -0.0562274830497686,     0.00619254149386311,
   0.0128278779473074,    -0.00638489172353369,    0.00539698961408181,    0.00514563873882979,
   0.000124460449862176,  -0.00184391506097899,   -0.000347180795710631,  -0.00221273607424939
};
//!< Nombre max de coefficients pour le DSP
#define FIR_MAX_COEFFS 128

//!< Nombre d'échantillons à traiter par le DSP
#ifndef BLOCK_SAMPLES
  #if defined(__MK20DX128__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__) || defined(__IMXRT1062__)
    #define BLOCK_SAMPLES  128
  #elif defined(__MKL26Z64__)
    #define BLOCK_SAMPLES  64
  #endif
#endif

//-------------------------------------------------------------------------
//! \class CFIRFilter
//! \brief Classe qui implémente le calcul d'un filtre FIR via le DSP
/*
 * A 180MHz :
 * Temps moyen pour  32 Taps avec 128 ech 50µs  (64 tours) total 8192 ech 3226µs
 * Temps moyen pour  48 Taps avec 128 ech 67µs  (64 tours) total 8192 ech 4332µs
 * Temps moyen pour  64 Taps avec 128 ech 85µs  (64 tours) total 8192 ech 5442µs
 * Temps moyen pour  96 Taps avec 128 ech 119µs (64 tours) total 8192 ech 7655µs
 * Temps moyen pour 128 Taps avec 128 ech 154µs (64 tours) total 8192 ech 9868µs
 * A 144MHz :
 * Temps moyen pour  32 Taps avec 128 ech  63µs (64 tours) total 8192 ech 4034µs
 * Temps moyen pour  48 Taps avec 128 ech  84µs (64 tours) total 8192 ech 5419µs
 * Temps moyen pour  64 Taps avec 128 ech 106µs (64 tours) total 8192 ech 6802µs
 * Temps moyen pour  96 Taps avec 128 ech 149µs (64 tours) total 8192 ech 9571µs
 * Temps moyen pour 128 Taps avec 128 ech 192µs (64 tours) total 8192 ech 12340µs
 */
class CFIRFilter
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    CFIRFilter();

    //-------------------------------------------------------------------------
    //! \brief Initialisation des coefficients du filtre
    //! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
    //! \param pfCoef Tableau des 32 coefficients du filtre
    //! \return Retourne true si initialisation OK
    bool InitFIRFilter(
      int iNTaps,
      const double *pfCoef
      );

    //-------------------------------------------------------------------------
    //! \brief Initialisation des coefficients du filtre
    //! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
    //! \param pfCoef Tableau des 32 coefficients du filtre
    //! \return Retourne true si initialisation OK
    bool InitFIRFilter(
      int iNTaps,
      const int16_t *piCoef
      );

    //-------------------------------------------------------------------------
    //! \brief Applique le filtre sur un tableau d'échantillons
    //! \param pFirIn Tableau des échantillons en entrée du filtre (taille BLOCK_SAMPLES)
    //! \param pFirOut Tableau des échantillons en sortie du filtre (taille BLOCK_SAMPLES)
    void SetFIRFilter(
      q15_t *pFirIn,
      q15_t *pFirOut
      );

  protected:
    //! Liste des coefficients en format q15_t (max FIR_MAX_COEFFS coefficients)
    volatile q15_t lstCoeff[FIR_MAX_COEFFS];

    //! Instance du filtre ARM DSP Math library
    arm_fir_instance_q15 fir_inst;

    //! Tableau des coefficients du filtre
    volatile q15_t StateQ15[(BLOCK_SAMPLES + FIR_MAX_COEFFS)*4];
};

//-------------------------------------------------------------------------
// Classe pour la gestion de la lecture d'un fichier wav
// Gère la lecture du fichier wav et l'envoi des échantillons vers le DAC0
class CReader
{
public:
  //-------------------------------------------------------------------------
  // Constructeur (initialisation de la classe)
  CReader();

  //-------------------------------------------------------------------------
  // Destructeur (initialisation de la classe)
  ~CReader();

  //-------------------------------------------------------------------------
  // Initialisation du fichier à lire (ouverture du fichier)
  // Retourne true si ouverture OK
  bool SetWaveFile(
    bool Bexp10,        // Indique si lecture en expansion de temps x10 (true)
    char *pPathWavFile  // Pointeur vers le nom du fichier
    );

  //-------------------------------------------------------------------------
  // Ferme le fichier wav
  void CloseWavefile();
  
  //-------------------------------------------------------------------------
  // Lance la lecture du fichier sélectionné
  // Retourne true si lecture OK
  bool StartRead();

  //-------------------------------------------------------------------------
  // Stoppe la lecture du fichier en lecture
  // Retourne true si arret OK
  bool StopRead();

  //-------------------------------------------------------------------------
  // Retourne true si une lecture est en cours
  bool IsReading() {return bReading;};

  //-------------------------------------------------------------------------
  // Retourne true si une saturation a été détectée sur le fichier en cours de lecture
  bool IsSaturation() {return bSaturation;};

  //-------------------------------------------------------------------------
  // Retourne true si un signal faible a été détectée sur le fichier en cours de lecture
  bool IsSignalFaible() {return bSignalFaible;};

  //-------------------------------------------------------------------------
  // Retourne la durée en secondes du fichier wav sélectionné
  unsigned int GetWaveDuration() {return iWaveDuration;};

  //-------------------------------------------------------------------------
  // Retourne la position de lecture en secondes
  unsigned int GetWavePos() {return iWavePos;};

  //-------------------------------------------------------------------------
  // Retourne la position de lecture en % de la durée complète du fichier
  unsigned short GetPosLecture() {return iPosLecture;};

  //-------------------------------------------------------------------------
  // Retourne la fréquence d'échantillonnage du fichier wav sélectionné en Hz
  // Tient compte de l'expansion de temps éventuelle
  // Retourne 0 si aucun fichier sélectionné
  int GetFe() {return iFe;};

  //-------------------------------------------------------------------------
  // Initialise l'utilisation ou non du filtre passe haut
  void SetFiltre(
    bool bFlt
    ) {bFiltre = bFlt;};
  
  //-------------------------------------------------------------------------
  // Fonction d'initialisation d'un buffer avec les échantillons du fichier wave
  // Retourne true si OK
  bool SetBuffer(
    uint16_t *pBuffer   // Pointeur sur le buffer à initialiser
    );
  
  //-------------------------------------------------------------------------
  // Fonction de traitement des interruption DMA pour remplir les buffers
  void OnITDMA();
  
  //-------------------------------------------------------------------------
  // Initialisation de l'atténuation de lecture
  void SetAttenuation(
    int iCodeAtt  // Code de l'atténuation
    );
  
  //-------------------------------------------------------------------------
  // Retourne le nom du fichier courant éventuellement modifié suite à optimisation
  char *GetWaveFileName() {return sPathWaveFile;};
  
  //-------------------------------------------------------------------------
  //! \brief Gestion des tâches de fonds
  void OnLoop();

  // Pointeur sur l'instance unique de la classe
  static CReader *pInstance;
  
protected:
  // Indique si une lecture est en cours
  bool bReading;

  // Indique que la lecture doit stopper
  bool bNeedToStop;

  // Indique une saturation des valeurs des échantillons (plus de 1% du fichier supérieur à 2047)
  bool bSaturation;
  // Nombre de saturation pour le fichier en cours
  unsigned long iNbSaturation;
  // Max du nombre de saturation pour le fichier en cours
  unsigned long iMaxSaturation;

  // Indique un signal faible (max plus petit que 1023)
  bool bSignalFaible;

  // Indique si la lecture est en expansion de temps
  bool bExp10;

  // Fréquence d'échantillonnage de la lecture en Hz
  int iFe;

  // Nom du fichier à lire
  char sPathWaveFile[255];

  // Nombre d'échantillons du fichier wav sélectionné
  unsigned long lWaveLength;

  // Nombre d'échantillons lus
  unsigned long lWaveEchRead;

  // Durée en secondes du fichier wav sélectionné
  unsigned int iWaveDuration;

  // Indique en secondes la position de lecture
  unsigned int iWavePos;

  // Indique la position de la lecture en % par rapport au fichier complet
  unsigned short iPosLecture;

  // Taille en octet d'un buffer
  unsigned long sizeBuffer;

#if defined(__MK66FX1M0__) // Teensy 3.6
  //! Double buffers des échantillons DMA
  volatile uint16_t samplesDMA[MAXSAMPLE*2];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  //! Sur Teensy 4.1, buffer stéréo
  static DMAMEM int16_t samplesDMA[MAXSAMPLE*4];
#endif

  // Pointeurs sur le buffer 1 et 2
  volatile uint16_t *P1, *P2;

  // Buffer de lecture du fichier wav
  int16_t samplesWave[MAXSAMPLE];
  
  // Gestionnaire du fichier wave
  CWaveFile waveFile;

  // Gestionnaire de restitution audio sur le DAC0
  Restitution restitution;

  // Décalage éventuel pour la gestion de l'atténuation
  int iDec;

  // Indique si atténuation (true) ou augmentation (false)
  bool bAttenuation;

  // Valeurs max des échantillons du fichier pour détecter un signal faible
  uint16_t iMaxEch;
  int16_t iMaxWav;

  // Avec ou sans filtre passe haut
  bool bFiltre;

  // Gestionnaire d'un éventuel filtre FIR
  CFIRFilter firFilter;

  //! Buffers temporaires pour le filtre
  volatile q15_t    samplesFirIn [BLOCK_SAMPLES];
  volatile q15_t    samplesFirOut[BLOCK_SAMPLES];

  //! Vérification démarrage lecture
  bool bReadingInProgress;
  unsigned long uiWaitStartRead;

};

#endif // CREADER_H
