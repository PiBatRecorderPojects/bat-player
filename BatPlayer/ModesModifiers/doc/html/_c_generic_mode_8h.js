var _c_generic_mode_8h =
[
    [ "CGenericMode", "class_c_generic_mode.html", "class_c_generic_mode" ],
    [ "CModeError", "class_c_mode_error.html", "class_c_mode_error" ],
    [ "LogFile", "class_log_file.html", "class_log_file" ],
    [ "CGENERICMODE_H", "_c_generic_mode_8h.html#a0773355042f44414b94c974182197f8d", null ],
    [ "MAXCHAR_ERROR", "_c_generic_mode_8h.html#ade19f3091f7295db0a6d8fe6e98b932b", null ],
    [ "MAXLOGFILENAME", "_c_generic_mode_8h.html#a902a3c9f7283165c45a027b64647ef9f", null ],
    [ "MAXSOFTNAME", "_c_generic_mode_8h.html#ab74797bb886fb4350cb6550a04a2d7e6", null ],
    [ "NOMODE", "_c_generic_mode_8h.html#a7285556589730dcffce649256d672a71", null ],
    [ "LEVEL_LOG", "_c_generic_mode_8h.html#a0e5a7c27be8b99ad6db5986c94a969fd", [
      [ "LLOG", "_c_generic_mode_8h.html#a0e5a7c27be8b99ad6db5986c94a969fda5d59e7759249319d78d7f14e9e29b806", null ],
      [ "LINFO", "_c_generic_mode_8h.html#a0e5a7c27be8b99ad6db5986c94a969fda22467a43520af2ba375d9a3072592ccf", null ],
      [ "LDEBUG", "_c_generic_mode_8h.html#a0e5a7c27be8b99ad6db5986c94a969fda99a9c50ae812fdb9418bb4d04e55521e", null ],
      [ "LREALTIME", "_c_generic_mode_8h.html#a0e5a7c27be8b99ad6db5986c94a969fda62dccaeefc64d2a14e00a325c83cb434", null ]
    ] ]
];