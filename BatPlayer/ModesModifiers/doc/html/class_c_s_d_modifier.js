var class_c_s_d_modifier =
[
    [ "CSDModifier", "class_c_s_d_modifier.html#ab0d7a43893bac6ba37d30dfdf183a904", null ],
    [ "GetSDNbFiles", "class_c_s_d_modifier.html#a515aa38127c0fcb4ddd3bfd735945da6", null ],
    [ "GetString", "class_c_s_d_modifier.html#a234eaa270b7a9de0cac8fe9ca4cf3982", null ],
    [ "ReceiveKey", "class_c_s_d_modifier.html#a72a2c4149a1e60ca993c6078af03c85a", null ],
    [ "SetParam", "class_c_s_d_modifier.html#a1be40b93f5cc4704c6d35c198778d16b", null ],
    [ "StartModif", "class_c_s_d_modifier.html#a17344844e9a2d7d3dbcda88b771ae35f", null ],
    [ "cEff", "class_c_s_d_modifier.html#a8de793610810fbd6b752c28236d1c7c9", null ],
    [ "iNbFiles", "class_c_s_d_modifier.html#aff952020491e41249aa8327b78c43e35", null ],
    [ "sExtFile", "class_c_s_d_modifier.html#a5019aa48be7067584cd4a08320820175", null ]
];