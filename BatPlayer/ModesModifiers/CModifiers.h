//-------------------------------------------------------------------------
//! \file CModifiers.h
//! \brief Generic class for managing buttons
//! Management of parameters modifiers. For each parameter, there are 2 display modes:
//! - Online, for example "> Min Freq 120kHz" with "120" editable.
//!     The selected line displays an arrow at the beginning of the line
//!     In modification, the modifiable zone appears in inverse
//! - Anywhere on the screen, for example "[Player]" with "Player" editable.
//!     If the parameter is not modifiable, it is never selected
//!     If the parameter is selected, the 1st and last characters are in reverse video
//!     In modification, the modifiable zone appears in inverse
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * CGenericModifier
  *   CCharModifier
  *   CModificatorInt
  *     CSecondModifier
  *     CDrawBarModifier
  *   CFloatModifier
  *   CEnumModifier
  *     CLineEnumModifier
  *   CBoolModifier
  *     CModificatorLight
  *   CPushButtonModifier
  *   CDateModifier
  *   CHourModifier
  *   CSDModifier
  *   CPlayRecModifier
  *   CBatteryModifier
  */

#ifndef CMODIFIER_H
#define CMODIFIER_H

#define MAX_LINEPARAM 21	//!< Max number of characters in a line
#define MAX_MODIF     12	//!< Max number of editable zones for one line
#define MAX_LINES     7		//!< Max number of lines in the screen
#define HLINEPIX 9			//!< Pixel height of a line of characters
#define LCHARPIX 6			//!< Pixel widht of a line of characters

//-------------------------------------------------------------------------
//! \class CGenericModifier
//! \brief Base class of a field modifier
//! Manages all behaviors common to modifiers
//! Manages two modes of modification:
//! - Online for Settings Mode
//! - On site for other modes
class CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! Example Min Freq 120kHz
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param bPush true to indicate a push to change the value
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CGenericModifier(
    const char *pIndicModif,
    bool bLine,             
    bool bPush,             
    const char **pForm,     
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CGenericModifier();

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Display of the parameter
  //! \param x X position of the display
  //! \param y Y position of the display
  virtual void PrintParam(
    int x,
    int y 
    );

  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  // The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Stop changing the parameter
  virtual void StopModif();

  //-------------------------------------------------------------------------
  //! \brief Returns true if a change is in progress
  bool IsModif() {return bModif;};
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the parameter is to redisplay
  bool IsRedraw() {return bRedraw;};
  
  //-------------------------------------------------------------------------
  // Initialise bRedraw
  void SetRedraw( bool bNewRedraw) { bRedraw = bNewRedraw;};
  
  //-------------------------------------------------------------------------
  //! \brief Calculation of the beginnings and ends of the fields to modify
  void FindBeginEndModif();
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the parameter is valid
  bool GetbValid() {return bValid;};

  //-------------------------------------------------------------------------
  //! \brief Initializes the validity of the parameter
  void SetbValid( bool bVal);

  //-------------------------------------------------------------------------
  //! \brief Returns true if the parameter is selected
  bool GetbSelect() {return bSelect;};

  //-------------------------------------------------------------------------
  //! \brief Initializes the parameter selection
  void SetbSelect( bool bSel);

  //-------------------------------------------------------------------------
  //! \brief Initialise bCalcul
  void SetbCalcul( bool bNewCalcul) { bCalcul = bNewCalcul;};
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if we are on the last fiel of the parameter
  bool IsLastZone();

  //-------------------------------------------------------------------------
  //! \brief Displaying a string on the screen
  //! \param pString String to display
  //! \param x Position x of the beginning of the string
  //! \param y Position y of the beginning of the string
  //! \param bInverse Normal or inverted display
  void DrawString(
    char *pString,     
    int x,             
    int y,             
    bool bInverse=false
    );

  //-------------------------------------------------------------------------
  //! \brief Go to the next right field if present
  //! \return Returns false if there is no more field
  bool NextZone();

  //-------------------------------------------------------------------------
  //! \brief Returns true if there are multiple fields to edit
  bool GetbZones() { return bZones;};

  //-------------------------------------------------------------------------
  //! \brief Returns the format string
  const char **GetFormat() {return pFormat;};

  //-------------------------------------------------------------------------
  //! \brief Set the format string
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  const void SetFormat(const char **pForm) {pFormat = pForm;};

  //-------------------------------------------------------------------------
  //! \brief Returns the string of editable fields
  const char *GetindicModif() {return indicModif;};

  //-------------------------------------------------------------------------
  //! \brief Initializes the X coordinate of the top left point of the modifier
  void SetX( int x) { iX = x;};

  //-------------------------------------------------------------------------
  //! \brief Initializes the Y coordinate of the top left point of the modifier
  void SetY( int y) { iY = y;};

  //-------------------------------------------------------------------------
  //! \brief Returns the X coordinate of the top left point of the modifier
  int GetX() { return iX;};

  //-------------------------------------------------------------------------
  //! \brief Returns the Y coordinate of the top left point of the modifier
  int GetY() { return iY;};

  //-------------------------------------------------------------------------
  //! \brief Initializes bEditable
  void SetEditable( bool bEdit) { bEditable = bEdit;};

  //-------------------------------------------------------------------------
  //! \brief Return bEditable value
  bool IsEditable() { return bEditable;};

  //-------------------------------------------------------------------------
  //! \brief Returns true if the parameter is editable only by push
  bool IsModifPush() { return bModifPush;};

  //-------------------------------------------------------------------------
  //! \brief Returns true if the parameter is in line
  bool IsInLine() { return bOnLine;};

  //-------------------------------------------------------------------------
  //! \brief Return the language used
  static int GetLanguage() { return iLanguage;};

  //-------------------------------------------------------------------------
  //! \brief Initialize the language used
  static void SetLanguage( int iL) { iLanguage = iL;};

  //-------------------------------------------------------------------------
  //! \brief Indicates whether the Left / Right buttons are used
  static void SetbLeftRight( bool bLR) { bLeftRight = bLR;};
  
  //-------------------------------------------------------------------------
  //! \brief Get font for string print
  static uint8_t *GetCharFont() {return pCharFont;};
  
  //-------------------------------------------------------------------------
  //! \brief Set font for string print
  static void SetCharFont( uint8_t *pFont) {pCharFont = pFont;};
  
  //-------------------------------------------------------------------------
  //! \brief Get font for arrow on selected line
  static uint8_t *GetArrowFont() {return pArrowFont;};
  
  //-------------------------------------------------------------------------
  //! \brief Set font for arrow on selected line
  static void SetArrowFont( uint8_t *pFont) {pArrowFont = pFont;};
  
  //-------------------------------------------------------------------------
  //! \brief Get char for arrow on selected line
  static char GetArrow() {return cArrow;};
  
  //-------------------------------------------------------------------------
  //! \brief Set char for arrow on selected line
  static void SetArrow( char cChar) {cArrow = cChar;};
  
protected:
  //! Indicates whether the parameter is selected
  bool bSelect;
  
  //! Indicates whether the parameter is being modified
  bool bModif;
  
  //! Indicates whether the parameter is to be displayed again
  bool bRedraw;
  
  //! Indicates whether the parameter changes value only by a push
  bool bModifPush;
  
  //! Indicates whether the parameter is editable
  bool bEditable;

  //! Indicates whether the parameter is to be recalculated (for some parameters, false by default)
  bool bCalcul;
  
  //! Position of the edit cursor (-1 no cursor)
  int iCurpos;
  
  //! Parameter display line
  char lineParam[MAX_LINEPARAM+5];
  
  //! Indicator of modifiable characters on the line
  //! $ indicates an editable area
  //! & indicates the beginning of an editable area
  //! the other characters indicate a non-modifiable area
  //! Example Volume : 28
  //!         Volume : &$       
  char indicModif[MAX_LINEPARAM+5];
  
  //! Parameter format like " Min Freq %04kHz"
  const char **pFormat;
  
  //! Start index of editable areas
  int ilstSIdxEditable[MAX_MODIF];
  
  //! End index of editable areas
  int ilstEIdxEditable[MAX_MODIF];
  
  //! Min index of the parameter value field
  int iIdxMin;
  
  //! Max index of the parameter value field
  int iIdxMax;
  
  //! Index of the field to modify
  int iIdxModif;
  
  //! Indicates whether there are multiple fields to edit
  bool bZones;
  
  //! Indicates whether the parameter is valid or not
  bool bValid;

  //! Indicates the number of editable zones
  int iNbZones;

  //! Width of a character in pixel
  static int iWidthCar;
  
  //! Height of a character in pixel
  static int iHeightCar;

  //! Indicates whether the Left / Right buttons are used
  static bool bLeftRight;
  
  //! Indicates whether the change is of line type (in the parameter mode) or not
  bool bOnLine;

  //! X and Y coordinate of the top left corner of the modifier
  int iX, iY;

  //! Language index
  static int iLanguage;
  
  //! Font for string print
  static uint8_t *pCharFont;
  
  //! Font for arrow on selected line
  static uint8_t *pArrowFont;
  
  //! Char for arrow on selected line
  static char cArrow;
};

//-------------------------------------------------------------------------
//! \class CCharModifier
//! \brief Management class for a string modifier
class CCharModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Name &$$$$$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Name %s"
  //! \param pPar Pointer on the string to modify
  //! \param iLen Number of characters
  //! \param bOnlyNumber Indicates whether only numbers 0 t0 9 are allowed
  //! \param bWhit Indicates whether whites are allowed
  //! \param bSymb Indicates whether symbols are allowed
  //! \param bTiret Indicates whether the - and _ symbols are allowed
  //! \param bNumb Indicates whether digits are allowed
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CCharModifier(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    char *pPar,             
    int iLen,               
    bool bOnlyNumber,       
    bool bWhit,             
    bool bSymb,             
    bool bTiret,            
    bool bNumb,             
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Name Toto"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

protected:
  //! Pointer on the string to modify
  char *pParam;
  
  //! Number of characters
  int iNbChar;

  //! Indicates whether only numbers 0 t0 9 are allowed
  bool bNumbersOnly;
  
  //! Indicates whether whites are allowed
  bool bWhite;
  
  //! Indicates whether symbols are allowed
  bool bSymboles;

  //! Indicates whether the - and _ symbols are allowed
  bool bTirets;
  
  //! Indicates whether digits are allowed
  bool bNumbers;
};

//-------------------------------------------------------------------------
//! \class CModificatorInt
//! \brief Management class of a integer type modifier
class CModificatorInt : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur (initialisation des paramètres aux valeurs par défaut)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param pPar Pointer on the integer to modify
  //! \param iMin Min value for integer to modify
  //! \param iMax Max value for integer to modify
  //! \param istep Step to increase integer
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CModificatorInt(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    int *pPar,              
    int iMin,               
    int iMax,               
    int istep,              
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Return Min value for integer to modify
  int GetMin() { return iMinimum;};

  //-------------------------------------------------------------------------
  //! \brief Initialize Min value for integer to modify
  void SetMin( int iMin);

  //-------------------------------------------------------------------------
  //! \brief Return Max value for integer to modify
  int GetMax() { return iMaximum;};

  //-------------------------------------------------------------------------
  //! \brief Initialize Max value for integer to modify
  void SetMax( int iMax);

protected:
  //! Pointer on the integer to modify
  int *pParam;
  
  //! Modified temporary value
  int iTemp;

  //! Min value for integer to modify
  int iMinimum;
  
  //! Max value for integer to modify
  int iMaximum;
  
  //! Step to increase integer
  int iStep;
  
  //! Indicates whether a sign is present or not
  bool bSigne;
};

//-------------------------------------------------------------------------
//! \class CFloatModifier
//! \brief Management class of a float type modifier
class CFloatModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %05.1fkHz"
  //! \param pPar Pointer on the float to modify
  //! \param fMin Min value for float to modify
  //! \param fMax Max value for float to modify
  //! \param fstep Step to increase float
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CFloatModifier(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    float *pPar,            
    float fMin,             
    float fMax,             
    float fstep,            
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120.0kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Return Min value for float to modify
  float GetMin() { return fMinimum;};

  //-------------------------------------------------------------------------
  //! \brief Initialize Min value for float to modify
  void SetMin( float fMin);

  //-------------------------------------------------------------------------
  //! \brief Return Max value for float to modify
  float GetMax() { return fMaximum;};

  //-------------------------------------------------------------------------
  //! \brief Initialize Min value for float to modify
  void SetMax( float fMax);

protected:
  //! Pointer on the float to modify
  float *pParam;
  
  //! Modified temporary value
  float fTemp;

  //! Min value of the parameter
  float fMinimum;
  
  //! Max value of the parameter
  float fMaximum;
  
  //! Step value of the parameter
  float fStep;
  
  //! Indicates whether a sign is present or not
  bool bSigne;
};

//-------------------------------------------------------------------------
//! \class CEnumModifier
//! \brief Management class of a enum type modifier
class CEnumModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param bPush true to indicate a push to change the value
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param pPar Pointer on the integer to modify
  //! \param iMax Max value of the enum (always starts at 0)
  //! \param pStrings Pointer on an array of strings for different values of the enum
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CEnumModifier(
    const char *pIndicModif,
    bool bLine,             
    bool bPush,             
    const char **pForm,     
    int *pPar,              
    int iMax,               
    const char **pStrings,  
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CEnumModifier();

  //-------------------------------------------------------------------------
  //! \brief To change array of strings for different values of the enum
  //! \param pStrings Pointer on an array of strings for different values of the enum
  void SetStringArray(
    const char **pStrings
    );

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Initialize validity of an element (by default, all are valid)
  //! \param idx Index of the element
  //! \param bval validity 
  void SetElemValidity( int idx, bool bVal);

  //-------------------------------------------------------------------------
  //! \brief Return the validity of an element
  //! \param idx Index of the element
  //! Return validity of the element
  bool GetElemValidity( int idx);

  //-------------------------------------------------------------------------
  //! \brief Choose the next valid value
  void NextValue();

  //-------------------------------------------------------------------------
  //! \brief Choose the prev valid value
  void PrevValue();

protected:
  //! Pointer on the integer to modify
  int *pParam;

  //! Modified temporary value
  int iTemp;
  
  //! Max value of the enum
  int iValMax;
  
  //! Pointer on an array of strings for different values of the enum
  const char **pTbStrings;
  
  //! Pointer to an arry of boolean indicating the validity of each element of the enum
  bool *pTbVal;
};

//-------------------------------------------------------------------------
//! \class CLineEnumModifier
//! \brief Management class of a enum type modifier modified with all vaules in a line: [24kHz 48kHz 96kHz]
class CLineEnumModifier : public CEnumModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param bPush true to indicate a push to change the value
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param pPar Pointer on the integer to modify
  //! \param iMax Max value of the enum (always starts at 0)
  //! \param pStrings Pointer on an array of strings for different values of the enum
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CLineEnumModifier(
    const char *pIndicModif,
    bool bLine,             
    bool bPush,             
    const char **pForm,     
    int *pPar,              
    int iMax,               
    const char **pStrings,  
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Display of the parameter
  //! \param x X position of the display
  //! \param y Y position of the display
  virtual void PrintParam(
    int x,
    int y 
    );
};

//-------------------------------------------------------------------------
//! \class CEnumModifier
//! \brief Management class of a boolean type modifier
class CBoolModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param bPush true to indicate a push to change the value
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param pPar Pointer on the boolean to modify
  //! \param pStrings Pointer on an array of strings for the two values of the boolean
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CBoolModifier(
    const char *pIndicModif,
    bool bLine,             
    bool bPush,             
    const char **pForm,     
    bool *pPar,             
    const char **pStrings,  
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Filter Yes"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

protected:
  //! Pointer on the boolean to modify
  bool *pParam;

  //! Modified temporary value
  bool iTemp;
  
  //! Pointer on an array of strings for different values of the boolean
  const char **pTbStrings;
};

//-------------------------------------------------------------------------
//! \class CPushButtonModifier
//! \brief Management class of a push button type modifier
class CPushButtonModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CPushButtonModifier(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Filter Yes"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Initialize string to print
  void SetString(
    char *pStr) {pString = pStr;};

  //-------------------------------------------------------------------------
  //! \brief Returns true if the button has been enabled and returns the flag to false
  bool GetbPush();

private:
  //! Indicates if the button has been activated
  bool bPush;

  //! Possible pointer to the text to display (NULL by default)
  char *pString;
};

//-------------------------------------------------------------------------
//! \class CDateModifier
//! \brief Management class of a date type modifier
class CDateModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CDateModifier(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Gives the max of one month depending on the year
  int GetMaxMonth( int iMois, int Annee);

  //-------------------------------------------------------------------------
  //! \brief Indicates if a year is a bissextile
  bool IsBissextile( int Annee);

protected:
  //! Memo of the current day
  int iNewJ;
  //! Memo of the current month
  int iNewM;
  //! Memo of the current year
  int iNewA;
};

//-------------------------------------------------------------------------
//! \class CHourModifier
//! \brief Management class of a hour type modifier
class CHourModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param pStrH Possible time string (hh: mm or hh: mm: ss) to modify (current time if NULL)
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CHourModifier(
    const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
    bool bLine,               // true to indicate a change on a line, false otherwise
    const char **pForm,       // Parameter format like " Min Freq %04dkHz"
    char *pStrH,              // Possible time string (hh: mm or hh: mm: ss) to modify (current time if NULL)
    int ix,                   // X coordinate of the top left point of the modifier
    int iy                    // Y coordinate of the top left point of the modifier
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

protected:
  //! Pointer memo on the parameter to be modified (NULL if current time)
  char *pStrHeure;
  
  //! Memo of the current hour
  int iNewH;
  //! Memo of the current minute
  int iNewM;
  //! Memo of the current second
  int iNewS;
};

//-------------------------------------------------------------------------
//! \class CSDModifier
//! \brief Management class of an SD card type modifier
//! Displays the remaining size on the SD card
//! On modification, displays information on the SD card and proposes to erase wav files present on the card
class CSDModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Min Freq %04dkHz"
  //! \param ext PExtension files to delete ("wav" for exemple, "*" for all)
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CSDModifier(
    const char *pIndicModif,
    bool bLine,             
    const char **pForm,     
    const char *ext,        
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Returns the free size of the SD card in GO
  static float GetSDFreeSpace();
  
  //-------------------------------------------------------------------------
  //! \brief Returns the free size of the SD card as a percentage
  //! Requires to call GetSDFreeSpace before
  static int GetSDPourcentFreeSpace() {return iFreeSizeSD;};
  
  //-------------------------------------------------------------------------
  //! \brief Returns the number of files matching with sExtFiles on the SD card
  //! CAUTION only read the root, not the subdirectories
  uint16_t GetSDNbFiles();
  
protected:
  //! Character Y / N for the choice to delete files
  char cEff;

  //! Free size of the SD card in GO
  static float fFreeSizeSD;

  //! Free size of the SD card as a percentage
  static int iFreeSizeSD;

  //! Number of files on the SD card
  int iNbFiles;

  //! Extension files to delete ("wav" for exemple, "*" for all)
  char sExtFile[15];
};

#define PBREAK 0 //!< Break value
#define PPLAY  1 //!< Play value
#define PREC   2 //!< Record value

//-------------------------------------------------------------------------
//! \class CPlayRecModifier
//! \brief Management class of an Break/Play/Rec button type modifier
class CPlayRecModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! Never in line
  //! \param pPar Pointer on the parameter to modify
  //! \param bPlay Indicates if the button can take the PLAY aspect
  //! \param bRec Indicates if the button can take the REC  aspect
  //! \param iWid Button width in pixel
  //! \param iHeig Button height in pixel
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CPlayRecModifier(
    int *pPar,                // Pointer on the parameter to modify
    bool bPlay,               // Indicates if the button can take the PLAY aspect
    bool bRec,                // Indicates if the button can take the REC  aspect
    int iWid,                 // Button width in pixel
    int iHeig,                // Button height in pixel
    int ix,                   // X coordinate of the top left point of the modifier
    int iy                    // Y coordinate of the top left point of the modifier
    );

  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

  //-------------------------------------------------------------------------
  //! \brief Display of the parameter
  virtual void PrintParam(
    int x,    // X position of the display
    int y     // Y position of the display
    );

protected:
  //! Pointer on the parameter to modify
  int *pParam;
  
  //! Indicates if the button can take the PLAY aspect
  bool bPlayer;
  
  //! Indicates if the button can take the RECORD aspect
  bool bRecord;
  
  //! Button width in pixel
  int iWidth;
  
  //! Button height in pixel
  int iHeigth;
};

//-------------------------------------------------------------------------
//! \class CBatteryModifier
//! \brief Management class of a battery type modifier (0 to 100%)
class CBatteryModifier : public CGenericModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! Never in line
  //! \param pPar Pointer on the parameter to modify
  //! \param iWid Button width in pixel
  //! \param iHeig Button height in pixel
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CBatteryModifier(
    int *pPar,
    int iWid, 
    int iHeig,
    int ix,   
    int iy    
    );

  //-------------------------------------------------------------------------
  //! \brief Display of the parameter
  virtual void PrintParam(
    int x,    // X position of the display
    int y     // Y position of the display
    );

private:
  //! Pointer on the parameter to modify
  int *pParam;

  //! Button width in pixel
  int iWidth;
  
  //! Button height in pixel
  int iHeigth;
};

//-------------------------------------------------------------------------
//! \class CDrawBarModifier
//! \brief Management class of a bargraph type modifier from 0 to 100%
class CDrawBarModifier : public CModificatorInt
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! Never in line
  //! \param pPar Pointer on the parameter to modify
  //! \param iWid Button width in pixel
  //! \param iHeig Button height in pixel
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  //! \param iMin Min value (0 by default)
  //! \param iMax Max value (100 by default)
  CDrawBarModifier(
    int *pPar,  
    int iWid,   
    int iHeig,  
    int ix,     
    int iy,     
    int iMin=0, 
    int iMax=100
    );

  //-------------------------------------------------------------------------
  //! \brief Display of the parameter
  virtual void PrintParam(
    int x,    // X position of the display
    int y     // Y position of the display
    );

private:
  //! Box width in pixel
  int iWidth;
  
  //! Box height in pixel
  int iHeigth;
};

//-------------------------------------------------------------------------
//! \class CModificatorLight
//! \brief Management class of a Boolean type modifier for screen brightness
//! (normal-false / weak-true) Can not be displayed in line mode
class CModificatorLight : public CBoolModifier
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! Never in line
  //! \param pPar Pointer on the parameter to modify
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CModificatorLight(
    bool *pPar,
    int ix,    
    int iy     
    );

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();
};

//! \enum TYPES_SECONDMODIFIER
//! \brief list of types of seconds modification
enum TYPES_SECONDMODIFIER {
  SECOND_HMS    = 0, //!< Modification type "hh:mm:ss"
  SECOND_HM     = 1, //!< Modification type "hh:mm"
  SECOND_MS     = 2  //!< Modification type "mm:ss"
};

//-------------------------------------------------------------------------
//! \class CSecondModifier
//! \brief Management class of a seconds modifier in time mode (hh:mm:ss)
class CSecondModifier : public CModificatorInt
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param TypeSecModif type of seconds modification (enum TYPES_SECONDMODIFIER)
  //! \param bLine true to indicate a change on a line, false otherwise
  //! \param pForm Parameter format like " Duration %s"
  //! \param pPar Pointer on the integer to modify
  //! \param iMin Min value
  //! \param iMax Max value for integer to modify, maximum allowed value 86400 (23:59:59)
  //! \param ix X coordinate of the top left point of the modifier
  //! \param iy Y coordinate of the top left point of the modifier
  CSecondModifier(
    int TypeSecModif,
    bool bLine,             
    const char **pForm,     
    int *pPar,
    int iMin,
    int iMax,               
    int ix,                 
    int iy                  
    );

  //-------------------------------------------------------------------------
  //! \brief Key Event Processing
  virtual void ReceiveKey(
    int iKey  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Returns a parameter string
  //! The return chain looks like " Min Freq 120kHz"
  virtual char *GetString();

  //-------------------------------------------------------------------------
  //! \brief Updating the parameter with the displayed string
  virtual void SetParam();

  //-------------------------------------------------------------------------
  //! \brief Start changing the parameter
  virtual void StartModif();

protected:
  //! Pointer on the string to be modified (hh:mm:ss)
  char strTime[10];
  
  //! Type of seconds modification (enum TYPES_SECONDMODIFIER)
  int iTypeSecModif;
  //! Current hour value
  int iH;
  //! Current minute value
  int iM;
  //! Currenr second value
  int iS;
};

#endif  /* CMODIFIER_H */


