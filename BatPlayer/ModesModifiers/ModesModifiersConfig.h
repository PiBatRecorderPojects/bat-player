/* 
 * File:   ModesModifiersConfig.h
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Configuring the ModesModifiers library

#ifndef MODESMODIFIERSCONFIG_H
#define MODESMODIFIERSCONFIG_H

/*
  To add a language, you must:
  - Add the language to the LANGUAGE enum at the start of the CModifiers.h file
  - Add to each string of the ConstChar.c file the translation of the message in the new language
*/
//! \enum LANGUAGE
//! \brief Languages that can be used for operator messages
enum LANGUAGE {
  LENGLISH    = 0,	//!< English language
  LFRENCH     = 1,	//!< French language
  LGERMAN     = 2,  //!< German language
  LDUTCH      = 3,  //!< Dutch language
  MAXLANGUAGE = 4
};

// Selecting the SD Card Management Library
// Uncomment the library used

// For SDFat version befor V2.0.0
// Selecting the SDFat with SdFatSdio or SdFatSdioEX library https://github.com/greiman/SdFat
//#define MMC_SDFAT
//#define MMC_SDFAT_EXT

// SD_FAT_TYPE = 0 for SdFat/File as defined in SdFatConfig.h,
// 1 for FAT16/FAT32, 2 for exFAT, 3 for FAT16/FAT32 and exFAT.
#define SD_FAT_TYPE 0

// Selecting the SDFs library https://github.com/greiman/SdFs
// WARNING SDFs is not working for ESP32
//#define MMC_SDFS

#endif // MODESMODIFIERSCONFIG_H
