/* 
 * File:   CTestModes.h
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiers.h"

// Classs to test CGenericMode and Modifiers

// Tests Modes
enum MODELIST
{
  MODETEST    = 1,
  MODEPARAMSB = 2,
  MODEPARAMSA = 3,
  MODEERROR   = 4
};

// Enum for testing
enum RECORD_MODE
{
  AUTOMATIC_REC   = 0,
  HETERODYNE_REC  = 1,
  MICTEST_REC     = 2,
  MAX_REC         = 3
};

// Enum for threshold mode
enum THRESHOLDMODE
{
  RELATIVE_THRESHOLD  = 0,
  ABSOLUTE_THRESHOLD  = 1,
  MAXMODE_THRESHOLD   = 2
};

#define MAXCHARNAME 8

struct Parameters
{
  char  sName[MAXCHARNAME+1]; // String to test CCharModifier
  int   iDuration;            // Integer to test CModificatorInt (5 to 10, step 1)
  float fFrequency;           // Float to test CFloatModifier (15 to 130 step 0.1)
  int   iRecordMode;          // Enum to test CEnumModifier
  bool  bAutoHeterodyne;      // Bool to test CBoolModifier
  char  sTime[8];             // String to test CHourModifier (hh:mm)
  int   iPlayMode;            // Integer to test CPlayRecModifier
  int   iRecMode;             // Integer to test CPlayRecModifier
  int   iBattery;             // Integer to test CBatteryModifier
  int   iDrawBar;             // Integer to test CDrawBarModifier
  bool  bLight;               // Bool to test CModificatorLum
  int   iLanguage;            // To test language change
  int   iModeThreshold;       // Mode threshold to test valid or not
  int   iRelativeThreshold;   // Relative threshold
  int   iAbsoluteThreshold;   // Absolute threshold
};

//-------------------------------------------------------------------------
// Generic class for testing
class CGenericTest: public CGenericMode
{
public:
  //-------------------------------------------------------------------------
  // Constructor (initialization of parameters to default values)
  CGenericTest();

  //-------------------------------------------------------------------------
  // Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  // Reading saved settings
  virtual bool ReadParams();
  
  //-------------------------------------------------------------------------
  // Writing parameters to save them
  virtual void WriteParams();

protected:
  // Common parameters for tests modes
  Parameters params;
};

// Liste of Mode Test modifiers
enum TESTMODE_LIST
{
  IDXMT_MODE        = 0,
  IDXMT_LIGHT       = 1,
  IDXMT_BAT         = 2,
  IDXMT_CURDATE     = 3,
  IDXMT_CURTIME     = 4,
  IDXMT_PLAY        = 5,
  IDXMT_REC         = 6,
  IDXMT_POS         = 7,
  IDXMT_TIME        = 8,
  IDXMT_TMAX        = 9,
  IDXMT_MAX         = 10 
};

//-------------------------------------------------------------------------
// Class for testing Modifiers not in line
class CModeTest: public CGenericTest
{
public:
  //-------------------------------------------------------------------------
  // Constructor (initialization of parameters to default values)
  CModeTest();

  //-------------------------------------------------------------------------
  // Beginning of the mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  // Called on end of change of a modifier
  // If a mode change, returns the requested mode
  virtual int OnEndChange(
    int idxModifier // Index of affected modifier
    );

  //-------------------------------------------------------------------------
  // Mode display on the screen
  // This method is called regularly by the main loop
  // By default, it displays the list of modifiers
  virtual void PrintMode();
  
protected:
  // Play/Rec position
  int iPlayrecPos;

  // For time simulation Play/Rec
  unsigned long uiTime;
  int iTime, iTimeMax;

  // New mode
  int iNewMode;
};

// Liste of Mode Parameters A modifiers
enum PARAMETERSA_LIST
{
  IDXPA_RETURN      = 0,
  IDXPA_CURTIME     = 1,
  IDXPA_CURDATE     = 2,
  IDXPA_NAME        = 3,
  IDXPA_DURSTEP     = 4,
  IDXPA_DURDIGIT    = 5,
  IDXPA_FREQUSTEP   = 6,
  IDXPA_FREQUDIGIT  = 7,
  IDXPA_RECMODE     = 8,
  IDXPA_HETERODYNE  = 9,
  IDXPA_TIME        = 10,
  IDXPA_BAT         = 11,
  IDXPA_LIGHT       = 12,
  IDXPA_LANGUAGE    = 13,
  IDXPA_SD          = 14,
  IDXPA_MAX         = 15
};

//-------------------------------------------------------------------------
// Class for testing Modifiers in line
class CModeParametersA: public CGenericTest
{
public:
  //-------------------------------------------------------------------------
  // Constructor (initialization of parameters to default values)
  CModeParametersA();

  //-------------------------------------------------------------------------
  // Beginning of the mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  // Called on end of change of a modifier
  // If a mode change, returns the requested mode
  virtual int OnEndChange(
    int idxModifier // Index of affected modifier
    ); 

protected:
  // string for battery text
  char sTxtBat[MAX_LINEPARAM];
};

// Liste of Mode Parameters B modifiers
enum PARAMETERSB_LIST
{
  IDXPB_MODE        = 0,
  IDXPB_LIGHT       = 1,
  IDXPB_BAT         = 2,
  IDXPB_CURTIME     = 3,
  IDXPB_CURDATE     = 4,
  IDXPB_NAME        = 5,
  IDXPB_MTHRESHOLD  = 6,
  IDXPB_RTHRESHOLD  = 7,
  IDXPB_ATHRESHOLD  = 8,
  IDXPB_DURSTEP     = 9,
  IDXPB_DURDIGIT    = 10,
  IDXPB_FREQUSTEP   = 11,
  IDXPB_FREQUDIGIT  = 12,
  IDXPB_RECMODE     = 13,
  IDXPB_HETERODYNE  = 14,
  IDXPB_TIME        = 15,
  IDXPB_LANGUAGE    = 16,
  IDXPB_SD          = 17,
  IDXPB_MAX         = 18 
};

//-------------------------------------------------------------------------
// Class for testing Modifiers normal and in line
class CModeParametersB: public CGenericTest
{
public:
  //-------------------------------------------------------------------------
  // Constructor (initialization of parameters to default values)
  CModeParametersB();

  //-------------------------------------------------------------------------
  // Beginning of the mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  // Called on end of change of a modifier
  // If a mode change, returns the requested mode
  virtual int OnEndChange(
    int idxModifier // Index of affected modifier
    );  
};

