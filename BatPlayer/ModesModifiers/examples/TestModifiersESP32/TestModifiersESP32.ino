/* 
 * File:   TestModifier.ino
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * WARNING - WARNING - WARNING - WARNING - WARNING
 * For a short call to the function sd.vol()->freeClusterCount() (except the first call made in the init) 
 * MAINTAIN_FREE_CLUSTER_COUNT must be set to 1 in SdFatConfig.h (SdFat library to download on https://github.com/greiman/SdFat)
 * 
 * ESP32 Test program for Modifiers, Generic mode and Log
 * Use card type WEMOS LOLIN32 
 */

#include <SPI.h>
#include "ModesModifiersConfig.h"
#ifdef MMC_SDFAT
  #include "SdFat.h"
#endif
#ifdef MMC_SDFS
  #include "SdFs.h"
#endif
#include <time.h>
#include <sys/time.h>                   // struct timeval
#include <TimeLib.h>
#include <stdio.h>
#include "U8g2lib.h"
#include "CGenericMode.h"
#include "CTestModes.h"

// SPI Pins for SD Card
const uint8_t SCLK_SD =  14; // SCLK
const uint8_t MISO_SD =   2; // MISO (Master Input Slave Output)
const uint8_t MOSI_SD =  15; // MOSI (Master Output Slave Input)
const uint8_t CS_SD   =  13; // SD CS

/*
 * Pins for multidirectionnal keys
 */
#define PIN_UP      25
#define PIN_DOWN     4
#define PIN_RIGHT   39
#define PIN_LEFT    36
#define PIN_CENTER   0

#define SDSPEED SD_SCK_MHZ(20)
#ifdef MMC_SDFAT
  SdFat sd;
#endif
#ifdef MMC_SDFS
  SdFs sd;
#endif

// Screen manager
U8G2 *pDisplay = NULL;

// Key manager
CKeyManager KeyManag;

// Log file manager
LogFile logTest("TstMod");

// Storing the loop time for button and screen treatments
unsigned long ulTimeKey;
unsigned long ulTimePrint;
// Time in ms between two buttons controls
#define TIMEKEY 10
// Time in ms between two screen display
#define TIMEPRINT 200

// GR Logo bitmap
extern const unsigned char LogoGR [];

// Comment to test the dynamic creation of the modes of operation (at a given moment a single mode of operation exists in memory)
// Otherwise, the operating modes are static (the operating modes all exist in memory for the duration of the program)
#define TEST_STATIC

#ifdef TEST_STATIC
  // Static modes of operation
  CModeTest         modeTest;
  CModeParametersA  modeParamsA;
  CModeParametersB  modeParamsB;
  CModeError        modeError;
#endif

//----------------------------------------------------
// Function to create or activate a new mode
CGenericMode* CreateNewMode(
  CGenericMode* pOldMode,   // Pointer on the previous mode of operation
  int idxMode               // New mode of operation index
  )
{
  CGenericMode* pNewMode = NULL;
#ifdef TEST_STATIC
  // Static creation of operating modes
  // We just swap the pointer on the current operating mode
  switch (idxMode)
  {
  case MODETEST    : pNewMode = &modeTest;     Serial.print("Swap to CModeTest");          break;
  case MODEPARAMSA : pNewMode = &modeParamsA;  Serial.print("Swap to CModeParametersA");   break;
  case MODEPARAMSB : pNewMode = &modeParamsB;  Serial.print("Swap to CModeParametersB");   break;
  case MODEERROR   : pNewMode = &modeError;    Serial.print("Swap to CModeError");         break;
  }
#else
  // Dynamic creation of operating modes
  // First, destruction of the previous mode to free memory
  delete pOldMode;
  // Second, creating the new mode of operation in memory
  switch (idxMode)
  {
  case MODETEST    : pNewMode = new CModeTest();         Serial.print("new CModeTest");          break;
  case MODEPARAMSA : pNewMode = new CModeParametersA();  Serial.print("new CModeParametersA");   break;
  case MODEPARAMSB : pNewMode = new CModeParametersB();  Serial.print("new CModeParametersB");   break;
  case MODEERROR   : pNewMode = new CModeError();        Serial.print("new CModeError");         break;
  }
#endif
  // Print ram usage after new, if used heap is not identical after a same mode, delete is missing....
  uint32_t FreeHeap = ESP.getFreeHeap();     // Available heap
  Serial.printf(" Free Heap %d\n", FreeHeap);
  return pNewMode;
}

//----------------------------------------------------
// Function to date files
void dateTime(uint16_t* date, uint16_t* timeh)
{
  time_t curtime;
  struct tm *timeinfo;
  time(&curtime);
  timeinfo = localtime (&curtime);
  // return date using FAT_DATE macro to format fields
  *date = FAT_DATE(timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday);

  // return time using FAT_TIME macro to format fields
  *timeh = FAT_TIME(timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
}

//----------------------------------------------------
// Print Logo
void PrintLogo()
{
  pDisplay->clearBuffer();
  pDisplay->drawXBM(0, 0, 128, 64, LogoGR);
  pDisplay->setFont(u8g2_font_6x13B_mf);  // Hauteur 9
  pDisplay->setFontPosTop();
  pDisplay->setCursor( 2, 27);
  pDisplay->print( "Modifiers");
  pDisplay->setFont(u8g2_font_micro_mr);  // Hauteur 5
  pDisplay->setCursor( 2, 2);
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  pDisplay->setCursor( 2, 52);
  pDisplay->print( VERSION_MODIFIERS);
  pDisplay->drawRFrame( 0, 0, 128, 64, 0);
  pDisplay->sendBuffer();
  delay(1000);
}

//-------------------------------------------------------------------------
// Create screen manager
void CreateGestScreen()
{
  pDisplay = new U8G2_SSD1306_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);  // Gestionnaire écran
  // Initialisation
  pDisplay->begin();
}

//-------------------------------------------------------------------------
// SD card init
bool InitSD()
{
  bool bReturn = false;
  // Initialize SD manager
  SPI.begin(SCLK_SD, MISO_SD, MOSI_SD, CS_SD);
#ifdef MMC_SDFAT
  if (!sd.begin( CS_SD, SDSPEED))
    Serial.println("SD begin() failed !!!");
  else
    bReturn = true;
  if (bReturn)
    // First long call for short call then
    sd.vol()->freeClusterCount();
#endif
#ifdef MMC_SDFS
    Serial.println("SDFs not working with ESP32 !!!");
#endif
  return bReturn;
}

//----------------------------------------------------------------------------
// Program setup
void setup()
{
  // Update to the system date
  struct tm start;
  start.tm_year = 2019 - 1900;
  start.tm_mon  = 0;
  start.tm_mday = 10;
  start.tm_hour = 10;
  start.tm_min  = 10;
  start.tm_sec  = 10;
  time_t t = mktime(&start);
  timeval now = { t, 0 };
  settimeofday(&now, NULL);
  // With trace on console
  logTest.SetbConsole( true);
  // Initialize language
  CGenericMode::SetLanguage( LENGLISH);
  // Initializing the console output
  Serial.begin(115200);
  Serial.println("====================================");
  Serial.println("=============== TEST ===============");

  // Initializing the Button Manager
  // With right and left buttons
  KeyManag.Begin( PIN_CENTER, PIN_UP, PIN_DOWN, PIN_RIGHT, PIN_LEFT);
  CGenericModifier::SetbLeftRight( true);
  // Without right and left buttons and test Change mode A and B buttons
  //KeyManag.Begin( PIN_PUSH, PIN_UP, PIN_DOWN, 0, 0, PIN_RIGHT, PIN_LEFT);
  //CGenericModifier::SetbLeftRight( false);

  // Create screen manager
  CreateGestScreen();

  // Print logo and version
  PrintLogo();

  // SD card init (remove SD card to test Error mode)
  if (!InitSD())
  {
    CModeError::SetTxtError((char *)"SD card error !");
    CGenericMode::ChangeMode( MODEERROR);
  }
  else
  {
    // Init of the callback to date the files
#ifdef MMC_SDFAT
    SdFile::dateTimeCallback(dateTime);
#endif
    // Start log and software
    LogFile::StartLog();
    if (CGenericMode::GetLanguage() == LFRENCH)
      LogFile::AddLog( LLOG, "Démarrage Test Modifiers %s, FCPU %dMHz, CPU revision %d, SDK %s", VERSION_MODIFIERS, ESP.getCpuFreqMHz(), ESP.getChipRevision(), ESP.getSdkVersion());
    else
      LogFile::AddLog( LLOG, "Starting Modifiers Test %s, FCPU %dMHz, CPU revision %d, SDK %s", VERSION_MODIFIERS, ESP.getCpuFreqMHz(), ESP.getChipRevision(), ESP.getSdkVersion());

    // Go to Test mode
    CGenericMode::ChangeMode( MODETEST);
  }
  // Initializes the treatment time
  ulTimeKey   = millis() + TIMEKEY;
  ulTimePrint = millis() + TIMEPRINT;
}

//----------------------------------------------------------------------------
// Main loop
void loop()
{
  // Test of the time since the last control of the buttons
  if (millis() > ulTimeKey and CGenericMode::GetpCurrentMode() != NULL)
  {
    // We treat a possible button
    int iOldMode = CGenericMode::GetCurrentMode();
    unsigned short key = KeyManag.GetKey();
    switch (key)
    {
    case K_UP      : Serial.println("Key Up")           ; break;
    case K_DOWN    : Serial.println("Key Down")         ; break;
    case K_PUSH    : Serial.println("Key Push")         ; break;
    case K_LEFT    : Serial.println("Key Left")         ; break;
    case K_RIGHT   : Serial.println("Key Right")        ; break;
    case K_MODEA   : Serial.println("Key change mode A"); break;
    case K_MODEB   : Serial.println("Key change mode B"); break;
    }
    if (key != K_NO)
    {
      int iNewMode = CGenericMode::GetpCurrentMode()->KeyManager( key);
      if (iNewMode != NOMODE and iNewMode != iOldMode)
        // Changing the operating mode
        CGenericMode::ChangeMode( iNewMode);
    }
    ulTimeKey = millis() + TIMEKEY;
  }
  // Test if it's time to display
  if (millis() > ulTimePrint and CGenericMode::GetpCurrentMode() != NULL)
  {
    // Print current mode
    CGenericMode::GetpCurrentMode()->PrintMode();
    ulTimePrint = millis() + TIMEPRINT;
  }
  // Attente 10ms
  delay( 100);
}
