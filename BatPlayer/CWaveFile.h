/* 
 * File:   CWaveFile.h
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "SdFat.h"

#ifndef CWAVEFILE_H
#define CWAVEFILE_H

//-------------------------------------------------------------------------
// Classe pour la création d'un fichier wave
class CWaveFile
{
  
  // Entête d'un fichier wav
  struct wavfile_header {
    char  riff_tag[4];
    int   riff_length;
    char  wave_tag[4];
    char  fmt_tag[4];
    int   fmt_length;
    short audio_format;
    short num_channels;
    int   sample_rate;
    int   byte_rate;
    short block_align;
    short bits_per_sample;
    char  data_tag[4];
    int   data_length;
  };

public:
  //-------------------------------------------------------------------------
  // Constructeur (initialisation de la classe)
  CWaveFile();

  //-------------------------------------------------------------------------
  // Destructeur (fermeture du fichier)
  virtual ~CWaveFile();

  //-------------------------------------------------------------------------
  // Ouverture du fichier en écriture
  // Returne true si OK et false en cas d'erreur
  bool OpenWaveFileForWrite(
      unsigned int iFe,     // Fréquence déchantillonnage en Hz
      const char *filepath, // Path et nom du fichier à créer
      bool bExp10 = true    // Indique que la Fe est divisée par 10 pour expansion de temps
      );
  
  //-------------------------------------------------------------------------
  // Ouverture du fichier en lecture
  // Returne true si OK et false en cas d'erreur
  bool OpenWaveFileForRead(
      const char *filepath, // Path et nom du fichier à créer
      bool bExpT            // True pour une expansion de temps de 10, false pour 1
      );
  
  //-------------------------------------------------------------------------
  // Fermeture du fichier
  void CloseWavefile();
  
  //-------------------------------------------------------------------------
  // Retourne la fréquence d'échantillonnage en Hz
  int GetSampleRate() {return header.sample_rate;};
  
  //-------------------------------------------------------------------------
  //! \brief Ecriture des échantillons
  //! Retourne le nombre total des échantillons du fichier, 0 si erreur
  //! \param data Pointeur sur le buffer des échantillons à écrire
  //! \param length Nombre d'échantillons à écrire
  unsigned long WavfileWrite(
    int16_t data[],
    unsigned int length
    );
    
  //-------------------------------------------------------------------------
  // Lecture des échantillons
  // Retourne le nombre d'octets effectivement lus (0 si fin du fichier atteinte)
  long WavefileRead(
    short *pData, // Pointeur sur les données à lire
    long length   // Longueur à lire en nombre d'échantillons
    );

  //-------------------------------------------------------------------------
  // Positionne le fichier en lecture à +/- Coef de sa durée totale
  // Retourne false si la durée du fichier est dépassée ou inférieure à 0
  bool ReadNext(
    int iNext,    // +1 pour avancer, -1 pour reculer
    int iCoef=10  // Coefficient d'avancement
    );
  
  //-------------------------------------------------------------------------
  // Positionne le fichier en lecture à un emplacement précis
  // Retourne -1 si la taille du fichier est dépassée
  // et la position en échantillons lus sinon
  long SetPosRead(
    unsigned long lPos  // Nombre d'échantillons ou se positionner
    );
  
  //-------------------------------------------------------------------------
  // Retourne la durée d'enregistrement en cours en secondes
  float GetRecordDuration();
    
  //-------------------------------------------------------------------------
  // Retourne la durée de lecture totale en secondes
  float GetPlayDuration();
    
  //-------------------------------------------------------------------------
  // Retourne la taille des données du fichier
  unsigned long GetDataLength();
    
  //-------------------------------------------------------------------------
  // Initialise le taux de décimation en lecture
  void SetDecimation(
    int iDecim  // Valeurs possibles : 1, 2 et 3
    );
    
  //-------------------------------------------------------------------------
  // Retourne true si expansion de temps x10
  bool GetExp10() {return bExpTime;};
    
private:
  //-------------------------------------------------------------------------
  // Fréquence d'échantillonnage en Hz
  float fFe;
  
  //-------------------------------------------------------------------------
  // Pointeur sur le fichier
#ifdef SDFAT_FILE_TYPE
  FsFile wavfile;
  //ExFile wavfile;
#else
  File wavfile;
#endif
  
  //-------------------------------------------------------------------------
  // Structure d'entête
  struct wavfile_header header;
  
  //-------------------------------------------------------------------------
  // Nombre d'échantillons du fichier en écriture
  unsigned long nbEch;
  
  //-------------------------------------------------------------------------
  // Temps d'un échantillon en seconde
  float tEch;
  
  //-------------------------------------------------------------------------
  // Indique une ouverture en écriture
  bool bWrite;

  //-------------------------------------------------------------------------
  // True pour une expansion de temps de 10, false pour 1
  bool bExpTime;
  
  //-------------------------------------------------------------------------
  // Indique le taut de décimation pour respecter la fréquence d'échantillonnage de sortie
  // Valeurs possible 1, 2 et 3
  int iDecimation;
};

#endif  /* CWAVEFILE_H */
