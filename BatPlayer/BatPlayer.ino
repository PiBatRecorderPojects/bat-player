/* 
 * File:   BatPlayer.ino
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * Lecteur Ultrason à base d'une carte Teensy 3.6
 */
 /*
 * ATTENTION - ATTENTION - ATTENTION - ATTENTION - ATTENTION
 * Pour un appel court à la fonction sd.vol()->freeClusterCount() (sauf le 1er appel fait dans l'init) 
 * il faut mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 dans SdFatConfig.h (librairie SdFat à télécharger sur https://github.com/greiman/SdFat)
 * 
 * Dans l'environnement ARDUINO, sélectionner "Outils" > "Type de carte Teensy 3.6" et "CPU speed : 144MHz"
*/

#include "SdFat.h"
#include "TimeLib.h"
#include <U8g2lib.h>
#include <CKeyManager.h>
#include "Const.h"
#include "CModeGen.h"

extern "C" volatile uint32_t set_arm_clock(uint32_t frequency);

// String for log messages
extern const char *txtLOWBAT[];
extern const char *txtSDERROR[];
extern const char *txtSTARTBP[];
extern const char *txtSDFATCFG[];

// Gestionnaire écran
U8G2 *pDisplay = NULL;

// Gestionnaire de carte SD spécifique Teensy 3.6
// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef MMC_SDFAT
  SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  SdFatSdioEX sd;
#endif
#ifdef SD_FAT_TYPE
#if SD_FAT_TYPE == 0
  SdFat sd;
#endif
#endif

// Gestionaire des boutons
CKeyManager KeyManag;
// Gestionnaire du fichier LogPR.txt
LogFile logPR( "BP");

// Mémorisation du temps du loop pour les traitements boutons et écran
unsigned long ulTimeKey;
unsigned long ulTimePrint;

// Bitmap du Logo
extern const unsigned char LogoGR [];

// Teensy 3.6 Serial Number
uint32_t uiSerialNumber = 0;

// Serial nuber string with recorder name
char sSerialName[MAXNAMERECORDER];

// Lecture du numéro de série suivant la discussion https://forum.pjrc.com/threads/91-teensy-3-MAC-address
uint32_t GetSerialNumber()
{
    uint32_t num;
#if defined(__MK66FX1M0__) // Teensy 3.6
    unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
    cli();                                // On stoppe les interruptions
    uint64_t num64;
    FTFL_FSTAT = FTFL_FSTAT_RDCOLERR | FTFL_FSTAT_ACCERR | FTFL_FSTAT_FPVIOL;
    *(uint32_t *)&FTFL_FCCOB3 = 0x41070000;
    FTFL_FSTAT = FTFL_FSTAT_CCIF;
    while (!(FTFL_FSTAT & FTFL_FSTAT_CCIF)) ; // wait
    num64 = *(uint64_t *)&FTFL_FCCOB7;
    SREG = sreg_backup;                   // On retaure les interruptions
    num = num64>>32;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    num = HW_OCOTP_MAC0 & 0xFFFFFF;
#endif
    return num;
}

// Gestion du temps entre deux lectures des touches
#define TIMEKEY 50  // Temps entre deux examun de l'état des touches
unsigned long uiTimeKey = 0;
// Gestion du temps entre deux affichages d'un mode
#define TIMEAFF 300 // Temps entre 2 affichages
unsigned long uiTimeAff = 0;

//----------------------------------------------------
// Fonction permettant de dater les fichiers
void dateTime(uint16_t* date, uint16_t* time)
{
  // return date using FAT_DATE macro to format fields
  *date = FAT_DATE(year(), month(), day());

  // return time using FAT_TIME macro to format fields
  *time = FAT_TIME(hour(), minute(), second());
}

//-------------------------------------------------------------------------
// Création du gestionnaire d'écran
void CreateGestScreen()
{
  // Création du gestionnaire d'écran
  switch (CModeGeneric::GetParamsC()->iScreenType)
  {
  case ST_SSD1306:
    pDisplay = new U8G2_SSD1306_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);
    break;
  default:
    pDisplay = new U8G2_SH1106_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);
  }
  // Initialisation
  pDisplay->begin();
  pDisplay->setFontPosTop();
  pDisplay->clearBuffer();
  pDisplay->sendBuffer();
}

//----------------------------------------------------
// Affichage du logo de départ
void AffLogo()
{
  pDisplay->clearBuffer();
  pDisplay->drawXBM(0, 0, 128, 64, LogoGR);
  pDisplay->setFont(u8g2_font_6x13B_mf);  // Hauteur 9
  pDisplay->setCursor(3,25);
  pDisplay->println("Bat");
  pDisplay->setCursor(3,37);
  pDisplay->println("Player");
  pDisplay->setFont(u8g2_font_micro_mr);  // Hauteur 5
  pDisplay->setCursor( 3, 2);
  switch (CModeGeneric::GetParamsC()->iScreenType)
  {
  case ST_SSD1306: pDisplay->print( "SSD1306"); break;
  default        : pDisplay->print( "SH1106") ; break;
  }
  // Print CPU speed
  char temp[20];
  sprintf(temp, "CPU %dMHz", F_CPU/1000000);
  pDisplay->setCursor( 3, 9);
  pDisplay->print( temp);
  // If problem, print SDFat
  if (MAINTAIN_FREE_CLUSTER_COUNT != 1)
  {
    pDisplay->setCursor( 3, 16);
    pDisplay->print( "SDFat cluster");
  }
  else
  {
    // Print serial number
    sprintf(temp, "%lu", uiSerialNumber);
    pDisplay->setCursor( 3, 16);
    pDisplay->print( temp);
  }
  // Print version
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  pDisplay->setCursor( 3, 50);
  pDisplay->print( VERSION);
  pDisplay->drawRFrame( 0, 0, 128, 64, 0);
  if (KeyManag.GetKey() == K_LEFT)
    delay(30000);
  // Print CPU type
  pDisplay->setFont(u8g2_font_micro_mr);  // Hauteur 5
  pDisplay->setCursor( 70, 2);
#if defined(__IMXRT1062__) // Teensy 4.1
  pDisplay->print( "T4.1");
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  pDisplay->print( "T3.6");
#endif
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  pDisplay->sendBuffer();
  //while (true);
}

//-------------------------------------------------------------------------
// Initialisation de la carte SD
bool InitSD()
{
  bool bReturn = false;
#ifdef MMC_SDFAT
  Serial.println("sd.begin() type MMC_SDFAT");
  if (!sd.begin())
#endif
#ifdef MMC_SDFAT_EXT
  Serial.println("sd.begin() type MMC_SDFAT_EXT");
  if (!sd.begin())
#endif
#ifdef SD_FAT_TYPE
  Serial.println("sd.begin() type SD_FAT_TYPE");
  if (!sd.begin(SdioConfig(FIFO_SDIO)))
#endif
    LogFile::AddLog( LLOG, txtSDERROR[CModeGeneric::GetParamsC()->iLanguage]);
  else
  {
    //Serial.printf("SD_FAT_TYPE %d\n", SD_FAT_TYPE);
    sd.chvol();
    bReturn = true;
  }
  return bReturn;
}

//-------------------------------------------------------------------------
// Récupération de l'heure de la RTC intégrée au Teensy 3.0
time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}

//----------------------------------------------------------------------------
// Setup du programme
void setup()
{
  // Lecture du numéro de série de la carte processeur (5 tentatives)
  uiSerialNumber = GetSerialNumber();
  // Initialise l'heure de la libairie Time avec la RTC du Teensy 3.0
  setSyncProvider(getTeensy3Time);
  // Initialisation de la liaison série de debug
  Serial.begin(115200);
  /*while(!Serial);
  delay(5000);*/
  //LogFile::bConsole = true;
  delay(100);
  Serial.println("===== Bat Player =====");

#if defined(__IMXRT1062__) // Teensy 4.1
  // Init de la fréquence CPU à 180MHz
  set_arm_clock(180000000);
#endif

  // Init sortie LED
  pinMode( 13, OUTPUT);
  digitalWrite( 13, LOW);
  // Initialisation du nom du lecteur en fonction de son numéro de série
  sprintf( sSerialName, "BP%lu", uiSerialNumber);
  logPR.SetLogFileName( (char *)sSerialName);
  // Init de la broche d'option
  pinMode(OPT_B0, INPUT_PULLUP);
  // Test de la version du PCB
  int iB0 = digitalRead(OPT_B0);
  // Initialisation du gestionnaire des boutons
  if (iB0 == HIGH)
    // PCB V0.1
    KeyManag.Begin( PINPUSH,  PINUP, PINDOWN, PINRIGHT, PINLEFT);
  else
    // PCB V0.2
    KeyManag.Begin( PINPUSHB, PINUP, PINDOWN, PINRIGHT, PINLEFT);
  // Init de la sortie Mute et on coupe l'ampli
  pinMode( BIT_MUTE, OUTPUT);
  digitalWrite( BIT_MUTE, LOW);

  // Initialisation de la carte SD (à faire avant la lecture des paramètres)
  bool bSD = InitSD();
  // Initialisation aux paramètres par défaut
  CModeGeneric::SetDefault();
  // Lecture des paramètres avec choix du type d'écran
  CModeGeneric::StaticReadParams();
  //Serial.printf("Après StaticReadParams répertoire [%s] fichier [%s], language %d\n", CModeGeneric::GetParamsC()->sCurrentDir, CModeGeneric::GetParamsC()->sWaveName, CModeGeneric::GetParamsC()->iLanguage);
  delay(500);
  
  // Lecture clavier pour éventuel mode debug et changement d'écran
  int key = KeyManag.GetKey();
  // Test si mode debug
  if (key == K_UP)
  {
    Serial.println("Teensy Recorder - Mode Debug");
    CModeGeneric::bDebug = true;
    logPR.SetbConsole( true);
    digitalWrite( 13, HIGH);
    delay(1000);
    digitalWrite( 13, LOW);
  }
  logPR.SetbConsole( true);
  
  // Test si changement de type d'écran
  char sScreen[30];
  sScreen[0] = 0;
  if (key == K_DOWN)
  {
    // On signale la détection du changement d'écran
    for (int i=0; i<3; i++)
    {
      digitalWrite( 13, HIGH);
      delay(300);
      digitalWrite( 13, LOW);
      delay(300);
    }
    // Changement du type d'écran
    switch (CModeGeneric::GetParamsC()->iScreenType)
    {
    case ST_SSD1306: CModeGeneric::GetParamsC()->iScreenType = ST_SH1106; break;
    case ST_SH1106 :
    default        : CModeGeneric::GetParamsC()->iScreenType = ST_SSD1306;
    }
    // Sauvegarde des paramètres y compris le nouveau choix d'écran
    CModeGeneric::StaticWriteParams();
  }
  // Init de la chaine affichée sur le logo
  switch (CModeGeneric::GetParamsC()->iScreenType)
  {
  case ST_SSD1306: strcpy( sScreen, "SSD1306"); break;
  case ST_SH1106 :
  default        : strcpy( sScreen, "SH1106" );
  }
  // Création du gestionnaire d'écran
  CreateGestScreen();

  // Si demandé, on baisse la luminosité de l'écran
  if (CModeGeneric::GetParamsC()->bLumiere)
    pDisplay->setContrast(1);
  else
    pDisplay->setContrast(255);

  // Mémo du temps pour le delai d'affichage du logo
  unsigned long uiTimeLogoA = millis();
  // Affichage du logo et de la version
  AffLogo();
  //delay(30000);

  // Initialisation du mode de fin d'erreur
  CModeError::SetNewMode( MPARAMS);

  if (!bSD)
  {
    // Mesure des batteries
    if (!CModeGeneric::CheckBatteries())
      // A partir de 10%, il est fortement conseillé de charger sa batterie, on impose ce message d'erreur
      CModeError::SetTxtError((char *)txtLOWBAT[CModeGeneric::GetParamsC()->iLanguage]);
    else
      CModeError::SetTxtError((char *)txtSDERROR[CModeGeneric::GetParamsC()->iLanguage]);
    // Passage en mode erreur
    CModeGeneric::ChangeMode( MERROR);
  }
  else
  {
    // Init de la callback pour dater les fichiers
    SdFile::dateTimeCallback(dateTime);
    LogFile::StartLog();
    // Mémo du démarrage dans le log
#if defined(__MK66FX1M0__) // Teensy 3.6
    LogFile::AddLog( LLOG, txtSTARTBP[CModeGeneric::GetParamsC()->iLanguage], "T3.6", uiSerialNumber, VERSION, F_CPU);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    LogFile::AddLog( LLOG, txtSTARTBP[CModeGeneric::GetParamsC()->iLanguage], "T4.1", uiSerialNumber, VERSION, F_CPU);
#endif
    if (MAINTAIN_FREE_CLUSTER_COUNT != 1)
      LogFile::AddLog( LLOG, txtSDFATCFG[CModeGeneric::GetParamsC()->iLanguage]);
    // Appel au premier freeClusterCount qui prend du temps avec des cartes >16GO (4.5s sur une 32GO),
    // ensuite, si MAINTAIN_FREE_CLUSTER_COUNT est à 1 dans SdFatConfig.h, l'appel ne prend pas trop de temps
    //unsigned long iTime1 = millis();
    sd.vol()->freeClusterCount();

    // Initialise le répertoire et fichier de départ après lecture des paramètres et après initialisation de la carte SD
    bool bOK = CModeGeneric::InitDirFile();
    // Delay d'affichage du logo
    unsigned long uiTimeLogoB = millis();
    if ((uiTimeLogoB - uiTimeLogoA) < 2000)
      delay(2000 - (uiTimeLogoB - uiTimeLogoA));

    //CModeGeneric::ChangeMode( MMODIFA);
    /*CModeGeneric::GetParamsC()->iMode = IDXMODEPARAMS;
    CModeGeneric::ChangeMode( MPARAMS);*/
    /*CModeGeneric::GetParamsC()->iMode = IDXMODEPARAMS;
    CModeGeneric::ChangeMode( MAUTO);*/
    // Initialise le mode de départ
    if (bOK)
    {
      // Le fichier existe, on passe en lecture
      CModeGeneric::GetParamsC()->iMode = IDXMODEPLAY;
      // Vérification du mode
      if (!CModeGeneric::GetParamsC()->bModeAuto)
        // Lecteur manuel
        CModeGeneric::ChangeMode( MPLAY);
      else
        // Lecteur auto
        CModeGeneric::ChangeMode( MAUTOP);
    }
    else
    {
      // Le fichier n'existe pas, on passe en mode sélection du fichier
      CModeGeneric::GetParamsC()->iMode = IDXMODEFILES;
      CModeGeneric::ChangeMode( MFILES);
    }
  }
  // Initialise le temps des traitements
  uiTimeKey = millis() + TIMEKEY;
  uiTimeAff = millis() + TIMEAFF;
}

//----------------------------------------------------------------------------
// Boucle principale
void loop()
{
  // Traitement des tâches de fond du mode courant
  CModeGeneric::GetpCurrentMode()->OnLoop();
  unsigned long uiMillis = millis();
  if (uiMillis > uiTimeKey)
  {
    //Serial.println("loop GetKey");
    // Traitement des boutons par le mode courant
    unsigned short key = KeyManag.GetKey();
    /*if (CModeGeneric::bDebug)
    {
      switch (key)
      {
      case K_UP      : Serial.println("Key Up")           ; break;
      case K_DOWN    : Serial.println("Key Down")         ; break;
      case K_PUSH    : Serial.println("Key Push")         ; break;
      case K_LEFT    : Serial.println("Key Left")         ; break;
      case K_RIGHT   : Serial.println("Key Right")        ; break;
      case K_MODEA   : Serial.println("Key change mode A"); break;
      case K_MODEB   : Serial.println("Key change mode B"); break;
      }
    }*/
    int iNewMode = CGenericMode::GetpCurrentMode()->KeyManager( key);
    /*if (iNewMode != NOMODE)
      Serial.printf("loop iNewMode %d, CurrentMode %d\n", iNewMode, CGenericMode::GetCurrentMode());*/
    if (iNewMode != NOMODE and iNewMode != CGenericMode::GetCurrentMode())
      // Changing the operating mode
      CGenericMode::ChangeMode( iNewMode);
    uiTimeKey = uiMillis + TIMEKEY;
  }
  if (uiMillis > uiTimeAff)
  {
    // Affichage du mode courant
    //Serial.println("loop PrintMode");
    CModeGeneric::GetpCurrentMode()->PrintMode();
    uiTimeAff = uiMillis + TIMEAFF;
  }
}
