/* 
 * File:   CModeParFile.cpp
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CModeParams
  * - CModeFiles
  */
#include <U8g2lib.h>
#include <EEPROM.h>
#include "Const.h"
#include "SdFat.h"
#include "CModeGen.h"
#include "CModeParFiles.h"

// Gestionnaire écran
extern U8G2 *pDisplay;

// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef SD_FAT_TYPE
  extern SdFat sd;
#endif
#ifdef MMC_SDFAT
  extern SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  extern SdFatSdioEX sd;
#endif

const char formIDXDATE[]   =  "       &$/&$/&$ ";
const char formIDXHEURE[]  =  "       &$:&$:&$ ";
const char formIDXBAT[]    =  " &$ ";
const char formIDXEXT[]    =  " &$ ";
const char formIDXSD[]     =  "                   & ";
const char formIDXLANG[]   =  "           &$$$$$$$$$";
const char formIDXGain[]   =  " Default gain: &$$$$$";
const char formIDXCOPYR[]  =  " &$$ ";
const char formIDXNEWBPS[] =  " &$$ ";
const char *txtStr[]       = {"%s",
                              "%s",
                              "%s",
                              "%s"};
const char formIDXAUTO[]   =  " &$$ ";
const char formIDXFLT[]    =  "                &$$";
const char formIDXSELSC[]  =  " &$";
const char *txtIDXAUTO[]   = {" %s",
                              " %s",
                              " %s",
                              " %s"};
extern const char *txtManuAuto[MAXLANGUAGE][2];
extern const char *txtIDXDATE[];
extern const char *txtIDXHEURE[];
extern const char *txtIDXBAT[];
extern const char *txtIDXEXT[];
extern const char *txtIDXSD[];
extern const char *txtIDXLANG[];
extern const char *txtIDXCOPYR[];
extern const char *txtLanguage[MAXLANGUAGE][MAXLANGUAGE];
extern const char *txtNoFile[];
extern const char *txtIDXFILT[];
extern const char *txtFILT[MAXLANGUAGE][2];
extern const char *txtIDXSELSC[];
extern const char *txtIDXGESTSC[];
extern const char *txtIDXDEFG[];
extern const char *txtIDXNEWBPS[];
extern const char *txtAttenuation[MAXLANGUAGE][11];

/*-----------------------------------------------------------------------------
 Classe de gestion du mode paramètres
 Affiche les différents paramètres et permets de les modifier
-----------------------------------------------------------------------------*/
//-------------------------------------------------------------------------
// Constructeur
CModeParams::CModeParams(): CModeGeneric()
{
  // Creation of parameter modifier lines
  // IDXMANU      Manual or auto read mode                                                Mode manuel
  LstModifParams.push_back(new CBoolModifier ( formIDXAUTO, true, true, txtIDXAUTO, &paramsC.bModeAuto, (const char **)txtManuAuto, 0, 0));
  // IDXSELSC     Selecting the scenario file in auto mode
  LstModifParams.push_back(new CPushButtonModifier( formIDXSELSC, true, txtIDXSELSC, 0, 0));
  // IDXGESTSC    Editing the selected scenario file
  LstModifParams.push_back(new CPushButtonModifier( formIDXSELSC, true, txtIDXGESTSC, 0, 0));
  // IDXNEWBPS    Addition of a new scenario file (automatic name ScenarioA.bps, ScenarioB.bps ...)
  LstModifParams.push_back(new CPushButtonModifier( formIDXNEWBPS, true, txtIDXNEWBPS, 0, 0));
  // IDXDATE      Update of the current date
  LstModifParams.push_back(new CDateModifier ( formIDXDATE , true, txtIDXDATE, 0, 0));
  // IDXHEURE     Update the current time
  LstModifParams.push_back(new CHourModifier( formIDXHEURE, true, txtIDXHEURE, NULL, 0, 0));
  // IDXBAT       Internal battery charge level
  LstModifParams.push_back(new CPushButtonModifier( formIDXBAT, true, txtIDXBAT, 0, 0));
  // IDXEXT       External battery charge level
  LstModifParams.push_back(new CPushButtonModifier ( formIDXEXT, true, txtIDXEXT, 0, 0));
  // IDXSD        SD card occupancy
  LstModifParams.push_back(new CSDModifier   ( formIDXSD, true, txtIDXSD, ".wav", 0, 0));
  // IDXPFLT      Play with filter or not
  LstModifParams.push_back(new CBoolModifier ( formIDXFLT, true, true, txtIDXFILT, &paramsC.bFiltre, (const char **)txtFILT, 0, 0));
  // IDXDEFG      Default gain
  LstModifParams.push_back(new CEnumModifier ( formIDXGain, true, false, txtIDXDEFG, (int *)&paramsC.iDefGain, MAXDB, (const char **)txtAttenuation, 0, 0));
  // IDXLANG      Language
  LstModifParams.push_back(new CEnumModifier ( formIDXLANG, true, false, txtIDXLANG, (int *)&paramsC.iLanguage, MAXLANGUAGE, (const char **)txtLanguage, 0, 0));
  // IDXCRIGHT    Copyright menu
  LstModifParams.push_back(new CPushButtonModifier ( formIDXCOPYR , true, txtIDXCOPYR, 0, 0));
}

//-------------------------------------------------------------------------
// Début du mode
void CModeParams::BeginMode()
{
  // Init du fichier du scénario
  SetBoutonScenario();
  //Serial.printf("CModeParams::BeginMode sScenarioName %s, sSCFile %s\n", paramsC.sScenarioName, sSCFile);
  // Cohérence des paramètres
  Coherenceparams();
  // Nombre de lignes visible à l'écran
  iNbLines = 6;
  // Appel de la méthode parente
  CModeGeneric::BeginMode();
  // Mesure de l'état des batteries
  CheckBatteries();
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeParams::EndMode()
{
  // Appel de la méthode parente
  CModeGeneric::EndMode();
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModeParams::OnEndChange(
  int idxModifier
  )
{
  // Cohérence entre les paramètres
  Coherenceparams();
  iNewMode = NOMODE;
  // Selon le paramètre modifié
  switch (idxModifier)
  {
  case IDXBATP:
    CheckBatteries();
    break;
  case IDXEXTP:
    CheckBatteries();
    break;
  case IDXLANG:
    // Init de la langue des modificateurs
    CGenericModifier::SetLanguage( paramsC.iLanguage);
    break;
  case IDXCRIGHT:
    // Goto Copyright mode
    iNewMode = MCRIGHT;
    break;
  case IDXSELSC:
    // Switching to scenario file selection mode
    iNewMode = MBPSSEL;
    break;
  case IDXGESTSC:
    // Switching to scenario file modification mode
    iNewMode = MMODIFA;
    break;
  case IDXNEWBPS:
    // Automatic addition of a new scenario file
    AddNewBPS();
    break;
  default:
    // Appel de la fonction de base
    iNewMode = CModeGeneric::OnEndChange( idxModifier);
  }
  // Mémorisation des paramètres
  WriteParams();
  return iNewMode;
}

//-------------------------------------------------------------------------
//! \brief Initialisation du fichier du scénario
void CModeParams::SetBoutonScenario()
{
  // Initialisation du fichier scénario sélectionné
  if (strlen(paramsC.sScenarioName) == 0)
    sprintf( sSCFile, txtIDXSELSC[paramsC.iLanguage], "??");
  else if (strlen(paramsC.sScenarioName) <= 15)
    // Nom de fichier pas trop long
    sprintf( sSCFile, txtIDXSELSC[paramsC.iLanguage], paramsC.sScenarioName);
  else
  {
    // Préparation du nom du fichier
    char sFile[25];
    String str = PrepareStrFile( paramsC.sCurrentDir, 15);
    str.toCharArray( sFile, 25);
    sprintf( sSCFile, txtIDXSELSC[paramsC.iLanguage], sFile);
  }
  ((CPushButtonModifier *)LstModifParams[IDXSELSC])->SetString( sSCFile);
}
  
//-------------------------------------------------------------------------
//! \brief Cohérence entre les différents paramètres
void CModeParams::Coherenceparams()
{
  // Si en mode auto
  if (paramsC.bModeAuto)
  {
    // Les paramètres de gestion des scénarios sont autorisés
    LstModifParams[IDXSELSC ]->SetbValid( true);
    LstModifParams[IDXGESTSC]->SetbValid( true);
    LstModifParams[IDXNEWBPS]->SetbValid( true);
  }
  else
  {
    // Les paramètres de gestion des scénarios ne sont pas autorisés
    LstModifParams[IDXSELSC ]->SetbValid(false);
    LstModifParams[IDXGESTSC]->SetbValid(false);
    LstModifParams[IDXNEWBPS]->SetbValid(false);
  }
}
  
//-------------------------------------------------------------------------
// Mesure de la batterie
void CModeParams::CheckBatteries()
{
  char temp[80];
  // Mesure des batteries
  CModeGeneric::CheckBatteries();
  // Init du paramètre batterie interne
  if (fNivBatLiPo < 2.9)
    sprintf( FormatBatInt, txtIDXBAT[paramsC.iLanguage], "--");
  else
  {
    sprintf( temp, "%d%% %4.1lfV", iNivBatLiPo, fNivBatLiPo);
    sprintf( FormatBatInt, txtIDXBAT[paramsC.iLanguage], temp);
  }
  ((CPushButtonModifier *)LstModifParams[IDXBATP])->SetString( FormatBatInt);
  // Init du paramètre batterie externe
  if (fNivBatExt < 10.0)
    sprintf( FormatBatExt, txtIDXEXT[paramsC.iLanguage], "--");
  else
  {
    sprintf( temp, "%d%% %4.1lfV", iNivBatExt, fNivBatExt);
    sprintf( FormatBatExt, txtIDXEXT[paramsC.iLanguage], temp);
  }
  ((CPushButtonModifier *)LstModifParams[IDXEXTP])->SetString( FormatBatExt);
}

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Files
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
//! \param bBPSFiles false, select wav file, true select bps files
CModeFiles::CModeFiles(
  bool bBPS
  ) : CModeGeneric()
{
  bBPSFiles = bBPS;
  // Création des 6 lignes d'affichage de la liste des fichiers wav présents
  for (int i=0; i<MAXLINESWAVE; i++)
    LstModifParams.push_back(new CPushButtonModifier( " &$$$$$$$$$$$$$$$$$$$", true, txtStr, 0, 0));
}

//-------------------------------------------------------------------------
// Destructeur
CModeFiles::~CModeFiles()
{
  
}

//-------------------------------------------------------------------------
// Début du mode
void CModeFiles::BeginMode()
{
  // Appel de la classe de base
  CModeGeneric::BeginMode();
  // Par défaut, la 1ère ligne est montrée et sélectionnée
  idxPremier  = 0;
  idxSelFile  = 0;
  if (bBPSFiles)
    // Lecture des fichiers bps de la racine uniquement
    ListFiles( NULL);
  else
    // Lecture des fichiers wav et répertoires du répertoire courant
    ListFiles( paramsC.sCurrentDir);
  // Initialisation de la liste des fichiers
  InitLstAffFiles();
  iFirstVisibleIdxLine = 3;
  iLastVisibleIdxLine  = 8;
  // Par défaut, pas de sélection d'un nouveau fichier
  bNewSelFile = false;
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeFiles::EndMode()
{
  // Appel de la classe de base
  CModeGeneric::EndMode();
  // Désélection du fichier
  LstModifParams[idxSelParam]->SetbSelect( false);
  // Mémorisation des paramètres
  WriteParams();
  //LogFile::AddLog( LLOG, "Arrêt du mode Sélection Fichier");
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModeFiles::OnEndChange(
  int idxModifier
  )
{
  iNewMode = NOMODE;
  if (idxModifier >= IDXFILE1 and idxModifier <= IDXFILE6)
  {
    // Sélection d'un fichier ou d'un répertoire
    // Récupération du nom du fichier sélectionné
    char sTemp[80];
    lstWaveFiles[idxPremier+idxModifier-IDXFILE1].toCharArray( sTemp, 80);
    //Serial.printf("Fichier en modif=%s\n", sTemp);
    // Test si c'est le répertoire du dessus
    if (sTemp[0] == '/' and sTemp[1] == '.' and sTemp[2] == '.')
    {
      // On remonte d'un répertoire, recherche du dernier /
      char *p = strrchr( paramsC.sCurrentDir, '/');
      if (p != NULL)
      {
        *p = 0;
        if (paramsC.sCurrentDir[0] == 0)
          strcpy( paramsC.sCurrentDir, "/");
        ListFiles( paramsC.sCurrentDir);
        idxPremier  = 0;
        idxSelFile  = 0;
        // Initialisation de la liste des fichiers
        InitLstAffFiles();
        // Selection du 1ère fichier
        LstModifParams[idxModifier]->SetbSelect( false);
        idxSelParam = IDXFILE1;
        LstModifParams[IDXFILE1]->SetbSelect( true);
      }
      else
        Serial.println("Erreur pour remonter d'un répertoire !!!");
    }
    // Test si c'est un répertoire
    else if (sTemp[0] == '/')
    {
      //Serial.printf("Sélection du répertoire paramsC.sCurrentDir %s, sTemp %s\n", paramsC.sCurrentDir, sTemp);
      // Oui, on ajoute ce répertoire au chemin complet et on l'explore
      if (strlen(paramsC.sCurrentDir) == 1)
        strcpy( paramsC.sCurrentDir, sTemp);
      else
        strcat( paramsC.sCurrentDir, sTemp);
      //Serial.printf("paramsC.sCurrentDir %s\n", paramsC.sCurrentDir);
      ListFiles( paramsC.sCurrentDir);
      idxPremier  = 0;
      idxSelFile  = 0;
      // Initialisation de la liste des fichiers
      InitLstAffFiles();
      // Selection du 1ère fichier
      LstModifParams[idxModifier]->SetbSelect( false);
      idxSelParam = IDXFILE1;
      LstModifParams[idxSelParam]->SetbSelect( true);
    }
    else
    {
      // Non, c'est un fichier,
      if (!bBPSFiles) 
      {
        // Mode wave, on le passe en lecture
        paramsC.iSelect = idxPremier+idxSelParam-IDXFILE1;
        lstWaveFiles[paramsC.iSelect].toCharArray( paramsC.sWaveName, 255);
        iNewMode = MPLAY;
        bNewSelFile = true;
        Serial.printf("Sélection répertoire [%s] fichier [%s], passage en lecture\n", paramsC.sCurrentDir, paramsC.sWaveName);
      }
      else
      {
        // Mode scénario, sélection du fichier et retour en mode paramètres
        strcpy( paramsC.sScenarioName, sTemp);
        iNewMode = MPARAMS;
        Serial.printf("Sélection scénario [%s]\n", paramsC.sScenarioName);
      }
    }
  }
  else
    // Appel de la fonction de base
    iNewMode = CModeGeneric::OnEndChange( idxModifier);

  if (iNewMode != NOMODE)
    Serial.printf("CModeFiles::OnEndChange = %d\n", iNewMode);
  return iNewMode;
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Si la touche est une touche de changement de mode, retourne le mode demandé
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes de traiter les actions opérateurs
int CModeFiles::KeyManager(
  unsigned short key  // Touche sollicitée par l'opérateur
  )
{
  // Test si Down et dernier fichier sélectionné
  if (key == K_DOWN and idxSelParam == IDXFILE6 and idxPremier < (int)lstWaveFiles.size()-MAXLINESWAVE)
  {
    // Il faut scroller vers le bas la liste des fichiers
    idxPremier++;
    idxSelFile++;
    // Initialisation de la liste des fichiers
    InitLstAffFiles();
  }
  // Test si Down et 1er fichier sélectionné
  else if (key == K_UP and idxSelParam == IDXFILE1 and idxPremier > 0)
  {
    Serial.println("CModeFiles::KeyManager K_UP");
    // Il faut scroller la liste des fichiers
    idxPremier--;
    idxSelFile--;
    // Initialisation de la liste des fichiers
    InitLstAffFiles();
  }
  else if (key != K_NO)
  {
    // Appel de la classe de base
    iNewMode = CModeGeneric::KeyManager( key);
  }
  return iNewMode;
}

//-------------------------------------------------------------------------
// Initialise la liste des fichiers affichés en fonction du 1er sélectionné
void CModeFiles::InitLstAffFiles()
{
  //Serial.println("CModeFiles::InitLstAffFiles A");
  if (lstWaveFiles.size() == 0)
  {
    strcpy( LstFilesScreen[0], txtNoFile[paramsC.iLanguage]);
    ((CPushButtonModifier *)LstModifParams[IDXFILE1])->SetString( (char *)LstFilesScreen[0]);
    for (int i=0; i<MAXLINESWAVE; i++)
    {
      // Pas de fichier, la ligne n'est pas modifiable
      LstModifParams[IDXFILE1+i]->SetEditable( false);
      if (i>0)
      {
        strcpy( LstFilesScreen[i], " ");
        ((CPushButtonModifier *)LstModifParams[IDXFILE1+i])->SetString( (char *)LstFilesScreen[i]);
      }
    }
  }
  else
  {
    for (int i=0; i<MAXLINESWAVE; i++)
    {
      //Serial.printf("CModeFiles::InitLstAffFiles for (%d) idxPremier %d\n", i, idxPremier);
      // Préparation du premier caractère
      strcpy( LstFilesScreen[i], " ");
      //LstModifParams[IDXFILE1+i]->SetEditable( true);
      if (i+idxPremier < (int)lstWaveFiles.size())
      {
        // On récupère les 14 lettres les plus significatives
        String str = PrepareStrFile( lstWaveFiles[i+idxPremier]);
        char sTemp[MAXCHARLINE+1];
        str.toCharArray( sTemp, MAXCHARLINE);
        strcat( LstFilesScreen[i], sTemp);
        ((CPushButtonModifier *)LstModifParams[IDXFILE1+i])->SetString( (char *)LstFilesScreen[i]);
        // Il y a un fichier, la ligne est modifiable
        LstModifParams[IDXFILE1+i]->SetEditable( true);
      }
      else
      {
        //                          12345678901234567890
        strcat( LstFilesScreen[i], "                    ");
        ((CPushButtonModifier *)LstModifParams[IDXFILE1+i])->SetString( (char *)LstFilesScreen[i]);
        // Pas de fichier, la ligne n'est pas modifiable
        LstModifParams[IDXFILE1+i]->SetEditable( false);
      }
    }
  }
  /*Serial.printf("CModeFiles::InitLstAffFiles iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine);
  Serial.println("CModeFiles::InitLstAffFiles");
  for (int i=0; i<MAXLINESWAVE; i++)
  {
    Serial.print("[");
    Serial.print(LstFilesScreen[i]);
    Serial.println("]");
  }*/
}
