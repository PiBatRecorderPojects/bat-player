/* 
 * File:   CModeParFiles.h
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CModeParams
  * - CModeFiles
  */

#include "CModeGen.h"

#ifndef CMODPARFILES_H
#define CMODPARFILES_H

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Paramètres
-----------------------------------------------------------------------------*/
class CModeParams : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  // Constructeur
  CModeParams();

  //-------------------------------------------------------------------------
  // Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  // Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief Cohérence entre les différents paramètres
  void Coherenceparams();
  
  //-------------------------------------------------------------------------
  //! \brief Initialisation du fichier du scénaio
  void SetBoutonScenario();
  
  //-------------------------------------------------------------------------
  //! \brief Mesure de la batterie
  void CheckBatteries();
  
protected:
  //! Format d'affichage du paramètre de visualisation de la batterie interne
  char FormatBatInt[MAX_LINEPARAM+10];
  //! Format d'affichage du paramètre de visualisation de la batterie externe
  char FormatBatExt[MAX_LINEPARAM+10];
  //! Fichier scénario sélectionné
  char sSCFile[25];
};

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Files
-----------------------------------------------------------------------------*/
class CModeFiles : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  // Constructeur
  //! \param bBPS Files false, select wav file, true select bps files
  CModeFiles(
    bool bBPS=false
    );

  //-------------------------------------------------------------------------
  // Destructeur
  virtual ~CModeFiles();
  
  //-------------------------------------------------------------------------
  // Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  // Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );
  
  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  // Initialise la liste des fichiers affichés en fonction du 1er sélectionné
  void InitLstAffFiles();
    
protected:
  // Indice du premier fichier montré
  int idxPremier;
  
  // Indice du fichier sélectionné
  int idxSelFile;
  
  // Liste des n fichiers montrés à l'écran
  char LstFilesScreen[MAXLINESWAVE][MAXCHARLINE+1];

  // Chaine de préparation des noms des fichiers et repertoires
  char sFiles[160];

  // Indique si on recherche des fichiers wav (false) ou bps (true)
  bool bBPSFiles;
};

#endif // CMODPARFILES_H
