/* 
 * File:   CModifAction.h
   BatPlayer Copyright (c) 2020 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <U8g2lib.h>
#include <SdFat.h>
#include "CModifAction.h"

// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef SD_FAT_TYPE
  extern SdFat sd;
#endif
#ifdef MMC_SDFAT
  extern SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  extern SdFatSdioEX sd;
#endif

// Gestionnaire écran
extern U8G2 *pDisplay;

//                             123456789012345678901
const char *formIDXMARETURN = " &$$$$$ ";
const char *formIDXMAPLUS   = " & ";
const char *formIDXMAMOINS  = " & ";
const char *formIDXMADEL    = "[Del  &]";
const char *formIDXMAINSERT = "[&$$$$$$$$]";
const char *formIDXMALINE   = "Line  &$$";
const char *formIDXMANBL    = "/&$$$";
const char *formIDXMAHOUR   = "           &$:&$:&$ ";
const char *formIDXMATYPE   = "       &$$$$$$$$ ";
const char *formIDXMADIR    = " &$$$$$$$$$$$$$$$$$$ ";
const char *formIDXMAFILE   = " &$$$$$$$$$$$$$$$$$$ ";
const char *formIDXMAVAL    = "[&$$$$]";
const char *txtIDXMAPLUS[]  = {"[+]", "[+]", "[+]", "[+]"};
const char *txtIDXMAMOINS[] = {"[-]", "[-]", "[-]", "[-]"};
const char *txtIDXMANBL[]   = {"/%03d",
                               "/%03d",
                               "/%03d",
                               "/%03d"};
const char *txtIDXMADIR[]   = {"[%s]",
                               "[%s]",
                               "[%s]",
                               "[%s]"};
extern const char *txtIDXMARETURN[];
extern const char *txtIDXMADEL[];
extern const char *txtIDXMAINSERT[];
extern const char *txtIDXMALINE[];
extern const char *txtIDXMATYPE[];
extern const char *txtIDXMADUR[];
extern const char *txtIDXMAHOUR[];
extern const char *txtActions[MAXLANGUAGE][4];
extern const char *txtYN[MAXLANGUAGE][2];
extern const char *txtVALID[MAXLANGUAGE][2];

/*-----------------------------------------------------------------------------
 Classe permettant la modification d'une action d'un scénario
-----------------------------------------------------------------------------*/
//    123456789012345678901    123456789012345678901 123456789012345678901
// 00 [Return] [+] [-]      00 [Return] [+] [-]      [Return] [+] [-]
// 01 [Supp N] [Ins. after] 09 [Supp N] [Ins. after] [Supp N] [Ins. after]
// 02 Line  001/999 [Valid] 18 Line  001/999 [Valid] Line  001/999 [Inval]
// 03 [Type: Play file]     27 [Type: Play dir ]     [Type: Wake up  ]
// 04 [Duration 00:00:00]   36 [Duration 00:00:00]   [Time     00:00:00]
// 05 [/toto/test         ] 45 [/toto/test         ]
// 06 [barba.wav          ] 54
//    000000000000000001111
//    001123344566778990012
//    062840628406284062840

//-------------------------------------------------------------------------
//! Constructeur
CModifAction::CModifAction():CModeGeneric( false)
{
  // IDXMARETURN Retour dans le mode précédent
  LstModifParams.push_back(new CPushButtonModifier( formIDXMARETURN, false, txtIDXMARETURN, 0, 0));
  // IDXMANEXT   Plus
  LstModifParams.push_back(new CPushButtonModifier( formIDXMAPLUS,   false, txtIDXMAPLUS, 54, 0));
  // IDXMAPREV  Moins
  LstModifParams.push_back(new CPushButtonModifier( formIDXMAMOINS,  false, txtIDXMAMOINS, 84, 0));
  // IDXMADEL    Del
  LstModifParams.push_back(new CBoolModifier      ( formIDXMADEL,    false, false, txtIDXMADEL, &bDel, (const char **)txtYN, 0, 9));
  // IDXMAINSERT Insert
  LstModifParams.push_back(new CPushButtonModifier( formIDXMAINSERT, false, txtIDXMAINSERT, 48, 9));
  // IDXMALINE   Ligne number
  LstModifParams.push_back(new CModificatorInt    ( formIDXMALINE,   false, txtIDXMALINE, &iLine,  1, 999, 1, 0, 18));
  // IDXMANBLINE Number of lines
  LstModifParams.push_back(new CModificatorInt    ( formIDXMANBL,    false, txtIDXMANBL, &iNbLine, 1, 999, 1, 54, 18));
  // IDXMAVALID  Valid line or not (comment)
  LstModifParams.push_back(new CBoolModifier      ( formIDXMAVAL,    false, true, txtIDXMADIR, &action.bComment, (const char **)txtVALID, 84, 18));
  // IDXMATYPE   Type de l'action
  LstModifParams.push_back(new CEnumModifier      ( formIDXMATYPE,   false, false, txtIDXMATYPE, &action.iAction, SC_ERROR, (const char **)txtActions, 0, 27));
  // IDXMADUR    Duration of action
  LstModifParams.push_back(new CHourModifier      ( formIDXMAHOUR,   false, txtIDXMADUR, action.sTime, 0, 36));
  // IDXMATIME   Time of action
  LstModifParams.push_back(new CHourModifier      ( formIDXMAHOUR,   false, txtIDXMAHOUR, sTime, 0, 36));
  // IDXMADIR    Répertoire de l'action
  LstModifParams.push_back(new CModificatorDir    ( formIDXMADIR,    false, txtIDXMADIR, action.sDirPath, 0, 45));
  // IDXMAFILE   Nom du fichier wav de l'action
  LstModifParams.push_back(new CModificatorFile   ( formIDXMAFILE,   false, txtIDXMADIR, action.sDirPath, action.sWavFile, 0, 54));
  LstModifParams[IDXBAT     ]->SetbValid( false);
  LstModifParams[IDXLUM     ]->SetbValid( false);
  LstModifParams[IDXMODE    ]->SetbValid( false);
  LstModifParams[IDXMALINE  ]->SetEditable(false);
  LstModifParams[IDXMANBLINE]->SetEditable(false);
}

//-------------------------------------------------------------------------
//! Destructeur
CModifAction::~CModifAction()
{
  // Reset the list of actions
  lstScenario.clear();
}

//-------------------------------------------------------------------------
//! Début du mode
void CModifAction::BeginMode()
{
  /*char sLine[80];
  lstScenario.clear();
  for (int i=0; i<4; i++)
  {
    lstScenario.push_back( ActionScenario());
    lstScenario[i].iAction = ACTWAIT;
    sprintf( lstScenario[i].sTime, "00:00:%02d", i);
    lstScenario[i].sDirPath[0] = 0;
    lstScenario[i].sWavFile[0] = 0;
  }
  for (int i=0; i<(int)lstScenario.size(); i++)
  {
      sprintf( sLine, "Line %d:", i);
      PrintAction( sLine, &(lstScenario[i]));
  }
  lstScenario.erase(lstScenario.begin()+2);
  Serial.println( "erase 2");
  for (int i=0; i<(int)lstScenario.size(); i++)
  {
      sprintf( sLine, "Line %d:", i);
      PrintAction( sLine, &(lstScenario[i]));
  }
  Serial.println( "insert 2");
  lstScenario.insert(lstScenario.begin()+2, ActionScenario());
  for (int i=0; i<(int)lstScenario.size(); i++)
  {
      sprintf( sLine, "Line %d:", i);
      PrintAction( sLine, &(lstScenario[i]));
  }
  lstScenario.clear();*/
  // Appel de la méthode parente
  CModeGeneric::BeginMode();
  // Lecture du scénario
  if (!ReadScenarioFile(paramsC.sScenarioName) and lstScenario.size() > 0)
  {
    // Add new BPS file
    AddNewBPS();
    // Initialisation aux valeurs par défaut
    action.iAction = ACTWAIT;
    strcpy( action.sTime, "00:00:00");
    action.sDirPath[0] = 0;
    action.sWavFile[0] = 0;
    lstScenario.clear();
  }
  iNbLine = lstScenario.size();
  if (iNbLine == 0)
    iLine = 0;
  else
  {
    // Select first action
    iLine = 1;
    SetAction( 0);
  }
  if (iNbLine > 1)
  {
    if (idxSelParam >= 0)
      LstModifParams[idxSelParam]->SetbSelect( false);
    idxSelParam = IDXMANEXT;
    LstModifParams[idxSelParam]->SetbSelect( true);
  }
  /*char sLine[20];
  for (int i=0; i<(int)lstScenario.size(); i++)
  {
    sprintf( sLine, "Scenario Line %d:", i);
    PrintAction( sLine, &(lstScenario[i]));
  }*/
  // Cohérence des paramètres
  Coherenceparams();
}

//-------------------------------------------------------------------------
//! Fin du mode
void CModifAction::EndMode()
{
  
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModifAction::OnEndChange(
  int idxModifier
  )
{
  //ActionScenario tmpAction;
  switch(idxModifier)
  {
  case IDXMARETURN:  // Return to params mode
    // Save current action
    if (iLine > 0 and SaveAction( iLine-1))
    {
      if (strlen(paramsC.sScenarioName) < 5 or strstr(paramsC.sScenarioName, ".bps") == NULL)
        strcpy( paramsC.sScenarioName, "Scenario.bps");
      // Save actions list
      WriteScenarioFile(paramsC.sScenarioName);
      iNewMode = MPARAMS;
    }
    break;
  case IDXMANEXT  :  // Next action
    // Save current action
    if (iLine > 0 and SaveAction( iLine-1))
    {
      if (iLine < (int)lstScenario.size())
      {
        iLine++;
        // Init new current action
        SetAction( iLine-1);
      }
    }
    break;
  case IDXMAPREV  :  // Previous action
    // Save current action
    if (iLine != 0 and SaveAction( iLine-1))
    {
      if (iLine > 1)
      {
        iLine--;
        // Init new current action
        SetAction( iLine-1);
      }
    }
    break;
  case IDXMADEL   :  // Del
    if (bDel)
    {
      lstScenario.erase(lstScenario.begin()+iLine-1);
      iNbLine = lstScenario.size();
      if (iLine > iNbLine)
        iLine = iNbLine;
      if (iNbLine > 0)
        SetAction( iLine-1);
      bDel = false;
    }
    break;
  case IDXMAINSERT:  // Insert after
    // Save current action
    if (iLine != 0 and SaveAction( iLine-1))
    {
      if (iNbLine <= 1 or iLine == (int)lstScenario.size())
        lstScenario.push_back( ActionScenario());
      else
        lstScenario.insert(lstScenario.begin()+iLine, ActionScenario());
      iNbLine = (int)lstScenario.size();
      lstScenario[iLine].iAction = ACTWAIT;
      strcpy( lstScenario[iLine].sTime, "00:00:00");
      strcpy( lstScenario[iLine].sDirPath, "");
      strcpy( lstScenario[iLine].sWavFile, "");
      iLine++;
      SetAction( iLine-1);
    }
    else if (lstScenario.size() == 0)
    {
      lstScenario.push_back( ActionScenario());
      iNbLine = (int)lstScenario.size();
      lstScenario[iLine].iAction = ACTWAIT;
      strcpy( lstScenario[iLine].sTime, "00:00:00");
      strcpy( lstScenario[iLine].sDirPath, "");
      strcpy( lstScenario[iLine].sWavFile, "");
      iLine++;
      SetAction( iLine-1);
    }
    break;
  case IDXMATYPE  :  // Type of action
    LstModifParams[IDXMADIR]->SetParam();
    ((CModificatorFile *)LstModifParams[IDXMAFILE])->SetFile( action.sDirPath, NULL);
    LstModifParams[IDXMAFILE]->SetParam();
    break;
  case IDXMAVALID :  // Valid line or not (comment)
    break;
  case IDXMADUR   :  // Duration of action
    break;
  case IDXMATIME  :  // Time of action
    break;
  case IDXMADIR   :  // Action dir
    LstModifParams[IDXMADIR]->SetParam();
    ((CModificatorFile *)LstModifParams[IDXMAFILE])->SetFile( action.sDirPath, NULL);
    LstModifParams[IDXMAFILE]->SetParam();
    break;
  case IDXMAFILE  :  // Action wav file
    LstModifParams[IDXMAFILE]->SetParam();
    break;
  }
  /*PrintAction((char *)"Fin OnEndChange", &action);
  Serial.printf("iLine %d\n", iLine);
  for (int i=0; i<(int)lstScenario.size(); i++)
  {
    char temp[50];
    sprintf(temp, "lstScenario[%d]", i);
    PrintAction(temp, &lstScenario[i]);
  }*/
  Coherenceparams();
  return iNewMode;
}

//-------------------------------------------------------------------------
//! \brief Cohérence entre les différents paramètres
void CModifAction::Coherenceparams()
{
  LstModifParams[IDXMARETURN]->SetbValid( true);
  if (lstScenario.size() == 0)
  {
    LstModifParams[IDXMANEXT  ]->SetbValid( false);
    LstModifParams[IDXMAPREV  ]->SetbValid( false);
    LstModifParams[IDXMADEL   ]->SetbValid( false);
    LstModifParams[IDXMATYPE  ]->SetbValid( false);
    LstModifParams[IDXMADUR   ]->SetbValid( false);
    LstModifParams[IDXMATIME  ]->SetbValid( false);
    LstModifParams[IDXMADIR   ]->SetbValid( false);
    LstModifParams[IDXMAFILE  ]->SetbValid( false);
  }
  else
  {
    LstModifParams[IDXMATYPE  ]->SetbValid( true);
    LstModifParams[IDXMADEL   ]->SetbValid( true);
    if (iLine == 1)
      LstModifParams[IDXMAPREV ]->SetbValid( false);
    else
      LstModifParams[IDXMAPREV ]->SetbValid( true);
    if (lstScenario.size() == 1 or iLine >= (int)lstScenario.size())
      LstModifParams[IDXMANEXT ]->SetbValid( false);
    else
      LstModifParams[IDXMANEXT ]->SetbValid( true);
    switch (action.iAction)
    {
    case PLAYFILE:
      LstModifParams[IDXMADIR ]->SetbValid( true);
      LstModifParams[IDXMAFILE]->SetbValid( true);
      LstModifParams[IDXMADUR ]->SetbValid( true);
      LstModifParams[IDXMATIME]->SetbValid( false);
      break;
    case PLAYDIR:
      LstModifParams[IDXMADIR ]->SetbValid( true);
      LstModifParams[IDXMAFILE]->SetbValid( false);
      LstModifParams[IDXMADUR ]->SetbValid( true);
      LstModifParams[IDXMATIME]->SetbValid( false);
      break;
    case ACTWAIT:
      LstModifParams[IDXMADIR ]->SetbValid( false);
      LstModifParams[IDXMAFILE]->SetbValid( false);
      LstModifParams[IDXMADUR ]->SetbValid( true);
      LstModifParams[IDXMATIME]->SetbValid( false);
      break;
    case WAKEUP:
      LstModifParams[IDXMADIR ]->SetbValid( false);
      LstModifParams[IDXMAFILE]->SetbValid( false);
      LstModifParams[IDXMADUR ]->SetbValid( false);
      LstModifParams[IDXMATIME]->SetbValid( true);
      break;
    default:
      LstModifParams[IDXMADIR ]->SetbValid( false);
      LstModifParams[IDXMAFILE]->SetbValid( false);
      LstModifParams[IDXMADUR ]->SetbValid( false);
      LstModifParams[IDXMATIME]->SetbValid( false);
    }
    if (!LstModifParams[idxSelParam]->GetbValid())
    {
      if (LstModifParams[IDXMANEXT  ]->GetbValid())
      {
        idxSelParam = IDXMANEXT;
        LstModifParams[IDXMANEXT]->SetbSelect( true);
      }
      else if (LstModifParams[IDXMAPREV  ]->GetbValid())
      {
        idxSelParam = IDXMAPREV;
        LstModifParams[IDXMAPREV]->SetbSelect( true);
      }
      else
      {
        idxSelParam = IDXMARETURN;
        LstModifParams[IDXMARETURN]->SetbSelect( true);
      }
    }
    if (action.bComment)
    {
      LstModifParams[IDXMATYPE  ]->SetEditable( false);
      LstModifParams[IDXMADUR   ]->SetEditable( false);
      LstModifParams[IDXMATIME  ]->SetEditable( false);
      LstModifParams[IDXMADIR   ]->SetEditable( false);
      LstModifParams[IDXMAFILE  ]->SetEditable( false);
    }
    else
    {
      LstModifParams[IDXMATYPE  ]->SetEditable( true);
      LstModifParams[IDXMADUR   ]->SetEditable( true);
      LstModifParams[IDXMATIME  ]->SetEditable( true);
      LstModifParams[IDXMADIR   ]->SetEditable( true);
      LstModifParams[IDXMAFILE  ]->SetEditable( true);
    }
  }
}

//-------------------------------------------------------------------------
//! \brief Save the current action in the list
//! \param idxAct Index of affected action
bool CModifAction::SaveAction(
  int idxAct
  )
{
  // Verify action
  bool bOK = VerifyAction( &action);
  if (bOK)
  {
    lstScenario[idxAct].iAction = action.iAction;
    lstScenario[idxAct].bComment = action.bComment;
    if (action.iAction == WAKEUP)
      strcpy( lstScenario[idxAct].sTime, sTime);
    else
      strcpy( lstScenario[idxAct].sTime, action.sTime);
    if (action.iAction == PLAYDIR or action.iAction == PLAYFILE)
      strcpy( lstScenario[idxAct].sDirPath, action.sDirPath);
    else
      lstScenario[idxAct].sDirPath[0] = 0;
    if (action.iAction == PLAYFILE)
      strcpy( lstScenario[idxAct].sWavFile, action.sWavFile);
    else
      lstScenario[idxAct].sWavFile[0] = 0;
    char temp[128];
    sprintf(temp, "SaveAction action:");
    PrintAction(temp, &action);
    sprintf(temp, "SaveAction lstScenario[%d]:", idxAct);
    PrintAction(temp, &lstScenario[idxAct]);
    /*for (int i=0; i<lstScenario.size(); i++)
    {
      sprintf(temp, "SaveAction lstScenario[%d]", i);
      PrintAction(temp, &lstScenario[i]);
    }*/
  }
  return bOK;
}
  
//-------------------------------------------------------------------------
//! \brief Initializes the current action from the list
//! \param idxAct Index of affected action
void CModifAction::SetAction(
  int idxAct
  )
{
  /*char temp[50];
  sprintf(temp, "CModifAction::SetAction %d iAction %d", idxAct, action.iAction);
  PrintAction(temp, &lstScenario[idxAct]);*/
  action.iAction = lstScenario[idxAct].iAction;
  action.bComment = lstScenario[idxAct].bComment;
  strcpy( sTime, lstScenario[idxAct].sTime);
  strcpy( action.sTime, lstScenario[idxAct].sTime);
  if (action.iAction <= PLAYDIR)
  {
    strcpy( action.sDirPath, lstScenario[idxAct].sDirPath);
    if (strlen(action.sDirPath) == 0)
      strcpy(action.sDirPath, "/");
    ((CModificatorDir *)LstModifParams[IDXMADIR])->SetDir( action.sDirPath);
  }
  else
    action.sDirPath[0] = 0;
  if (action.iAction == PLAYFILE)
  {
    strcpy( action.sWavFile, lstScenario[idxAct].sWavFile);
    ((CModificatorFile *)LstModifParams[IDXMAFILE])->SetFile( action.sDirPath, action.sWavFile);
  }
  else
    action.sWavFile[0] = 0;
  /*sprintf(temp, "CModifAction::SetAction %d", idxAct);
  PrintAction(temp, &action);*/
}

//-------------------------------------------------------------------------
//! \class CModificatorDir
//! \brief Management class of directory selected type modifier

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation des paramètres aux valeurs par défaut)
//! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
//! \param bLine true to indicate a change on a line, false otherwise
//! \param pForm Parameter format like " Min Freq %04dkHz"
//! \param pDir Pointer on the dir to modify
//! \param ix X coordinate of the top left point of the modifier
//! \param iy Y coordinate of the top left point of the modifier
CModificatorDir::CModificatorDir(
  const char *pIndicModif,
  bool bLine,             
  const char **pForm,     
  char *pDir,              
  int ix,                 
  int iy                  
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
  //Serial.printf("CModificatorDir::CModificatorDir %d\n", this);
  pParam = pDir;
  // Search all dir
  SearchDir((char *)"/");
  // Set current dir
  SetDir( pDir);
  idxDir = 0;
}

//-------------------------------------------------------------------------
//! \brief Key Event Processing
void CModificatorDir::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  bool bTraite = false;
  if (bModif)
  {
    switch (iKey)
    {
    case K_UP:
      // Parameter increment
      idxDir++;
      if (idxDir >= (int)lstDir.size())
        idxDir=0;
      bTraite = true;
      break;
    case K_DOWN:
      // Parameter decrement
      idxDir--;
      if (idxDir < 0)
        idxDir = (int)lstDir.size() - 1;
      bTraite = true;
      break;
    }
  }
  if (!bTraite)
    // Call of the basic method
    CGenericModifier::ReceiveKey( iKey);
  else
  {
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
//! \brief Returns a parameter string
//! The return chain looks like " Min Freq 120kHz"
char *CModificatorDir::GetString()
{
  char sDirPath[256];
  strcpy(sDirPath, lstDir[idxDir].c_str());
  char *p = sDirPath;
  if (strlen(sDirPath) > 19)
    p = &sDirPath[strlen(sDirPath) - 19];
  else if (strlen(sDirPath) < 19)
  {
    while (strlen(sDirPath) < 19)
      strcat(sDirPath, " ");
  }
  sprintf( lineParam, pFormat[iLanguage], p);
  //Serial.printf("CModificatorDir::GetString=%s\n", lineParam);
  return lineParam;
}

//-------------------------------------------------------------------------
//! \brief Updating the parameter with the displayed string
void CModificatorDir::SetParam()
{
  if (idxDir >= 0)
    strcpy( pParam, lstDir[idxDir].c_str());
  else
    strcpy( pParam, " ");
}

//-------------------------------------------------------------------------
//! \brief Recherche récursive des répertoires
//! \param *pStartDir Start dir (/TEST/SUB for exemple)
void CModificatorDir::SearchDir(
  char *pStartDir
  )
{
  if (pStartDir == NULL)
  {
    //Serial.println("CModificatorDir::SearchDir pStartDir = NULL !!");
    return;
  }
  else if (strlen(pStartDir) == 0)
    strcpy( pStartDir, "/");
  //Serial.printf("CModificatorDir::SearchDir [%s]\n", pStartDir);
  char name[80];
  char CompleteDir[256];
  bool bOK = true;
#ifdef SD_FAT_TYPE
  FsFile file;
  FsFile root;
#else
  SdFile file;
#endif
  // Ajout de répertoire dans la liste
  lstDir.push_back( pStartDir);
  strcpy( CompleteDir, pStartDir);
  //Serial.printf("CModificatorDir::SearchDir Ajout [%s]\n", pStartDir);
  // On sélectionne le répertoire
#ifdef SD_FAT_TYPE
  bOK = root.open( pStartDir);
#else
  bOK = sd.chdir( pStartDir, true),
#endif
  if (!bOK)
  {
    Serial.printf("CModificatorDir::SearchDir impossible to open [%s]\n", pStartDir);
    return;
  }
  // On recherche les répertoires
#ifndef SD_FAT_TYPE
  sd.vwd()->rewind(); 
#endif
#ifdef SD_FAT_TYPE
  while (file.openNext(&root, O_RDONLY))
#else
  while (file.openNext(sd.vwd(), O_READ))
#endif
  {
    // Récupération du nom du fichier
    file.getName( name, 80);
    //Serial.println(name);
    // On ne prend pas en compte le répertoire "System Volume Information"
    if (strcmp( name, "System Volume Information") != 0)
    {
      if (file.isDir())
      {
        // C'est un répertoire, exploration récursive
        if (pStartDir[strlen(pStartDir)-1] == '/')
          sprintf( CompleteDir, "%s%s", pStartDir, name);
        else
          sprintf( CompleteDir, "%s/%s", pStartDir, name);
        SearchDir( CompleteDir);
      }
    }
    file.close();
  }
#ifdef SD_FAT_TYPE
  root.close(); 
#endif
}

//-------------------------------------------------------------------------
//! \brief Set curent directory
//! \param *pDir  Full path (/TEST/SUB for example)
void CModificatorDir::SetDir(
  char *pDir
  )
{
  //Serial.printf("CModificatorDir::SetDir(%s) %d\n", pDir, this);
  // Initialization of the variable
  if (pParam != pDir)
    strcpy(pParam, pDir);
  // Search the dir in the list
  idxDir = -1;
  for (int i=0; i < (int)lstDir.size(); i++)
  {
    if (strcmp(lstDir[i].c_str(), pParam) == 0)
    {
      idxDir = i;
      break;
    }
  }
  if (idxDir < 0)
    idxDir = 0;
  GetString();
}

//-------------------------------------------------------------------------
//! \class CModificatorFile
//! \brief Management class of file selected type modifier

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation des paramètres aux valeurs par défaut)
//! \param pIndicModif Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
//! \param bLine true to indicate a change on a line, false otherwise
//! \param pForm Parameter format like " Min Freq %04dkHz"
//! \param pFile Pointer on the file name to modify
//! \param ix X coordinate of the top left point of the modifier
//! \param iy Y coordinate of the top left point of the modifier
CModificatorFile::CModificatorFile(
  const char *pIndicModif,
  bool bLine,             
  const char **pForm, 
  char *pDir,    
  char *pFile,              
  int ix,                 
  int iy                  
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
  // Initialization of the variables
  pParam = pFile;
  // Initialisation of the file list
  SetFile( pDir, pFile);
  if (lstFiles.size() > 0)
    idxFile = 0;
  else
    idxFile = -1;
}

//-------------------------------------------------------------------------
//! \brief Key Event Processing
void CModificatorFile::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  bool bTraite = false;
  if (bModif)
  {
    switch (iKey)
    {
    case K_UP:
      // Parameter increment
      idxFile++;
      if (idxFile >= (int)lstFiles.size())
        idxFile = 0;
      bTraite = true;
      break;
    case K_DOWN:
      // Parameter decrement
      idxFile--;
      if (idxFile < 0)
        idxFile = (int)lstFiles.size() - 1;
      bTraite = true;
      break;
    }
  }
  if (!bTraite)
    // Call of the basic method
    CGenericModifier::ReceiveKey( iKey);
  else
  {
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
//! \brief Returns a parameter string
//! The return chain looks like " Min Freq 120kHz"
char *CModificatorFile::GetString()
{
  char sCurrentFile[80];
  if (idxFile >= 0)
    strcpy(sCurrentFile, lstFiles[idxFile].c_str());
  else
    strcpy(sCurrentFile, " ");
  char *p = sCurrentFile;
  if (strlen(sCurrentFile) > 19)
    p = &sCurrentFile[strlen(sCurrentFile) - 19];
  else if (strlen(sCurrentFile) < 19)
  {
    while (strlen(sCurrentFile) < 19)
      strcat(sCurrentFile, " ");
  }
  sprintf( lineParam, pFormat[iLanguage], p);
  //Serial.printf("CModificatorFile::GetString=%s\n", lineParam);
  return lineParam;
}

//-------------------------------------------------------------------------
//! \brief Updating the parameter with the displayed string
void CModificatorFile::SetParam()
{
  if (idxFile >= 0)
    strcpy( pParam, lstFiles[idxFile].c_str());
  else
    strcpy( pParam, " ");
}

//-------------------------------------------------------------------------
//! \brief Recherche des fichiers dans un répertoire
//! \param *pDir     Chemin complet (/TEST/SUB par exemple)
//! \param *pFilter  Filtre de recherche des fichiers (*.wav par exemple, *.* pour tous)
void CModificatorFile::SearchFiles(
  char *pDir,
  char *pFilter
  )
{
  //Serial.printf("CModificatorFile::SearchFiles %s, %s\n", pDir, pFilter);
  char name[80];
  char temp[80];
  char filter[12];
#ifdef SD_FAT_TYPE
  FsFile file;
  FsFile root;
#else
  SdFile file;
#endif
  // Préparation du filtre en minuscules
  strcpy( filter, &pFilter[1]);
  strlwr( filter);
  // On sélectionne le répertoire
#ifdef SD_FAT_TYPE
  root.open(pDir);
#else
  sd.chdir( pDir);
#endif
  // On recherche les fichiers
#ifndef SD_FAT_TYPE
  sd.vwd()->rewind(); 
#endif
#ifdef SD_FAT_TYPE
  while (file.openNext(&root, O_RDONLY))
#else
  while (file.openNext(sd.vwd(), O_READ))
#endif
  {
    // Récupération du nom du fichier
    file.getName( name, 80);
    //Serial.println(name);
    // On ne prend pas en compte le répertoire "System Volume Information"
    if (strcmp( name, "System Volume Information") != 0)
    {
      // Passage en minuscules
      strcpy( temp, name);
      strlwr( temp);
      // Test si c'est un fichier recherché
      if (!file.isDir() and (strstr(temp, filter) != NULL or strstr(filter, "*.*") != NULL))
      {
        // C'est un fichier recherché, mémorisation
        lstFiles.push_back( name);
        //Serial.printf("CModificatorFile::SearchFiles add %s\n", name);
      }
    }
    file.close();
  }
  if (lstFiles.size() > 0)
    idxFile = 0;
  else
    idxFile = -1;
}

//-------------------------------------------------------------------------
//! \brief Set curent directory and file
//! \param *pDir  Full path (/TEST/SUB for example)
//! \param *pFile File name
void CModificatorFile::SetFile(
  char *pDir,
  char *pFile
  )
{
  //Serial.printf("CModificatorFile::SetFile( %s, %s)\n", pDir, pFile);
  lstFiles.clear();
  idxFile = -1;
  SearchFiles( pDir, (char *)"*.wav");
  // Search the file in the list
  for (int i=0; i < (int)lstFiles.size(); i++)
  {
    if (strcmp(lstFiles[i].c_str(), pParam) == 0)
    {
      idxFile = i;
      break;
    }
  }
  if (idxFile < 0 and lstFiles.size() > 0)
    idxFile = 0;
  GetString();
}
