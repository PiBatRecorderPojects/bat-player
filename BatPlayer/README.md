BatPlayer
=
This directory exposes the BatPlayer software code.
It uses the Arduino IDE (<https://www.arduino.cc/en/main/software>) along with the TeensyDuino extension (<https://www.pjrc.com/teensy/teensyduino.html>) for Teensy cards.

**It mainly uses the following libraries:**

- **SDFat** <https://github.com/greiman/SdFat>
- **U8G2Lib** <https://github.com/olikraus/u8g2>
- **ModesModifiers** internal library in ModesModifiers directory 

Version V0.30 - Version for Teensy 3.6 and 4.1 - 15/02/2024
-
- Using Teensyduino 1.59.0
- Using Arduino IDE 2.3.1
- On selection of a file, in reading mode, automatic positioning of the cursor on Reading
- Auto mode, gain reading correction (Thanks Lionel)
- Teensy 4.1 card management
- Battery voltage, MCP3221 use and check every 10s
- Saturation threshold at 4095 on T3.6 and +/-8191 on T4.1
- Fixed saving gain file of a wav file

Version V0.2 - 20/02/2021
-
- Use of the ModesModiers library
- Management of wav files including with the uppercase extension
- Adding a Copyright menu
- Addition of "ConstChar.c" for multilingual management
- Addition of German and Dutch languages
- Set U8G2 V2.28.10
- Set SDFAT V2.0.4 for working with SDXC in FAT64 (exFAT). Formatting to FAT32 is no longer necessary
- Added automatic play of scenarios

Version V0.11 - 01/06/2020
-
- Management of several types of screens (SSD1306 et SH1106)
- Buffer of 4096 so as not to exceed 5ms of IT processing in CReader.cpp, otherwise, some screens do not like

Version V0.10 - Initial version of the BatPlayer
-
