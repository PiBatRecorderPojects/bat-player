/* 
 * File: CModeAuto.cpp
   BatPlayer Copyright (c) 2020 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <U8g2lib.h>
#include <EEPROM.h>
#include <Snooze.h>
#include "TimeLib.h"
#include "SdFat.h"
#include "Const.h"
#include "CModeAuto.h"

// Gestionnaire écran
extern U8G2 *pDisplay;

// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef SD_FAT_TYPE
  extern SdFat sd;
#endif
#ifdef MMC_SDFAT
  extern SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  extern SdFatSdioEX sd;
#endif

// Pour la récup de l'heure RTC au réveil
extern time_t getTeensy3Time();

extern const char *txtIDXSCWSTBY[];

// Chaînes pour l'atténuation
const char *txtStrP_SC[]       = {"%s",
                                  "%s",
                                  "%s",
                                  "%s"};
const char *txtStrP_SCB[]      = {"[%s]",
                                  "[%s]",
                                  "[%s]",
                                  "[%s]"};
const char *txtIntB_SC[]       = {"%03d s",
                                  "%03d s",
                                  "%03d s",
                                  "%03d s"};
const char *txtFEB[]           = {"SF%03dkHz",
                                  "FE%03dkHz",
                                  "SF%03dkHz",
                                  "SF%03dkHz"};
const char formIDXSCPARAMS[]   =  " &$$ ";
const char *txtIDXSCPARAMS[]   = {"[%s]",
                                  "[%s]",
                                  "[%s]",
                                  "[%s]"};
const char *txtLineSC[]        = {"%04d",
                                  "%04d",
                                  "%04d",
                                  "%04d"};
const char *txtNbLineSC[]      = {"/%04d",
                                  "/%04d",
                                  "/%04d",
                                  "/%04d"};
extern const char *txtSCPARAMS[MAXLANGUAGE][2];
extern const char *txtWaitSCH[];
extern const char *txtWakeupSCH[];
extern const char *txtAttenuation[MAXLANGUAGE][11];
extern const char *txtClicToWakeup[];
extern const char *txtLogPlayFile[];
extern const char *txtLogErPlayFile[];
extern const char *txtLogPlayDir[];
extern const char *txtLogErPlayDir[];
extern const char *txtLogWait[];
extern const char *txtLogWakeup[];
extern const char *txtLOGERDIR[];
extern const char *txtLOGERFILE[];
extern const char *txtMODESPLAY[];
extern const char *txtErrorPlayFile[];
extern const char *txtErrorPlayDir[];
extern const char *txtLogOpenBPS[];
extern const char *txtLOGSATUR[];
extern const char *txtLOGWEAK[];
extern const char *txtLOGSTOPRO[];

// Drivers du mode veille
SnoozeDigital digital;
SnoozeAlarm   alarm;
// Initialisation de gestionnaire de veille
SnoozeBlock configTeensy( digital, alarm);

/*-----------------------------------------------------------------------------
 Classe de gestion du mode CModeAuto
-----------------------------------------------------------------------------*/

/*  Auto mode screens
 *   Play file               Play dir                Wait                    Standby
 *   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901
 *  1[->Params. ] BAT LUM   1[->Params. ] BAT LUM   1[->Params. ] BAT LUM   1[->Params. ] BAT LUM
 *  2 ||  ================  2 ||  ================  2                       2                    
 *  30085s 000 s     011 s  30072s 000 s     011 s  3                       3                   
 *  4Play file FE384kHz     4 Loop File FE384kHz    4Waiting 52s            4Waiting 22:00:00    
 *  5GrandRhino.wav         5 GrandRhino.wav        5Standby in 15s         5Standby in 15s                    
 *  6/Exemples/Rhino        6 /Exemples/Rhino       6Clic to wakeup         6Clic to wakeup
 *  7+30dB    0001/0010     7 +30dB    0001/0010    7         0001/0010     7         0001/0010
 */

//-------------------------------------------------------------------------
// Constructeur
CModeAuto::CModeAuto() : CModeGeneric()
{
  //Serial.println("Constructeur CModeAuto");
  paramsP.iEtatPlay  = SPAUSE;
  paramsP.iPosPlay   = 0;
  paramsP.iDurPlay   = 100;
  paramsP.iPosPlayPc = 10;
  paramsP.iFe        = 384;
  paramsC.iModePlay  = PLAYONE;
  iDuration = 0;
  strcpy( sStrWakeup, " ");
  strcpy( sStrWait, " ");
  bParams  = false;
  iLine    = -1;
  iLinePlus= 0;
  iNbLine  = 0;
  iStatus  = SC_ERROR;
  strcpy( sStrInfoClic, txtClicToWakeup[paramsC.iLanguage]);
  // Initialisation des paramètres
  // Création des modificateurs du mode Play dans l'ordre de l'énuméré IDXPARAMSAUTO
  // IDXSCPLAY    Start / Stop reading
  LstModifParams.push_back(new CPlayRecModifier( &(paramsP.iEtatPlay), true, false, 24, 9, 0, 10));
  // IDXSCBARRE   Reading progress bar
  LstModifParams.push_back(new CDrawBarModifier( &(paramsP.iPosPlayPc), 98, 8, 28, 11));
  // IDXSCTMAX    Length of wav file
  LstModifParams.push_back(new CModificatorInt( "&$$  ", false, txtIntB_SC, &(paramsP.iDurPlay), 0, 999, 1, 97, 19));
  // IDXSCTPOS    Playback time position
  LstModifParams.push_back(new CModificatorInt( "&$$  ", false, txtIntB_SC, &(paramsP.iPosPlay), 0, 999, 1, 28, 19));
  // IDXSCFILE    Name of the wav file         12345678901234567890
  LstModifParams.push_back(new CCharModifier( "&$$$$$$$$$$$$$$$$$$$", false, txtStrP_SC, sCurrentFile, 16, false, false, false, false, false, 0, 37));
  // IDXSCFE      Current file sampling frequency
  LstModifParams.push_back(new CModificatorInt( "&$$", false, txtFEB, &(paramsP.iFe), 0, 999, 1, 66, 28));
  // IDXSCMPLAY   File playback mode
  LstModifParams.push_back(new CEnumModifier( " &$$$$$$$ ", false, true, txtStrP_SC, &(paramsC.iModePlay), PLAYMAX, (const char **)txtMODESPLAY, 0, 28));
  // IDXSCDIR     Current directory name
  LstModifParams.push_back(new CCharModifier( "&$$", false, txtStrP_SC, sCurrentDir, 16, false, false, false, false, false, 0, 46));
  // IDXSCATTEN   Attenuation of the selected file
  LstModifParams.push_back(new CEnumModifier ( " &$$$$ ", false, false, txtStrP_SC, &(paramsP.iOptimisation), MAXDB, (const char **)txtAttenuation, 0, 55));
  // IDXSCPARAMS  Button to go back to parameters mode (instead of the standard button to go directly to parameters)
  LstModifParams.push_back(new CBoolModifier ( formIDXSCPARAMS, false, true, txtIDXSCPARAMS, &bParams, (const char **)txtSCPARAMS, 0, 0));
  // IDXSCLINE    Line number in execution
  LstModifParams.push_back(new CModificatorInt( " &$$", false, txtLineSC, &iLinePlus, 0, 999, 1, 60, 55));
  // IDXSNBL      Number of lines to run
  LstModifParams.push_back(new CModificatorInt( " &$$", false, txtNbLineSC, &iNbLine, 0, 999, 1, 84, 55));
  // IDXSCDUR     Waiting time (seconds) for the next play action
  LstModifParams.push_back(new CModificatorInt( " &$$", false, txtLineSC, &iDuration, 0, 999, 1, 0, 19));
  // IDXSCWAIT    Waiting time (seconds) for wait action
  LstModifParams.push_back(new CCharModifier( "&$$", false, txtStrP_SC, sStrWait, 16, false, false, false, false, false, 0, 28));
  // IDXSCWAKEUP  Wakeup time for wakeup action
  LstModifParams.push_back(new CCharModifier( "&$$", false, txtStrP_SC, sStrWakeup, 16, false, false, false, false, false, 0, 28));
  // IDXSCWSTBY   Time before standby
  LstModifParams.push_back(new CCharModifier( "&$$", false, txtStrP_SC, sStrWaitStandby, 16, false, false, false, false, false, 0, 37));
  // IDXSCCLIC    Info clic
  LstModifParams.push_back(new CCharModifier( "&$$", false, txtStrP_SC, sStrInfoClic, 16, false, false, false, false, false, 0, 46));
  // IDXSCERROR   Eventuelle erreur
  LstModifParams.push_back(new CCharModifier( "&$$", false, txtStrP_SC, sStrError, 16, false, false, false, false, false, 0, 37));
  // Paramètre du mode invalide, il est remplacé par IDXSCPARAMS qui permet de passer directement en mode paramètre
  LstModifParams[IDXMODE    ]->SetEditable( false);
  LstModifParams[IDXMODE    ]->SetbValid( false);
  // Paramètres non modifiable
  LstModifParams[IDXSCPLAY  ]->SetEditable( false);
  LstModifParams[IDXSCBARRE ]->SetEditable( false);
  LstModifParams[IDXSCTMAX  ]->SetEditable( false);
  LstModifParams[IDXSCTPOS  ]->SetEditable( false);
  LstModifParams[IDXSCFILE  ]->SetEditable( false);
  LstModifParams[IDXSCFE    ]->SetEditable( false);
  LstModifParams[IDXSCMPLAY ]->SetEditable( false);
  LstModifParams[IDXSCDIR   ]->SetEditable( false);
  LstModifParams[IDXSCATTEN ]->SetEditable( false);
  LstModifParams[IDXSCLINE  ]->SetEditable( false);
  LstModifParams[IDXSNBL    ]->SetEditable( false);
  LstModifParams[IDXSCDUR   ]->SetEditable( false);
  LstModifParams[IDXSCWAIT  ]->SetEditable( false);
  LstModifParams[IDXSCWAKEUP]->SetEditable( false);
  LstModifParams[IDXSCWSTBY ]->SetEditable( false);
  LstModifParams[IDXSCCLIC  ]->SetEditable( false);
  LstModifParams[IDXSCERROR ]->SetEditable( false);
  // On sélectionne par défaut le paramètre de changement de mode
  LstModifParams[idxSelParam]->SetbSelect( false);
  idxSelParam = IDXSCPARAMS;
  LstModifParams[IDXSCPARAMS]->SetbSelect( true);
  //Serial.println("Constructeur CModeAuto OK");
}

//-------------------------------------------------------------------------
// Destructeur
CModeAuto::~CModeAuto()
{
}

//-------------------------------------------------------------------------
// Début du mode
void CModeAuto::BeginMode()
{
  //Serial.printf("CModeAuto::BeginMode\n");
  LogFile::AddLog( LLOG, txtLogOpenBPS[paramsC.iLanguage], paramsC.sScenarioName);
  iCurrentSeconde = second();
  iIndicePlay = paramsC.iSelect;
  // Appel de la classe de base
  CModeGeneric::BeginMode();
  // Lecture du scénario
  if (ReadScenarioFile(paramsC.sScenarioName) and lstScenario.size() > 0)
  {
    iNbLine = (int)lstScenario.size();
    // Select first action
    iLine = -1;
    //Serial.println("CModeAuto::BeginMode NextAction");
    NextAction();
  }
  // Initialisation filtre
  Reader.SetFiltre( paramsC.bFiltre);
  // Cohérence des paramètres en fonction du mode
  Coherenceparams();
  //Serial.printf("CModeAuto::BeginMode OK\n");
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeAuto::EndMode()
{
  //Serial.println("CModeAuto::EndMode");
  if (Reader.IsReading())
    StopRead();
  // Appel de la classe de base
  CModeGeneric::EndMode();
  // Restitution des paramètres manuels
  ReadParams();
}

//-------------------------------------------------------------------------
// On passe à l'action suivante
bool CModeAuto::NextAction()
{
  bool bOK = false;
  if (iLine >= 0 and currentAction.iAction <= PLAYDIR)
    Reader.CloseWavefile();
  // Action suivante
  bool bNewAction = false;
  do
  {
    iLine++;
    if (iLine >= iNbLine)
      iLine = 0;
    iLinePlus = iLine + 1;
    if (!lstScenario[iLine].bComment)
      bNewAction = true;
  }
  while (!bNewAction);
  //Serial.printf("CModeAuto::NextScenario line %d/%d\n", iLinePlus, iNbLine);
  // Init de l'action courante
  currentAction.iAction = lstScenario[iLine].iAction;
  currentAction.bComment = lstScenario[iLine].bComment;
  strcpy( currentAction.sTime, lstScenario[iLine].sTime);
  if (currentAction.iAction == PLAYDIR or currentAction.iAction == PLAYFILE)
    strcpy( currentAction.sDirPath, lstScenario[iLine].sDirPath);
  else
    currentAction.sDirPath[0] = 0;
  if (currentAction.iAction == PLAYFILE)
    strcpy( currentAction.sWavFile, lstScenario[iLine].sWavFile);
  else
    currentAction.sWavFile[0] = 0;
  // Initialisation des heures, minutes et secondes
  sscanf( currentAction.sTime, "%d:%d:%d", &iHour, &iMinutes, &iSeconds);

  // Attente du début de l'action
  switch(currentAction.iAction)
  {
  case PLAYFILE:
    bOK = SetPlayFile();
    break;
  case PLAYDIR :
    bOK = SetPlayDir();
    break;
  case ACTWAIT :
    bOK = SetWait();
    break;
  case WAKEUP  :
    bOK = SetWakeup();
    break;
  }
  //Serial.printf("After SetAction iDuration %d\n", iDuration);
  Coherenceparams();
  //Serial.printf("NextAction iModePlay %d, iStatus %d\n", paramsC.iModePlay, iStatus);
  return bOK;
}

//-------------------------------------------------------------------------
// Lancement d'une action lecture d'un fichier
bool CModeAuto::SetPlayFile()
{
  bool bOK = false;
  // Trace de l'action
  char temp[128];
  GetStringAction( temp, 128, &currentAction);
  iStatus = PLAYONE;
  // Initialisation du fichier à lire
  strcpy( paramsC.sCurrentDir, currentAction.sDirPath);
  strcpy( paramsC.sWaveName, currentAction.sWavFile);
  //Serial.printf("CModeAuto::SetPlayFile [%s] [%s]\n", currentAction.sDirPath, currentAction.sWavFile);
  if (InitWavFile())
  {
    paramsP.iPosPlay = 0;
    paramsP.iDurPlay = Reader.GetWaveDuration();
    // Test si durée à 0
    if (iHour == 0 and iMinutes == 0 and iSeconds == 0)
    {
      iDuration = paramsP.iDurPlay;
      paramsC.iModePlay = PLAYONE;
    }
    else
    {
      iDuration = iHour * 3600 + iMinutes * 60 + iSeconds;
      if (iDuration > (int)Reader.GetWaveDuration())
        paramsC.iModePlay = PLAYLOOP;
      else
        paramsC.iModePlay = PLAYONE;
    }
    iStatus = PLAYFILE;
    LogFile::AddLog( LLOG, txtLogPlayFile[paramsC.iLanguage], temp, iDuration);
    // Lancement de la lecture
    StartRead();
    bOK = true;
  }
  else
  {
    iStatus = SC_ERROR;
    strcpy( sStrError, txtErrorPlayFile[paramsC.iLanguage]);
    LogFile::AddLog( LLOG, txtLogErPlayFile[paramsC.iLanguage], temp);
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Lancement d'une action lecture d'un répertoire
bool CModeAuto::SetPlayDir()
{
  bool bOK = false;
  // Trace de l'action
  char temp[128];
  GetStringAction( temp, 128, &currentAction);
  iStatus = PLAYFILES;
  // Initialisation du répertoire à lire
  strcpy( paramsC.sCurrentDir, currentAction.sDirPath);
  //Serial.printf("CModeAuto::SetPlayDir [%s]\n", currentAction.sDirPath);
  // Init de la liste des fichiers wav du répertoire à lire
  ListFiles( paramsC.sCurrentDir);
  //Serial.printf("lstWaveFiles %d fichiers\n", lstWaveFiles.size());
  if (lstWaveFiles.size() > 0)
  {
    // Calcul de la durée des fichiers du répertoire
    iIndicePlay = -1;
    int iDirDuration = 0;
    CWaveFile wave;
    char sWaveName[80];
    for (int i=0; i<(int)lstWaveFiles.size(); i++)
    {
      strcpy( sWaveName, lstWaveFiles[i].c_str());
      if (sWaveName[0] != '/' and sWaveName[1] != '.' and sWaveName[1] != 0)
      {
        // Oui, c'est un fichier
        //Serial.printf("Test durée %s\n", sWaveName);
        wave.OpenWaveFileForRead( lstWaveFiles[i].c_str(), false);
        iDirDuration += (int)wave.GetPlayDuration();
        wave.CloseWavefile();
        if (iIndicePlay < 0)
          iIndicePlay = i;
      }
    }
    //Serial.printf("CModeAuto::SetPlayDir iIndicePlay %d\n", iIndicePlay);
    if (iIndicePlay >= 0)
    {
      // Sélection du premier fichier wav
      //Serial.printf("CModeAuto::SetPlayDir %d files, 1er %s\n", lstWaveFiles.size(), lstWaveFiles[iIndicePlay].c_str());
      strcpy( paramsC.sWaveName, lstWaveFiles[iIndicePlay].c_str());
      if (InitWavFile())
      {
        paramsP.iPosPlay = 0;
        paramsP.iDurPlay = Reader.GetWaveDuration();
        // Test si durée à 0
        if (iHour == 0 and iMinutes == 0 and iSeconds == 0)
        {
          iDuration = iDirDuration;
          paramsC.iModePlay = PLAYFILES;
        }
        else
        {
          iDuration = iHour * 3600 + iMinutes * 60 + iSeconds;
          if (iDirDuration > iDuration)
            paramsC.iModePlay = PLAYFILES;
          else
            paramsC.iModePlay = PLAYFLOOP;
        }
        iStatus = PLAYDIR;
        LogFile::AddLog( LLOG, txtLogPlayDir[paramsC.iLanguage], temp, iDuration);
        // Lancement de la lecture
        if (!StartRead())
        {
          StopRead();
          if (InitWavFile() and StartRead())
            bOK = false;
        }
        else
          bOK = true;
      }
    }
  }
  else
  {
    iStatus = SC_ERROR;
    strcpy( sStrError, txtErrorPlayDir[paramsC.iLanguage]);
    LogFile::AddLog( LLOG, txtLogErPlayDir[paramsC.iLanguage], temp);  
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Lancement d'une action de type attente
bool CModeAuto::SetWait()
{
  // Trace de l'action
  char temp[128];
  GetStringAction( temp, 128, &currentAction);
  // Calcul de la durée
  iDuration = iHour * 3600 + iMinutes * 60 + iSeconds;
  // Initialisation de la chaine d'attente
  sprintf( sStrWait, txtWaitSCH[paramsC.iLanguage], iDuration);
  iStatus = ACTWAIT;
  iAttenteVeille = 15;
  sprintf( sStrWaitStandby, txtIDXSCWSTBY[paramsC.iLanguage], temp, iAttenteVeille);
  LogFile::AddLog( LLOG, txtLogWait[paramsC.iLanguage], temp, iDuration);
  return true;
}

//-------------------------------------------------------------------------
// Lancement d'une action de type réveil a une certaine heure
bool CModeAuto::SetWakeup()
{
  // Trace de l'action
  char temp[128];
  GetStringAction( temp, 128, &currentAction);
  // Calcul du nombre de secondes du réveil par rapport à minuit
  int iDeb = iHour * 3600 + iMinutes * 60 + iSeconds;
  // Calcul du nombre de secondes par rapport à minuit pour l'heure courante
  int iCurrent = hour() * 3600 + minute() * 60 + second();
  // Calcul du nombre de secondes à attendre jusqu'à l'heure de départ de l'action
  if (iCurrent < iDeb)
    // Cas ou l'heure courante est plus petite que l'heure de début
    iDuration = iDeb - iCurrent;
  else
    // Cas ou l'heure courante et de début sont de chaque coté de minuit
    iDuration = iDeb + 24*3600 - iCurrent;
  //Serial.printf("SetWakeup iDuration %d\n", iDuration);
  // Initialisation de la chaine d'attente
  sprintf( sStrWakeup, txtWakeupSCH[paramsC.iLanguage], currentAction.sTime);
  iStatus = WAKEUP;
  iAttenteVeille = 15;
  sprintf( sStrWaitStandby, txtIDXSCWSTBY[paramsC.iLanguage], iAttenteVeille);
  LogFile::AddLog( LLOG, txtLogWakeup[paramsC.iLanguage], temp, currentAction.sTime);
  return true;
}
    
//-------------------------------------------------------------------------
// Attente de l'heure de réveil
bool CModeAuto::Standby()
{
  bool bOK = true;
  // Effacement de l'écran
  pDisplay->clearBuffer();
  pDisplay->sendBuffer();
  // Calcul de la durée de veille en H, M et S
  int iH, iM, iS;
  int iAttenteSecondes = iDuration;
  iH = iAttenteSecondes / 3600;
  iM = (iAttenteSecondes - iH*3600) / 60;
  iS = iAttenteSecondes - (iH*3600 + iM*60);
  iAttenteSecondes = iH*3600 + iM*60 + iS;
  //Serial.printf("Attente heure. Il est %02d:%02d:%02d, attente %d dans %02d:%02d:%02d\n", hour(), minute(), second(), iDuration, iH, iM, iS);
  //delay(300);
  int who = 0;
  // Init du bouton de sortie de veille
  digital.pinMode(PINPUSH, INPUT_PULLUP, RISING); // Push
  // ATTENTION, ne pas mettre l'heure du réveil mais le temps en H/Mn/secondes avant le réveil
  alarm.setRtcTimer( iH, iM, iS);
  //LogFile::AddLog( LLOG, "Réveil à %02d:%02d:%02d", hour(), minute(), second());
  // =====     Et veille    =====
  who = Snooze.hibernate( configTeensy);
  // ===== Sortie de veille =====
  Serial.printf("End wait %02d:%02d:%02d\n", hour(), minute(), second());
  // Initialise l'heure de la libairie Time avec la RTC du Teensy 3.0
  setSyncProvider(getTeensy3Time);
  // Initialisation de la carte SD
  if (!isSDInit())
  {
    Serial.println("Erreur SD en sortie de veille !");
    // Reset du logiciel
    CPU_RESTART;
  }
  else
    sd.vol()->freeClusterCount();
  // Test si sortie via bouton
  if (who == PINPUSH)
    // Passage en mode paramètres
    iNewMode = MPARAMS;
  else
  {
    //Serial.println("CModeAuto::Standby NextAction");
    // Fin de veille, on lance l'action suivante
    bOK = NextAction();
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModeAuto::OnEndChange(
  int idxModifier
  )
{
  // Selon le paramètre modifié
  switch (idxModifier)
  {
  case IDXMODE:
  case IDXSCPARAMS:
    // Passage en mode paramètres
    iNewMode = MPARAMS;
    break;
  default:
    // Appel de la fonction de base
    iNewMode = CModeGeneric::OnEndChange( idxModifier);
  }
  return iNewMode;
}

//-------------------------------------------------------------------------
// Init du fichier sélectionné
// Retourne true si fichier OK
bool CModeAuto::InitWavFile()
{
  bool bOK = true;
  //Serial.printf( "CModeAuto::InitWavFile [%s] [%s]\n", paramsC.sCurrentDir, paramsC.sWaveName);
  // Select root SD
  sd.chdir( "/");
  // Sélection du répertoire actif
  if (!sd.chdir( paramsC.sCurrentDir))
  {
    LogFile::AddLog( LLOG, txtLOGERDIR[paramsC.iLanguage], paramsC.sCurrentDir);
    bOK = false;
  }
  else
  {
    // Sélection du fichier wave
    if (!Reader.SetWaveFile( false, paramsC.sWaveName))
    {
      // Seconde tentative
      delay(500);
      if (!Reader.SetWaveFile( false, paramsC.sWaveName))
      {
        LogFile::AddLog( LLOG, txtLOGERFILE[CModeGeneric::GetParamsC()->iLanguage], paramsC.sWaveName);
        bOK = false;
      }
    }
    // Initialisation de l'atténuation de lecture
    ReadAttenuation();
    Reader.SetAttenuation( paramsP.iOptimisation);
    //Serial.printf( "CModeAuto::InitWavFile [%s] [%s] Attenuation %d\n", paramsC.sCurrentDir, paramsC.sWaveName, paramsP.iOptimisation);
    paramsP.iFe = Reader.GetFe()/1000;
    paramsP.cSaturation = ' ';
    // Copie des 19 caractères les plus significatifs du répertoire courant
    if (strlen(paramsC.sCurrentDir) <= MAXCHARLINE-2)
      strcpy( sCurrentDir, paramsC.sCurrentDir);
    else
      strcpy( sCurrentDir, &(paramsC.sCurrentDir[strlen(paramsC.sCurrentDir)-(MAXCHARLINE-2)]));
    // Préparation du nom du fichier
    String str = PrepareStrFile( paramsC.sWaveName);
    str.toCharArray( sCurrentFile, MAXCHARLINE);
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Init du fichier suivant dans la lecture d'un répertoire
// Retourne true si fichier OK, false si problème ou fin du répertoire
bool CModeAuto::NextWavFile(
  bool bBoucle  // true si boucle du répertoire
  )
{
  //Serial.printf("CModeAuto::NextWavFile iIndicePlay=%d, size %d\n", iIndicePlay, lstWaveFiles.size());
  bool bOK = false;
  bool bFinRep = false;
  // Sélection du nouveau fichier à lire
  do
  {
    iIndicePlay++;
    if (iIndicePlay >= (int)lstWaveFiles.size())
    {
      // On boucle au début de la liste
      iIndicePlay = 0;
      if (bFinRep)
        // On a déjà fait un tour sans rien trouver, on sort en erreur
        bOK = true;
      else
        // On indique avoir passé la fin du répertoire
        // Mais on continu la recherche pour trouver le 1er fichier valide
        bFinRep = true;
    }
    if (iIndicePlay < (int)lstWaveFiles.size())
    {
      // Récupération du nom du fichier sélectionné
      char sTemp[80];
      lstWaveFiles[iIndicePlay].toCharArray( sTemp, 80);
      // Test si c'est un fichier
      if (sTemp[0] != '/' and sTemp[1] != '.' and sTemp[1] != 0)
        // Oui, c'est un fichier, on sort
        bOK = true;
    }
  }
  while (!bOK);
  char sTemp[80];
  lstWaveFiles[iIndicePlay].toCharArray( sTemp, 80);
  // Test si c'est un fichier
  if (sTemp[0] != '/' and sTemp[1] != '.' and sTemp[1] != 0)
  {
      // Oui, c'est un fichier, on  sélectionne le fichier à lire
    paramsC.iSelect = iIndicePlay;
    strcpy( paramsC.sWaveName, lstWaveFiles[paramsC.iSelect].c_str());
    //Serial.printf("CModeAuto::NextWavFile Lecture iIndicePlay=%d, wave=%s\n", iIndicePlay, paramsC.sWaveName);
    if (InitWavFile())
      // On le passe en lecture
      StartRead();
    else
    {
      // On stoppe la lecture sur erreur
      strcpy( paramsC.sWaveName, " ");
      paramsP.iEtatPlay = SPAUSE;
      bOK = false;
    }
  }
  else
  {
    // On stoppe la lecture sur erreur
    strcpy( paramsC.sWaveName, " ");
    paramsP.iEtatPlay = SPAUSE;
    bOK = false;
    //Serial.printf("CModeAuto::NextWavFile Erreur !\n");
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief To display information outside of modifiers
void CModeAuto::AddPrint()
{
  bool bNewSecond = false;
  if (second() != iCurrentSeconde)
  {
    //Serial.printf("AddPrint iModePlay %d, iStatus %d\n", paramsC.iModePlay, iStatus);
    bNewSecond = true;
    iCurrentSeconde = second();
    if (iDuration > 0)
    {
      iDuration--;
      if (iDuration == 0)
      {
        if (Reader.IsReading())
          Reader.StopRead();
        // Fin de l'action courante, on lance l'action suivante
        NextAction();
      }
      else
      {
        // Selon l'action courante
        switch(iStatus)
        {
        case PLAYFILE:
          // Fin de lecture, on relance la lecture
          if (!Reader.IsReading())
          {
            //Serial.printf("Restart Read\n");
            StopRead();
            StartRead();
          }
          break;
        case PLAYDIR :
          if (!Reader.IsReading())
          {
            //  On recherche un nouveau fichier
            bool bBoucle = false;
            if (paramsC.iModePlay == PLAYFLOOP)
              bBoucle = true;
            //Serial.printf("Restart Read dir\n");
            NextWavFile( bBoucle);
          }
          break;
        case ACTWAIT :
        case WAKEUP  :
          if (bNewSecond and iAttenteVeille > 0)
          {
            iAttenteVeille--;
            sprintf( sStrWaitStandby, txtIDXSCWSTBY[paramsC.iLanguage], iAttenteVeille);
            if (iAttenteVeille == 0)
              // Passage en veille
              Standby();
          }
          break;
        case SC_ERROR:
          break;
        }
      }
    }
  }
  if (iStatus <= PLAYDIR)
  {
    paramsP.iPosPlay    = Reader.GetWavePos();         // Position en secondes de la lecture du fichier courant
    paramsP.iDurPlay    = Reader.GetWaveDuration();    // Durée max en secondes du fichier courant
    paramsP.iPosPlayPc  = Reader.GetPosLecture();      // Position en pourcent de la lecture du fichier courant
    // Force l'affichage de certains paramètres
    LstModifParams[IDXTPOS ]->SetRedraw( true);
    LstModifParams[IDXBARRE]->SetRedraw( true);
    // Affichage de la saturation éventuelle
    if (Reader.IsSaturation())
      paramsP.cSaturation = 71;      // Saturation des échantillons du fichier (12 bits max)
    else if (Reader.IsSignalFaible())
      paramsP.cSaturation = 68;      // Signal faible des échantillons du fichier (12 bits max)
    else
      paramsP.cSaturation = 63;       // Echantillons OK
    pDisplay->setFont(u8g2_font_open_iconic_arrow_1x_t );  // Hauteur 7
    pDisplay->drawGlyph( 120, 28, paramsP.cSaturation);
    pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  }
  /*if (bNewSecond)
    Serial.printf("AddPrint paramsC.iModePlay %d\n", paramsC.iModePlay);*/
}

//-------------------------------------------------------------------------
// Lecture du choix d'atténuation d'un fichier
// Dans NonFichierwav.txt, récupère l'indice d'atténuation
void CModeAuto::ReadAttenuation()
{
  // Sélection du répertoire courant
  sd.chdir( paramsC.sCurrentDir);
  // Par défaut, atténuation par défaut
  paramsP.iOptimisation = paramsC.iDefGain;
  paramsC.bFiltre = false;
  // Création du nom du fichier (wav remplacé par txt)
  char sFileName[255];
  strcpy(sFileName, paramsC.sWaveName);
  sFileName[strlen(sFileName)-4] = 0;
  strcat(sFileName, ".txt");
  /*Serial.print("ReadAttenuation ");
  Serial.println(sFileName);*/
  // Ouverture du fichier
  SdFile dataFile;
  if (dataFile.open( sFileName, O_READ))
  {
    // Lecture de l'index de l'atténuation dans le fichier
    char sTemp[256];
    if (dataFile.fgets(sTemp, 255) > 0)
      sscanf (sTemp, "%d", &(paramsP.iOptimisation));
    char *p = strstr( sTemp, ", ");
    if (p == NULL)
      // Optimisation avec filtre absente, on initialise avec la valeur sans filtre
      paramsP.iOptimFiltre = paramsP.iOptimisation;
    else
      sscanf (&p[1], "%d", &(paramsP.iOptimFiltre));
    // Fermeture du fichier
    dataFile.close();
    //Serial.printf("ReadAttenuation %s %d, %d, [%s]\n", sFileName, paramsP.iOptimisation, paramsP.iOptimFiltre, sTemp);
  }
}

//-------------------------------------------------------------------------
// Lance la lecture du fichier
bool CModeAuto::StartRead()
{
  Reader.SetFiltre( paramsC.bFiltre);
  return Reader.StartRead();
}

//-------------------------------------------------------------------------
// Stoppe la lecture du fichier
void CModeAuto::StopRead()
{
  char sTemp[80];
  if (Reader.IsSaturation())
    strcpy( sTemp, txtLOGSATUR[CModeGeneric::GetParamsC()->iLanguage]);
  else if (Reader.IsSignalFaible())
    strcpy( sTemp, txtLOGWEAK[CModeGeneric::GetParamsC()->iLanguage]);
  else
    strcpy( sTemp, " ");
  Reader.StopRead();
  LogFile::AddLog( LLOG, txtLOGSTOPRO[CModeGeneric::GetParamsC()->iLanguage], paramsC.sCurrentDir, paramsC.sWaveName, sTemp);
}

//-------------------------------------------------------------------------
//! \brief Cohérence entre les différents paramètres
void CModeAuto::Coherenceparams()
{
  //Serial.printf("CModeAuto::Coherenceparams iStatus %d, sCurrentFile %s\n", iStatus, sCurrentFile);
  switch (iStatus)
  {
  case PLAYFILE:
  case PLAYDIR :
    LstModifParams[IDXSCPLAY  ]->SetbValid( true);
    LstModifParams[IDXSCBARRE ]->SetbValid( true);
    LstModifParams[IDXSCTMAX  ]->SetbValid( true);
    LstModifParams[IDXSCTPOS  ]->SetbValid( true);
    LstModifParams[IDXSCFILE  ]->SetbValid( true);
    LstModifParams[IDXSCFE    ]->SetbValid( true);
    LstModifParams[IDXSCMPLAY ]->SetbValid( true);
    LstModifParams[IDXSCDIR   ]->SetbValid( true);
    LstModifParams[IDXSCATTEN ]->SetbValid( true);
    LstModifParams[IDXSCWAIT  ]->SetbValid( false);
    LstModifParams[IDXSCWAKEUP]->SetbValid( false);
    LstModifParams[IDXSCWSTBY ]->SetbValid( false);
    LstModifParams[IDXSCCLIC  ]->SetbValid( false);
    LstModifParams[IDXSCERROR ]->SetbValid( false);
    break;
  case ACTWAIT :
    LstModifParams[IDXSCPLAY  ]->SetbValid( false);
    LstModifParams[IDXSCBARRE ]->SetbValid( false);
    LstModifParams[IDXSCTMAX  ]->SetbValid( false);
    LstModifParams[IDXSCTPOS  ]->SetbValid( false);
    LstModifParams[IDXSCFILE  ]->SetbValid( false);
    LstModifParams[IDXSCFE    ]->SetbValid( false);
    LstModifParams[IDXSCMPLAY ]->SetbValid( false);
    LstModifParams[IDXSCDIR   ]->SetbValid( false);
    LstModifParams[IDXSCATTEN ]->SetbValid( false);
    LstModifParams[IDXSCWAIT  ]->SetbValid( true);
    LstModifParams[IDXSCWAKEUP]->SetbValid( false);
    LstModifParams[IDXSCWSTBY ]->SetbValid( true);
    LstModifParams[IDXSCCLIC  ]->SetbValid( true);
    LstModifParams[IDXSCERROR ]->SetbValid( false);
    break;
  case WAKEUP:
    LstModifParams[IDXSCPLAY  ]->SetbValid( false);
    LstModifParams[IDXSCBARRE ]->SetbValid( false);
    LstModifParams[IDXSCTMAX  ]->SetbValid( false);
    LstModifParams[IDXSCTPOS  ]->SetbValid( false);
    LstModifParams[IDXSCFILE  ]->SetbValid( false);
    LstModifParams[IDXSCFE    ]->SetbValid( false);
    LstModifParams[IDXSCMPLAY ]->SetbValid( false);
    LstModifParams[IDXSCDIR   ]->SetbValid( false);
    LstModifParams[IDXSCATTEN ]->SetbValid( false);
    LstModifParams[IDXSCWAIT  ]->SetbValid( false);
    LstModifParams[IDXSCWAKEUP]->SetbValid( true);
    LstModifParams[IDXSCWSTBY ]->SetbValid( true);
    LstModifParams[IDXSCCLIC  ]->SetbValid( true);
    LstModifParams[IDXSCERROR ]->SetbValid( false);
    break;
  case SC_ERROR:
  default:
    LstModifParams[IDXSCPLAY  ]->SetbValid( false);
    LstModifParams[IDXSCBARRE ]->SetbValid( false);
    LstModifParams[IDXSCTMAX  ]->SetbValid( false);
    LstModifParams[IDXSCTPOS  ]->SetbValid( false);
    LstModifParams[IDXSCFILE  ]->SetbValid( false);
    LstModifParams[IDXSCFE    ]->SetbValid( false);
    LstModifParams[IDXSCMPLAY ]->SetbValid( false);
    LstModifParams[IDXSCDIR   ]->SetbValid( false);
    LstModifParams[IDXSCATTEN ]->SetbValid( false);
    LstModifParams[IDXSCWAIT  ]->SetbValid( false);
    LstModifParams[IDXSCWAKEUP]->SetbValid( false);
    LstModifParams[IDXSCWSTBY ]->SetbValid( false);
    LstModifParams[IDXSCCLIC  ]->SetbValid( true);
    LstModifParams[IDXSCERROR ]->SetbValid( true);
    break;
  }
  /*for (int i=0; i<LstModifParams.size(); i++)
  {
    if (LstModifParams[i]->GetbValid())
      Serial.printf("Param %d valid\n", i);
    else
      Serial.printf("Param %d invalid\n", i);
  }*/
}
  
