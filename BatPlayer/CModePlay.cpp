/* 
 * File:   CModePlay.h
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <U8g2lib.h>
#include <EEPROM.h>
#include "SdFat.h"
#include "Const.h"
#include "CModePlay.h"

// Gestionnaire écran
extern U8G2 *pDisplay;

// Gestionnaire de carte SD spécifique Teensy 3.6
#ifdef SD_FAT_TYPE
  extern SdFat sd;
#endif
#ifdef MMC_SDFAT
  extern SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  extern SdFatSdioEX sd;
#endif

const char *txtStrP[MAXLANGUAGE]  = {"%s",
                                     "%s",
                                     "%s",
                                     "%s"};
const char *txtStrPB[MAXLANGUAGE] = {"[%s]",
                                     "[%s]",
                                     "[%s]",
                                     "[%s]"};
const char *txtInt[MAXLANGUAGE]   = {"%03d s",
                                     "%03d s",
                                     "%03d s",
                                     "%03d s"};
//                              12345678912345
const char formIDXFLT[]    =   "          &$$ ";
extern const char *txtFE[];
extern const char *txtMODESPLAY[MAXLANGUAGE][4];
extern const char *txtAttenuation[MAXLANGUAGE][11];
extern const char *txtIDXFLT[];
extern const char *txtFLT[MAXLANGUAGE][2];
extern const char *txtREAD[];
extern const char *txtLOOP[];
extern const char *txtRDIR[];
extern const char *txtRLDIR[];
extern const char *txtLOGERDIR[];
extern const char *txtLOGERFILE[];
extern const char *txtLOGSTOPR[];
extern const char *txtLOGSATUR[];
extern const char *txtLOGWEAK[];
extern const char *txtLOGERATT[];
extern const char *txtLOGSTARTR[];
extern const char *txtLOGSTOPRO[];

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Play
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModePlay::CModePlay() : CModeGeneric()
{
  paramsP.iEtatPlay     = SPAUSE;
  paramsP.iPosPlay      = 0;
  paramsP.iDurPlay      = 100;
  paramsP.iPosPlayPc    = 10;
  paramsP.iFe           = 384;
  paramsP.cSaturation   = ' ';
  paramsP.iOptimisation = 0;
  paramsP.iOptimFiltre  = 0;
  // Copie des 19 caractères les plus significatifs du répertoire courant
  if (strlen(paramsC.sCurrentDir) <= MAXCHARLINE-2)
    strcpy( sCurrentDir, paramsC.sCurrentDir);
  else
    strcpy( sCurrentDir, &(paramsC.sCurrentDir[strlen(paramsC.sCurrentDir)-(MAXCHARLINE-2)]));
  // Préparation du nom du fichier
  String str = PrepareStrFile( paramsC.sWaveName);
  str.toCharArray( sCurrentFile, MAXCHARLINE);
  // Ecran 128 x 64 pixels Fonte 0 (H 8xL 6), 8 lignes de 21 caractères, lignes 11, 20, 29, 38, 47, 56
  // Initialisation des paramètres
  // Création des modificateurs du mode Play dans l'ordre de l'énuméré IDXPARAMS
  // Modificateur de Lecture/Pause
  LstModifParams.push_back(new CPlayRecModifier( &(paramsP.iEtatPlay), true, false, 24, 9, 0, 10));
  // Barregraphe de la position de lecture
  LstModifParams.push_back(new CDrawBarModifier( &(paramsP.iPosPlayPc), 98, 8, 28, 11));
  // Durée max de lecture
  LstModifParams.push_back(new CModificatorInt( "&$$  ", false, txtInt, &(paramsP.iDurPlay), 0, 999, 1, 97, 19));
  // Position de lecture en secondes
  LstModifParams.push_back(new CModificatorInt( "&$$  ", false, txtInt, &(paramsP.iPosPlay), 0, 999, 1, 28, 19));
  // Nom du fichier wav courant                12345678901234567890
  LstModifParams.push_back(new CCharModifier( "&$$$$$$$$$$$$$$$$$$$", false, txtStrP, sCurrentFile, 16, false, false, false, false, false, 0, 37));
  // Fréquence d'échantillonnage du fichier courant
  LstModifParams.push_back(new CModificatorInt( "&$$", false, txtFE, &(paramsP.iFe), 0, 999, 1, 66, 28));
  // Mode de lecture des fichiers
  LstModifParams.push_back(new CEnumModifier( " &$$$$$$$ ", false, true, txtStrPB, &(paramsC.iModePlay), PLAYMAX, (const char **)txtMODESPLAY, 0, 28));
  // Nom du répertoire courant
  LstModifParams.push_back(new CCharModifier( "&$$", false, txtStrPB, sCurrentDir, 16, false, false, false, false, false, 0, 46));
  // Atténuation éventuelle de la lecture (Sans filtre)
  LstModifParams.push_back(new CEnumModifier ( "[&$$$$]", false, false, txtStrPB, &(paramsP.iOptimisation), MAXDB, (const char **)txtAttenuation, 0, 55));
  // Atténuation éventuelle de la lecture (Avec Filtre)
  LstModifParams.push_back(new CEnumModifier ( "[&$$$$]", false, false, txtStrPB, &(paramsP.iOptimFiltre), MAXDB, (const char **)txtAttenuation, 0, 55));
  // IDXFLT      Avec ou sans filtre passe haut
  LstModifParams.push_back(new CBoolModifier ( formIDXFLT, false, true, txtIDXFLT, &paramsC.bFiltre, (const char **)txtFLT, 45, 55));
  // Paramètres non modifiable
  LstModifParams[IDXBARRE]->SetEditable( false);
  LstModifParams[IDXTMAX ]->SetEditable( false);
  LstModifParams[IDXTPOS ]->SetEditable( false);
  LstModifParams[IDXFILE ]->SetEditable( false);
  LstModifParams[IDXFE   ]->SetEditable( false);
  LstModifParams[IDXDIR  ]->SetEditable( false);
  LstModifParams[IDXFLT  ]->SetEditable( false);
}

//-------------------------------------------------------------------------
// Destructeur
CModePlay::~CModePlay()
{
}

//-------------------------------------------------------------------------
// Début du mode
void CModePlay::BeginMode()
{
  //Serial.printf("CModePlay::BeginMode sCurrentDir=%s, sWaveName=%s\n", paramsC.sCurrentDir, paramsC.sWaveName);
  if (lstWaveFiles.size() == 0)
    // Init de la liste des fichiers wav du répertoire courant
    ListFiles( paramsC.sCurrentDir);
  /*Serial.print("CModePlay::BeginMode lstWaveFiles.size()=");
  Serial.println(lstWaveFiles.size());
  for (int i=0; i<lstWaveFiles.size(); i++)
  {
    char sTemp[80];
    lstWaveFiles[i].toCharArray( sTemp, 80);
    Serial.print(i);
    Serial.print(", sTemp=");
    Serial.println(sTemp);
  }*/
  paramsC.iMode = IDXMODEPLAY;
  iIndicePlay = paramsC.iSelect;
  // Appel de la classe de base
  CModeGeneric::BeginMode();
  // Si on vient de sélectionner un fichier
  if (bNewSelFile)
  {
    // Selection du paramètre Play
    LstModifParams[idxSelParam]->SetbSelect( false);
    idxSelParam = IDXPLAY;
    LstModifParams[idxSelParam]->SetbSelect( true);
    bNewSelFile = false;
  }
  // Initialisation filtre
  Reader.SetFiltre( paramsC.bFiltre);
  // Cohérence des paramètres
  Coherenceparams();
  // Init du filtre
  //Reader.SetFiltre( paramsC.bFiltre);
  // Initialisation du fichier sélectionné
  if (!InitWavFile())
    paramsC.sWaveName[0] = 0;
}

//-------------------------------------------------------------------------
// Fin du mode
void CModePlay::EndMode()
{
  if (Reader.IsReading())
    StopRead();
  // Appel de la classe de base
  CModeGeneric::EndMode();
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModePlay::OnEndChange(
  int idxModifier
  )
{
  // Selon le paramètre modifié
  switch (idxModifier)
  {
  case IDXPLAY: // Start/Stop lecture
    // Modification du paramètre Start/Stop lecture
    if (paramsP.iEtatPlay == 1 and !Reader.IsReading())
    {
      // En fonction du mode de lecture
      switch( paramsC.iModePlay)
      {
      case PLAYONE:
        LogFile::AddLog( LLOG, txtREAD[CModeGeneric::GetParamsC()->iLanguage]);
        break;
      case PLAYLOOP:
        LogFile::AddLog( LLOG, txtLOOP[CModeGeneric::GetParamsC()->iLanguage]);
        break;
      case PLAYFILES:
        LogFile::AddLog( LLOG, txtRDIR[CModeGeneric::GetParamsC()->iLanguage]);
        break;
      case PLAYFLOOP:
        LogFile::AddLog( LLOG, txtRLDIR[CModeGeneric::GetParamsC()->iLanguage]);
        break;
      }
      // Lancement de la lecture
      StartRead();
    }
    else if (paramsP.iEtatPlay == 0 and Reader.IsReading())
    {
      // Arret de la lecture
      //Serial.println("CModePlay::ManageKey Arret de la lecture");
      StopRead();
    }
    break;
  case IDXATTEN: // Changement de l'atténuation du fichier sélectionné
  case IDXATTENF:
    // Modification de l'atténuation de lecture
    SaveAttenuation();
    if (paramsC.bFiltre)
      Reader.SetAttenuation( paramsP.iOptimFiltre);
    else
      Reader.SetAttenuation( paramsP.iOptimisation);
    break;
  /*case IDXFLT: // Changement du filtre
    if (paramsC.bFiltre)
      Reader.SetAttenuation( paramsP.iOptimFiltre);
    else
      Reader.SetAttenuation( paramsP.iOptimisation);
    break;*/
  default:
    // Appel de la fonction de base
    iNewMode = CModeGeneric::OnEndChange( idxModifier);
  }
  // Cohérence des paramètres
  Coherenceparams();
  // Ecriture éventuelle des paramètres en EEPROM
  WriteParams();
  return iNewMode;
}
  
//-------------------------------------------------------------------------
// Init du fichier sélectionné
// Retourne true si fichier OK
bool CModePlay::InitWavFile()
{
  bool bOK = true;
  // Sélection du répertoire actif
  if (!sd.chdir( paramsC.sCurrentDir))
  {
      LogFile::AddLog( LLOG, txtLOGERDIR[CModeGeneric::GetParamsC()->iLanguage], paramsC.sCurrentDir);
      bOK = false;
      strcpy( sCurrentDir , " ");
      strcpy( sCurrentFile, " ");
  }
  else
  {
    // Sélection du fichier wave
    /*Serial.print("CModePlay::BeginMode paramsC.sWaveName [");
    Serial.print( paramsC.sWaveName);
    Serial.println("]");*/
    if (!Reader.SetWaveFile( false, paramsC.sWaveName))
    {
      // Seconde tentative
      delay(500);
      if (!Reader.SetWaveFile( false, paramsC.sWaveName))
      {
        LogFile::AddLog( LLOG, txtLOGERFILE[CModeGeneric::GetParamsC()->iLanguage], paramsC.sWaveName);
        bOK = false;
      }
    }
    // Initialisation de l'atténuation de lecture
    ReadAttenuation();
    if (paramsC.bFiltre)
      Reader.SetAttenuation( paramsP.iOptimFiltre);
    else
      Reader.SetAttenuation( paramsP.iOptimisation);
    paramsP.iFe = Reader.GetFe()/1000;
    paramsP.cSaturation = ' ';
    // Copie des 19 caractères les plus significatifs du répertoire courant
    if (strlen(paramsC.sCurrentDir) <= MAXCHARLINE-2)
      strcpy( sCurrentDir, paramsC.sCurrentDir);
    else
      strcpy( sCurrentDir, &(paramsC.sCurrentDir[strlen(paramsC.sCurrentDir)-(MAXCHARLINE-2)]));
    // Préparation du nom du fichier
    String str = PrepareStrFile( paramsC.sWaveName);
    str.toCharArray( sCurrentFile, MAXCHARLINE);
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Init du fichier suivant dans la lecture d'un répertoire
// Retourne true si fichier OK, false si problème ou fin du répertoire
bool CModePlay::NextWavFile(
  bool bBoucle  // true si boucle du répertoire
  )
{
  //Serial.printf("CModePlay::NextWavFile iIndicePlay=%d, size %d\n", iIndicePlay, lstWaveFiles.size());
  bool bOK = false;
  bool bFinRep = false;
  // Sélection du nouveau fichier à lire
  do
  {
    iIndicePlay++;
    if (iIndicePlay >= lstWaveFiles.size())
    {
      // On boucle au début de la liste
      iIndicePlay = 0;
      if (bFinRep)
        // On a déjà fait un tour sans rien trouvé, on sort en erreur
        bOK = true;
      else
        // On indique avoir passé la fin du répertoire
        // Mais on continu la recherche pour trouver le 1er fichier valide
        bFinRep = true;
    }
    if (iIndicePlay < lstWaveFiles.size())
    {
      // Récupération du nom du fichier sélectionné
      char sTemp[80];
      lstWaveFiles[iIndicePlay].toCharArray( sTemp, 80);
      // Test si c'est un fichier
      if (sTemp[0] != '/' and sTemp[1] != '.' and sTemp[1] != 0)
        // Oui, c'est un fichier, on sort
        bOK = true;
    }
  }
  while (!bOK);
  if (bFinRep and !bBoucle)
  {
    // On stoppe la lecture et on initialise le fichier sélectionné
    paramsP.iEtatPlay = SPAUSE;
    paramsC.iSelect = iIndicePlay;
    lstWaveFiles[paramsC.iSelect].toCharArray( paramsC.sWaveName, 255);
    // Préparation du nom du fichier
    String str = PrepareStrFile( paramsC.sWaveName);
    str.toCharArray( sCurrentFile, MAXCHARLINE);
    bOK = false;
    //Serial.printf("CModePlay::NextWavFile Fin lecture iIndicePlay=%d, wave=%s\n", iIndicePlay, paramsC.sWaveName);
  }
  else
  {
    // On sélectionne le fichier à lire
    paramsC.iSelect = iIndicePlay;
    lstWaveFiles[paramsC.iSelect].toCharArray( paramsC.sWaveName, 255);
    //Serial.printf("CModePlay::NextWavFile Lecture iIndicePlay=%d, wave=%s\n", iIndicePlay, paramsC.sWaveName);
    if (InitWavFile())
      // On le passe en lecture
      StartRead();
    else
    {
      // On stoppe la lecture sur erreur
      strcpy( paramsC.sWaveName, " ");
      paramsP.iEtatPlay = SPAUSE;
      bOK = false;
    }
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief To display information outside of modifiers
void CModePlay::AddPrint()
{
  // Initialisation des infos de lecture
  if (paramsP.iEtatPlay == 1 and !Reader.IsReading())
  {
    // Fin de la lecture, mémo de l'arret
    char sTemp[80];
    if (Reader.IsSaturation())
      strcpy( sTemp, txtLOGSATUR[CModeGeneric::GetParamsC()->iLanguage]);
    else if (Reader.IsSignalFaible())
      strcpy( sTemp, txtLOGWEAK[CModeGeneric::GetParamsC()->iLanguage]);
    else
      strcpy( sTemp, " ");
    LogFile::AddLog( LLOG, txtLOGSTOPR[CModeGeneric::GetParamsC()->iLanguage], paramsC.sCurrentDir, paramsC.sWaveName, sTemp);
    // Test si on doit relancer
    switch (paramsC.iModePlay)
    {
    default:
    case PLAYONE:   // Lecture du fichier courant puis stop
      // On stoppe la lecture
      paramsP.iEtatPlay = SPAUSE;
      LstModifParams[IDXPLAY]->SetRedraw( true);
      break;
    case PLAYLOOP:  // Lecture en boucle du fichier courant
      // On relance la lecture du fichier courant
      StartRead();
      break;
    case PLAYFILES: // Lecture des fichiers du répertoire puis stop
      NextWavFile( false);
      break;
    case PLAYFLOOP: // Lecture en boucle des fichiers du répertoire
      NextWavFile( true);
      break;
    }
  }
  paramsP.iPosPlay    = Reader.GetWavePos();         // Position en secondes de la lecture du fichier courant
  paramsP.iDurPlay    = Reader.GetWaveDuration();    // Durée max en secondes du fichier courant
  paramsP.iPosPlayPc  = Reader.GetPosLecture();      // Position en pourcent de la lecture du fichier courant
  // Force l'affichage de certains paramètres
  LstModifParams[IDXTPOS ]->SetRedraw( true);
  LstModifParams[IDXBARRE]->SetRedraw( true);
  // Affichage de la saturation éventuelle
  if (Reader.IsSaturation())
    paramsP.cSaturation = 71;      // Saturation des échantillons du fichier (12 bits max)
  else if (Reader.IsSignalFaible())
    paramsP.cSaturation = 68;      // Signal faible des échantillons du fichier (12 bits max)
  else
    paramsP.cSaturation = 63;      // Echantillons OK
  pDisplay->setFont(u8g2_font_open_iconic_arrow_1x_t );  // Hauteur 7
  pDisplay->drawGlyph( 120, 28, paramsP.cSaturation);
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  /*switch (paramsP.cSaturation)
  {
  case 71: Serial.println("Saturation"); break;
  case 68: Serial.println("Signal faible"); break;
  case 63: Serial.println("Ech OK"); break;
  default: Serial.println("Ech ???");
  }*/
}
  
//-------------------------------------------------------------------------
// Sauvegarde du choix d'atténuation d'un fichier
// Dans NonFichierwav.txt, sauvegarde de l'indice d'atténuation
void CModePlay::SaveAttenuation()
{
  //Serial.printf("CModePlay::SaveAttenuation A %s\n", paramsC.sWaveName);
  // Sélection du répertoire courant
  sd.chdir( paramsC.sCurrentDir);
  // Création du nom du fichier (wav remplacé par txt)
  char sFileName[255];
  strcpy(sFileName, paramsC.sWaveName);
  sFileName[strlen(sFileName)-4] = 0;
  strcat(sFileName, ".txt");
  // Ouverture du fichier
  SdFile dataFile;
  if (dataFile.open( sFileName, O_CREAT | O_WRITE))
  {
    // Ecriture de l'index de l'atténuation dans le fichier
    char sTemp[256];
    sprintf (sTemp, "%d (%s), %d (%s)", paramsP.iOptimisation, (char *)txtAttenuation[paramsC.iLanguage][paramsP.iOptimisation], paramsP.iOptimFiltre, (char *)txtAttenuation[paramsC.iLanguage][paramsP.iOptimFiltre]);
    dataFile.println( sTemp);
    // Fermeture du fichier
    dataFile.sync();
    dataFile.close();
  }
  else
    LogFile::AddLog( LLOG, txtLOGERATT[CModeGeneric::GetParamsC()->iLanguage], sFileName);
}

//-------------------------------------------------------------------------
// Lecture du choix d'atténuation d'un fichier
// Dans NonFichierwav.txt, récupère l'indice d'atténuation
void CModePlay::ReadAttenuation()
{
  // Sélection du répertoire courant
  sd.chdir( paramsC.sCurrentDir);
  // Par défaut, atténuation par défaut
  paramsP.iOptimisation = paramsC.iDefGain;
  // Création du nom du fichier (wav remplacé par txt)
  char sFileName[255];
  strcpy(sFileName, paramsC.sWaveName);
  sFileName[strlen(sFileName)-4] = 0;
  strcat(sFileName, ".txt");
  /*Serial.print("ReadAttenuation ");
  Serial.println(sFileName);*/
  // Ouverture du fichier
  SdFile dataFile;
  if (dataFile.open( sFileName, O_READ))
  {
    // Lecture de l'index de l'atténuation dans le fichier
    char sTemp[256];
    if (dataFile.fgets(sTemp, 255) > 0)
      sscanf (sTemp, "%d", &(paramsP.iOptimisation));
    char *p = strstr( sTemp, ", ");
    if (p == NULL)
      // Optimisation avec filtre absente, on initialise avec la valeur sans filtre
      paramsP.iOptimFiltre = paramsP.iOptimisation;
    else
      sscanf (&p[1], "%d", &(paramsP.iOptimFiltre));
    // Fermeture du fichier
    dataFile.close();
    //Serial.printf("ReadAttenuation %s %d, %d, [%s]\n", sFileName, paramsP.iOptimisation, paramsP.iOptimFiltre, sTemp);
  }
}

//-------------------------------------------------------------------------
// Lance la lecture du fichier
void CModePlay::StartRead()
{
  LogFile::AddLog( LLOG, txtLOGSTARTR[CModeGeneric::GetParamsC()->iLanguage], paramsC.sCurrentDir, paramsC.sWaveName, txtAttenuation[LFRENCH][paramsP.iOptimisation]);
  //Reader.SetFiltre( paramsC.bFiltre);
  Reader.StartRead();
}

//-------------------------------------------------------------------------
// Stoppe la lecture du fichier
void CModePlay::StopRead()
{
  char sTemp[80];
  if (Reader.IsSaturation())
    strcpy( sTemp, txtLOGSATUR[CModeGeneric::GetParamsC()->iLanguage]);
  else if (Reader.IsSignalFaible())
    strcpy( sTemp, txtLOGWEAK[CModeGeneric::GetParamsC()->iLanguage]);
  else
    strcpy( sTemp, " ");
  Reader.StopRead();
  LogFile::AddLog( LLOG, txtLOGSTOPRO[CModeGeneric::GetParamsC()->iLanguage], paramsC.sCurrentDir, paramsC.sWaveName, sTemp);
}

//-------------------------------------------------------------------------
//! \brief Cohérence entre les différents paramètres
void CModePlay::Coherenceparams()
{
  // Atténuation en fonction d'avec ou sans filtre
  if (paramsC.bFiltre)
  {
    LstModifParams[IDXATTEN ]->SetbValid( false);
    LstModifParams[IDXATTENF]->SetbValid( true);
  }
  else
  {
    LstModifParams[IDXATTEN ]->SetbValid( true);
    LstModifParams[IDXATTENF]->SetbValid( false);
  }
}

//-------------------------------------------------------------------------
//! \brief Gestion des tâches de fonds
void CModePlay::OnLoop()
{
  Reader.OnLoop();
}


