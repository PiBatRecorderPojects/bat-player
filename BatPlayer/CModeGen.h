/* 
 * File:   CModeGen.h
   BatPlayer Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CADCVoltage
  * - CModeGeneric
  * -   CModeCopyright
  */
#include "ModesModifiers.h"

#ifndef CMODEGEN_H
#define CMODEGEN_H


//-------------------------------------------------------------------------
//! \class CADCVoltage
//! \brief Management class of an MCP3221 for reading battery voltages
class CADCVoltage {
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructor (initialization of parameters to default values)
    CADCVoltage();

    //-------------------------------------------------------------------------
    //! \brief Test if the MCP3221 is present
    //! \return True if a MCP3221 is present
    bool begin();

    //-------------------------------------------------------------------------
    //! \brief Read humidity
    //! \return Batterie voltage in volt
    float getVoltage();

  protected:
    //-------------------------------------------------------------------------
    //! \brief Read ADC MCP3221
    //! \return ADC value
    uint16_t read();

    //! Indicates that the MCP initialization is OK
    bool bInitOK;
};

/*-----------------------------------------------------------------------------
 Classe générique de gestion d'un mode de fonctionnement
 Définie l'interface minimale de gestion d'un mode de fonctionnement
 Gère le menu de changement de mode, l'état de la batterie et la luminosité écran
-----------------------------------------------------------------------------*/
class CModeGeneric: public CGenericMode
{
public:
  //-------------------------------------------------------------------------
  // Constructeur
  //! \param bparams true si avec les paramètres communs aux différents modes
  CModeGeneric(
    bool bparams = true
    );

  //-------------------------------------------------------------------------
  // Destructeur
  virtual ~CModeGeneric();
  
  //-------------------------------------------------------------------------
  // Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  // Fin du mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  // Initialisation des paramètres par défaut
  static void SetDefault();
  
  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief Reading saved settings
  virtual bool ReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief Reading saved settings in static function
  static bool StaticReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief Writing parameters to save them
  virtual void WriteParams();
  
  //-------------------------------------------------------------------------
  //! \brief Writing parameters to save them in static function
  static void StaticWriteParams();
  
  //-------------------------------------------------------------------------
  //! \brief Print Ram usage
  //! \param pTxt Sting to print
  void PrintRamUsage(
    char *pTxt
    );

  //-------------------------------------------------------------------------
  // Initialisation du répertoire de départ
  // Retourne true si OK
  static bool InitDirFile();
  
  //-------------------------------------------------------------------------
  // Retourne la chaine à afficher en ne prenant que les n caractères les plus significatifs
  // 123456789012345678901
  //  FichierTestGrandRhinolophe.wav
  //  rTestGrandRhinolophe
  String PrepareStrFile(
    String str,           // Chaine à vérifier
    int iMax=MAXCHARLINE  // Taille max du nom de fichier
    );

  //-------------------------------------------------------------------------
  //! \brief Update screen (standard behavior = pDisplay->sendBuffer())
  virtual void UpdateScreen();

  //-------------------------------------------------------------------------
  // Mesure de la batterie
  static bool CheckBatteries();
  
  //-------------------------------------------------------------------------
  // List the wav files and directories of the current or root directory
  void ListFiles(
    const char * dirname // Répertoire de départ des fichiers wav ou, si null, fichiers scénario bps de la racine
    );
    
  //-------------------------------------------------------------------------
  // Returns the pointer to the common parameters
  static ParamModes *GetParamsC();
  
  //-------------------------------------------------------------------------
  //! \brief Initialization function and calibration of the ADC0 to read the heterodyne frequency
  //! \param bRefVCC true for VCC Ref, false for internal 1V2 ref
  static void ADC0Init(
    bool bRefVCC
    );
    
  //-------------------------------------------------------------------------
  //! \brief Single-shot reading function of an analog input with ADC 0 to read the heterodyne frequency
  //! Returns the value read (0-4095)
  //! \param iPin Pin to read
  static uint16_t ADC0Read(
    int iPin
    );
    
  //-------------------------------------------------------------------------
  //! \brief Test the existence of a directory and a file
  //! \param *pDir  Dir name
  //! \param *pFile File name (possibly NULL)
  static bool TestFiles(
    char *pDir,
    char *pFile
    );
    
  //-------------------------------------------------------------------------
  //! \brief Reading a scenario file
  //! \param *pFileName File name
  static bool ReadScenarioFile(
    char *pFileName
    );
    
  //-------------------------------------------------------------------------
  //! \brief Writing a scenario file
  //! \param *pFileName File name
  static bool WriteScenarioFile(
    char *pFileName
    );
    
  //-------------------------------------------------------------------------
  //! \brief Automatic addition of a new scenario file
  void AddNewBPS();
  
  //-------------------------------------------------------------------------
  //! \brief Decoding the fields of a row
  //! \param *pAction Parameters of the action after decoding
  //! \param *pLine Line to decode
  //! \return true si OK
  static bool DecodeLine(
    ActionScenario *pAction,
    char *pLine
    );
    
  //-------------------------------------------------------------------------
  //! \brief Decoding the fields of a row
  //! \param *pAction Parameters of the action after decoding
  //! \return true si OK
  static bool VerifyAction(
    ActionScenario *pAction
    );
    
  //-------------------------------------------------------------------------
  //! \brief Coding of the fields of a row
  //! \param *pLine Line to code
  //! \param *pAction Parameters of the action to be coded
  //! \return true si OK
  static bool CodeLine(
    ActionScenario *pAction,
    char *pLine
    );
    
  //-------------------------------------------------------------------------
  //! \brief Return action in a string
  //! \param *sTxt String to initialize
  //! \param *iSize string size
  //! \param *pAction Action to print
  static void GetStringAction(
    char *sTxt,
    int iSize,
    ActionScenario *pAction
    );

  //-------------------------------------------------------------------------
  //! \brief Display of an action on the debug serial link
  //! \param Text at the beginning of the line
  //! \param *pAction Action to print
  static void PrintAction(
    char *sTxt,
    ActionScenario *pAction
    );
    
  //-------------------------------------------------------------------------
  //! \brief Function test of the SD card. Resets the card if necessary and goes into error if the card stops responding
  //! \return true if OK, false if SD error
  bool isSDInit();
  
protected:
  // Avec ou sans les paramètres communs aux modes
  bool bParams;
  
  // Paramètres communs des modes
  static ParamModes paramsC;

  // Etat de charge de la batterie en pourcent
  int  iChargBat;

  // Tension de la batterie LiPo interne en V
  static double fNivBatLiPo;
  // Tension de la batterie externe en V
  static double fNivBatExt;
  // Niveau de charge de la batterie LiPo interne en %
  static short iNivBatLiPo;
  // Niveau de charge de la batterie externe en %
  static short iNivBatExt;

  // Liste des fichiers wav présents dans le répertoire courant
  static std::vector<String> lstWaveFiles;

  //! Liste des actions du scénario courant
  static std::vector<ActionScenario> lstScenario;

  //! Temps pour la lecture des batteries
  uint32_t TimeBat;
  
  //! Indique qu'un MCP3221 est présent pour la meure de la tension batterie
  static bool bADCBat;

  //! Gestionnaire d'un éventuel MCP3221 pour la mesure de la tension batterie
  static CADCVoltage ADCBat;

  //! Indique une sélection de fichier
  static bool bNewSelFile;

public:
  // Mode debug
  static bool bDebug;
};

//-------------------------------------------------------------------------
//! \class CModeCopyright
//! \brief Classe d'affichage des copyright
class CModeCopyright : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeCopyright();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief  Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param[in] key Touch to treat
  //! \return If a mode change, returns the requested mode, else return NOMODE
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Seconde courante pour une gestion de l'affichage toute les secondes
  uint8_t iCurrentSecond;
  
};

#endif // CMODEGEN_H
