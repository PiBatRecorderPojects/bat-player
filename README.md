![Bat Player V0.2](https://git.framasoft.org/PiBatRecorderPojects/bat-player/raw/master/Etude%20Technique/Documents/BatPlayerV02.JPG)

![Bat Player prototype 01](https://git.framasoft.org/PiBatRecorderPojects/bat-player/raw/master/Etude%20Technique/Documents/BatPlayerB.PNG)

__Bat Player (BP)__ est un lecteur de fichier wav ayant des capacités ultrasons. Il est capable de restituer les sons à la fréquence d’échantillonnage de 384kHz au maximum, donc des signaux de 10Hz jusqu’à 192kHz. Son haut-parleur de type Tweeter à ruban est donné pour 40kHz maximum. Il reste opérationnel jusqu’à 100/120kHz environ mais avec un rendement réduit.

Ce dossier regroupe l'ensemble des éléments permettant de construire et/ou modifier BatPlayer

État des lieux :
-

- Plus de 20 BatPlayer réalisés.
- Un dossier de fabrication complet est disponible.
- Il est possible de réaliser un exemplaire seul dans son garage mais il est plus économique de se regrouper pour, en une journée, réaliser 10 exemplaires lors d'un atelier participatif.
- La version 0.2 fonctionne avec une carte processeur Teensy 3.6, la carte électronique est pré-montée, l'amplificateur est amélioré et il est possible de brancher un second haut-parleur.

Description des répertoires
-

- **Etude Technique** regroupe les documents de conception du projet à la racine, les documents de fabrication, manuel d'utilisation et chargement logiciel dans **Documents**.
- **Fabrication** regroupe les documents de fabrication nécessaires à la réalisation d'un ou de plusieurs exemplaires chez soi ou lors d'un atelier participatif.
- **Schema** est le schéma de fonctionnement et le circuit-imprimé de Bat Player réalisés avec l'outil libre KiCad (<http://kicad-pcb.org/>).
- **Update** regroupe un document, un outil et des fichiers binaires pour télécharger le logiciel d'un Bat Player.
- **BatPlayer**, le code source du logiciel. Il utilise l'environnement (IDE) Arduino (<https://www.arduino.cc/en/main/software>) accompagné de l'extension TeensyDuino (<https://www.pjrc.com/teensy/teensyduino.html>) pour les cartes Teensy.


Licence :
-

__Bat Player Copyright (c) 2018 Vrignault Jean-Do.__
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
